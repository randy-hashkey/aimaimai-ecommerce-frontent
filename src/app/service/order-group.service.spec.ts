import {TestBed} from '@angular/core/testing';

import {OrderGroupService} from './order-group.service';

describe('OrderGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderGroupService = TestBed.get(OrderGroupService);
    expect(service).toBeTruthy();
  });
});
