import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {OrderGroupResponse, orderMerchantGroupContent} from '../models/order.group.model.';

@Injectable({
  providedIn: 'root'
})
export class OrderGroupService {

  orderGroupIdToMap: Map<string, orderMerchantGroupContent> = new Map<string, orderMerchantGroupContent>();

  constructor(private _httpClient: HttpClient) {
  }

  approve(orderGroupId: string): Observable<OrderGroupResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('orderGroupId', orderGroupId);

    return this._httpClient.get('./api/sales/order-group/approve?' + httpParams.toString(), {headers: headers})
      .map((response: OrderGroupResponse) => response);
  }

  reject(orderGroupId: string): Observable<OrderGroupResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('orderGroupId', orderGroupId);

    return this._httpClient.get('./api/sales/order-group/reject?' + httpParams.toString(), {headers: headers})
      .map((response: OrderGroupResponse) => response);
  }

  finish(orderGroupId: string): Observable<OrderGroupResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('orderGroupId', orderGroupId);

    return this._httpClient.get('./api/sales/order-group/finish?' + httpParams.toString(), {headers: headers})
      .map((response: OrderGroupResponse) => response);
  }


}
