import {Injectable} from '@angular/core';
import {AddressContent, AddressResponse} from '../models/address.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  addressIdToMap: Map<string, AddressContent> = new Map<string, AddressContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  registerAddress(content: AddressContent): Observable<AddressResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/address/create', content, {headers: headers})
      .map((response: AddressResponse) => response)
      .do(response => {
        this.addressIdToMap.set(response.address.id, response.address);
      });
  }

  listMyAddress(page: number, size: number): Observable<AddressResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/address', {headers: headers})
      .map((response: AddressResponse) => response);
  }

  getMyAddress(id: string): Observable<AddressContent> {
    if (this.addressIdToMap.has(id)) {
      return Observable.of(this.addressIdToMap.get(id));
    }
    this.addressIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/address/get/' + id, {headers: headers})
      .map((response: AddressResponse) => response.address)
      .do(response => {
        this.addressIdToMap.set(id, response);
      });
  }

  edit(content: AddressContent): Observable<AddressContent> {
    // if (this.addressIdToMap.has(content.id)) return Observable.of(this.addressIdToMap.get(content.id));
    this.addressIdToMap.set(content.id, null);

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/address/edit', content, {headers: headers})
      .map((response: AddressResponse) => response.address)
      .do(response => {
        this.addressIdToMap.set(response.id, response);
      });
  }

  delete(content: AddressContent): Observable<AddressContent> {
    this.addressIdToMap.set(content.id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/address/delete', content, {headers: headers})
      .map((response: AddressResponse) => response.address)
      .do(response => {
        this.addressIdToMap.set(content.id, response);
      });
  }

  getAddressById(id: string): Observable<AddressContent> {
    if (this.addressIdToMap.has(id)) {
      return Observable.of(this.addressIdToMap.get(id));
    }
    this.addressIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/address/get' + id, {headers: headers})
      .map((response: AddressResponse) => response.address)
      .do(response => {
        this.addressIdToMap.set(id, response);
      });
  }

  setPrimary(content: AddressContent): Observable<AddressContent> {
    this.addressIdToMap.set(content.id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/address/set-primary', content, {headers: headers})
      .map((response: AddressResponse) => response.address)
      .do(response => {
        this.addressIdToMap.set(content.id, response);
      });
  }

  getPrimary(): Observable<AddressContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/address/get-primary-address', {headers: headers})
      .map((response: AddressResponse) => response.address);
  }

}
