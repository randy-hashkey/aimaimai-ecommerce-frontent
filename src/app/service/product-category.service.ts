import {Injectable} from '@angular/core';
import {CategoryContent, CategoryResponse} from '../models/product-category.model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  categoryIdToMap: Map<string, CategoryContent> = new Map<string, CategoryContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  list(page: number, size: number, search): Observable<CategoryResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'name,desc');

    if (search) {
      httpParams = httpParams.set('q', search);
    }

    return this._httpClient.get('./api/catalog/category/public?' + httpParams.toString(), {headers: headers})
      .map((response: CategoryResponse) => response);
  }


  getById(name: string): Observable<CategoryContent> {
    if (this.categoryIdToMap.has(name)) {
      return Observable.of(this.categoryIdToMap.get(name));
    }
    this.categoryIdToMap.set(name, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/catalog/category/public/' + name, {headers: headers})
      .map((response: CategoryResponse) => response.category)
      .do(response => {
        this.categoryIdToMap.set(name, response);
      });
  }

  create(content: CategoryContent): Observable<CategoryContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/category/create', content, {headers: headers})
      .map((content: CategoryResponse) => content.category)
      .do(response => {
        this.categoryIdToMap.set(response.name, response);
      });
  }

  delete(content: CategoryContent): Observable<CategoryResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/category/delete', content, {headers: headers})
      .map((content: CategoryResponse) => content)
      .do(response => {
        // this.categoryIdToMap.set(response, response)
      });
  }
}
