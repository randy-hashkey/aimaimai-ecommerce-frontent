import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {CreatePaypalResponse, OrderPaymentContent, OrderPaymentResponse} from '../models/order.payment.model';

@Injectable({
  providedIn: 'root'
})
export class OrderPaymentService {

  constructor(private _httpClient: HttpClient) {
  }

  createOrderByPaypal(orderId: string): Observable<CreatePaypalResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/order/paypal/public/create/' + orderId, {headers: headers})
      .map((response: CreatePaypalResponse) => response);
  }

  getOrderPayment(orderId: string): Observable<OrderPaymentContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/order/payment/get-by-id?orderId=' + orderId, {headers: headers})
      .map((response: OrderPaymentResponse) => response.orderPayment);
  }


}
