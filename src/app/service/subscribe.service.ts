import {Injectable} from '@angular/core';
import {SubscribeContent, SubscribeResponse} from '../models/subscribe.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class SubscribeService {

  constructor(private _httpClient: HttpClient) {
  }


  registerSubscribes(content: SubscribeContent): Observable<SubscribeResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/blog/subscribe/email/public/register', content, {headers: headers})
      .map((response: SubscribeResponse) => response);
  }

  unSubscribeMail(email: string): Observable<SubscribeResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let content = {
      email: email
    };

    return this._httpClient.post('./api/blog/subscribe/email/public/unsubscribe', content, {headers: headers})
      .map((response: SubscribeResponse) => response);
  }
}
