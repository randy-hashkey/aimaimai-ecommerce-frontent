import { TestBed } from '@angular/core/testing';

import { MerchantGroupReportService } from './merchant-group-report.service';

describe('MerchantGroupReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MerchantGroupReportService = TestBed.get(MerchantGroupReportService);
    expect(service).toBeTruthy();
  });
});
