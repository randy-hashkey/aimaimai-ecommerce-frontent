import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {AddressResponse} from '../../models/address.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../../auth.service';
import {OrderGroupResponse} from '../../models/report/order-group-report.model';

@Injectable({
  providedIn: 'root'
})
export class MerchantGroupReportService {

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  list(merchantId : string, page: number, size: number): Observable<OrderGroupResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/report/order/list-by-merchant?merchantId='+merchantId, {headers: headers})
      .map((response: OrderGroupResponse) => response);
  }
}
