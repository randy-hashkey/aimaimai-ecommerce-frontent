import {Injectable} from '@angular/core';
import {LoginModel, LoginResponse, RegisterModel, UserContent, UserResponse} from '../models/user.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {AppSettings} from '../app.setting';
import {AddressContent} from '../models/address.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userIdToModelMap: Map<string, UserContent> = new Map<string, UserContent>();

  userEmailToModelMap: Map<string, LoginModel> = new Map<string, LoginModel>();

  userAddressToModelMap: Map<string, AddressContent> = new Map<string, AddressContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  login(loginModel: LoginModel): Observable<LoginResponse> {
    return this._httpClient.post(AppSettings.USER_LOGIN, loginModel)
      .map((response: LoginResponse) => response);
  }

  get(id: string): Observable<UserContent> {
    if (this.userIdToModelMap.has(id)) {
      return Observable.of(this.userIdToModelMap.get(id));
    }

    this.userIdToModelMap.set(id, null);

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/user/user/get-by-id/' + id, {headers: headers})
      .map((response: UserContent) => response)
      .do(response => {
        this.userIdToModelMap.set(id, response);
      });
  }

  register(content: RegisterModel): Observable<UserResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/user/email/public/register', content, {headers: headers})
      .map((response: UserResponse) => response)
      .do(response => {
        this.userIdToModelMap.set(response.user.id, response.user);
      });
  }

  registerAsMerchant(): Observable<UserResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._httpClient.post('./api/user/user/register-as-merchant', {headers: headers})
      .map((response: UserResponse) => {
        this._authService.logout();
        this._authService.login(response.user);
        return response;
      });
  }


  edit(content: UserContent): Observable<UserContent> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/user/user/edit-my-info', content, {headers: headers})
      .map((content: UserResponse) => content.user)
      .do(response => {
        this.userIdToModelMap.set(response.id, response);
      });
  }

  getMyProfile(): Observable<UserContent> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/user/user/get', {headers: headers})
      .map((response: UserContent) => response)
      .do(response => {
        this.userIdToModelMap.set(response.id, response);
      });
  }

  getMyEmail(): Observable<LoginModel> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/user/email/get-my-email', {headers: headers})
      .map((response: LoginModel) => response)
      .do(response => {
        this.userEmailToModelMap.set(response.email, response);
      });
  }

  getMyAddress(): Observable<AddressContent> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/address/get', {headers: headers})
      .map((response: AddressContent) => response)
      .do(response => {
        this.userAddressToModelMap.set(response.id, response);
      });
  }

  getMyPhone(): Observable<UserContent> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/user/phone/list-mine', {headers: headers})
      .map((response: UserContent) => response)
      .do(response => {
        this.userIdToModelMap.set(response.id, response);
      });
  }
}
