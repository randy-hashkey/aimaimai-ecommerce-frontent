import {Injectable} from '@angular/core';
import {AttributeContent, AttributeResponse} from '../models/attribute.model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  attributeIdToMap: Map<string, AttributeContent> = new Map<string, AttributeContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  list(page: number, size: number, search: string): Observable<AttributeResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'name,desc');

    if (search) {
      httpParams = httpParams.set('search', search);
    }

    return this._httpClient.get('./api/catalog/attribute/public?' + httpParams.toString(), {headers: headers})
      .map((response: AttributeResponse) => response);
  }

  getById(name: string): Observable<AttributeContent> {
    if (this.attributeIdToMap.has(name)) {
      return Observable.of(this.attributeIdToMap.get(name));
    }
    this.attributeIdToMap.set(name, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/catalog/attribute/public/' + name, {headers: headers})
      .map((response: AttributeResponse) => response.attribute)
      .do(response => {
        this.attributeIdToMap.set(name, response);
      });
  }

  delete(content: AttributeContent): Observable<AttributeContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/attribute/delete', content, {headers: headers})
      .map((response: AttributeResponse) => response.attribute)
      .do(response => {
        this.attributeIdToMap.set(content.name, null);
        this.attributeIdToMap.set(response.name, response);
      });
  }


  create(content: AttributeContent): Observable<AttributeContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/attribute/create', content, {headers: headers})
      .map((content: AttributeResponse) => content.attribute)
      .do(response => {
        this.attributeIdToMap.set(response.name, response);
      });
  }
}
