import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {ProductCategoryResponse} from '../../models/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductSpecialCategoryPublicService {

  constructor(private _httpClient: HttpClient) {
  }


  productMostPopularPublic(page: number, size: number): Observable<ProductCategoryResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'id,desc');

    return this._httpClient.get('./api/catalog/product/popular/public?' + httpParams.toString(), {headers: headers})
      .map((response: ProductCategoryResponse) => response);
  }

  bestsellersPublic(page: number, size: number): Observable<ProductCategoryResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'id,desc');

    return this._httpClient.get('./api/catalog/product/public/bestsellers?' + httpParams.toString(), {headers: headers})
      .map((response: ProductCategoryResponse) => response);
  }
}
