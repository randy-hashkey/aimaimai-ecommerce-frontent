import {Injectable} from '@angular/core';
import {ProductContent, ProductResponse} from '../../models/product.model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductImageResponse} from '../../models/product-image.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class ProductPublicService {

  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  constructor(private _httpClient: HttpClient) {
  }

  listPublic(page: number, size: number, search: string, orderBy: string): Observable<ProductResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('search', search)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'updatedAt,desc');

    if (orderBy) {
      httpParams = httpParams.append('orderBy', orderBy);
    }

    return this._httpClient.get('./api/catalog/product/public?' + httpParams.toString(), {headers: headers})
      .map((response: ProductResponse) => response);
  }


  listByCategoryPublic(category: string, page: number, size: number, search: string, orderBy: string): Observable<ProductResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('search', search)
      .set('category', category)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'updatedAt,desc');

    if (orderBy) {
      httpParams = httpParams.append('orderBy', orderBy);
    }

    return this._httpClient.get('./api/catalog/product-category/public/list-by-category/' + category + '?' + httpParams.toString(), {headers: headers})
      .map((response: ProductResponse) => response);
  }

  listByMerchantPublic(page: number, size: number, search: string, merchantId: string): Observable<ProductResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('search', search)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'createdAt,desc');

    return this._httpClient.get('./api/catalog/product/public/merchant?merchantId=' + merchantId, {params: httpParams, headers: headers})
      .map((response: ProductResponse) => response);
  }

  getByIdPublic(id: string): Observable<ProductContent> {
    if (this.productIdToMap.get(id) != null) {
      return Observable.of(this.productIdToMap.get(id));
    }
    this.productIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/catalog/product/public/get/' + id, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {
        this.productIdToMap.set(id, response);
      });
  }

  getImagesByProductIdPublic(productId): Observable<ProductImageResponse> {
    this.productIdToMap.set(productId, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/catalog/product-image/public/' + productId, {headers: headers})
      .map((response: ProductImageResponse) => response)
      .do(response => {
      });
  }
}
