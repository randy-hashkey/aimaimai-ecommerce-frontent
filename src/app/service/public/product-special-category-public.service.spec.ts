import {TestBed} from '@angular/core/testing';

import {ProductSpecialCategoryPublicService} from './product-special-category-public.service';

describe('ProductSpecialCategoryPublicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductSpecialCategoryPublicService = TestBed.get(ProductSpecialCategoryPublicService);
    expect(service).toBeTruthy();
  });
});
