import {TestBed} from '@angular/core/testing';

import {ProductPublicService} from './product-public.service';

describe('ProductPublicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductPublicService = TestBed.get(ProductPublicService);
    expect(service).toBeTruthy();
  });
});
