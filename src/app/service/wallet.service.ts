import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TaxContent, TaxResponse} from '../models/tax.model';
import {WalletResponse} from '../models/wallet.model';

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  constructor(private _httpClient: HttpClient) {
  }

  getMyWallet(currencyId:string): Observable<WalletResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/wallet/credit/get-by-currency/'+currencyId, {headers: headers})
      .map((response: WalletResponse) => response);
  }
}
