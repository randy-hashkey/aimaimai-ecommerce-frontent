import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {SliderResponse} from '../models/slider.model';

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  constructor(private _httpClient: HttpClient) {
  }

  list(page: number, size: number): Observable<SliderResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'updatedAt,desc');

    return this._httpClient.get('./api/blog/slider/public?' + httpParams.toString(), {headers: headers})
      .map((response: SliderResponse) => response);
  }

}
