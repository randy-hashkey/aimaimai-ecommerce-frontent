import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Cart, CartItem, CartResponse} from '../models/cart.model';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private static __cart = '__Cart';


  constructor(private _cookieService: CookieService, private _httpClient: HttpClient, private _authService: AuthService) {
  }

  addToCart(content: CartItem) {

    if (!this._authService.loggedIn()) {
      this.addCartToCookies(content);
      return;
    }
    this.addCartToAccount(content).subscribe();
  }

  getCart(): Observable<Cart> {
    if (!this._authService.loggedIn()) {
      let cart: Cart = new Cart();

      let exist: boolean = this._cookieService.check(CartService.__cart);
      if (exist) {
        cart = JSON.parse(this._cookieService.get(CartService.__cart));
      }
      return Observable.of(cart);
    }

    let exist: boolean = this._cookieService.check(CartService.__cart);
    if (exist) {
      let cart: Cart = JSON.parse(this._cookieService.get(CartService.__cart));
      let item: CartItem;
      for (item of cart.cartItems) {
        this.addCartToAccount(item).subscribe();
      }

      this._cookieService.delete(CartService.__cart);
    }

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._httpClient.get('./api/sales/cart/get', {headers: headers})
      .map((content: CartResponse) => content.cart);
  }

  deleteCart(content: CartItem): Observable<Cart> {
    if (!this._authService.loggedIn()) {
      return Observable.of(this.deleteCartFromCookies(content));
    }

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._httpClient.post('./api/sales/cart/delete-item', content, {headers: headers})
      .map((content: CartResponse) => content.cart);
  }

  editCart(content: CartItem): Observable<Cart> {

    if (!this._authService.loggedIn()) {
      return Observable.of(this.editCartFromCookies(content));
    }

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/sales/cart/edit', content, {headers: headers})
      .map((content: CartResponse) => content.cart);
  }

  private addCartToCookies(content: CartItem) {
    let cart: Cart = new Cart();
    // let cartItem: CartItem;

    let exist: boolean = this._cookieService.check(CartService.__cart);
    if (exist) {

      cart = JSON.parse(this._cookieService.get(CartService.__cart));

      // cartItem = cart.cartItems.find(p => p.productId == content.productId);
      //
      // if (cartItem) {
      //   content.quantity = content.quantity + cartItem.quantity;
      //   cart.cartItems.splice(cart.cartItems.indexOf(cartItem), 1);
      // }

    }
    cart.cartItems.push(content);

    this._cookieService.set(CartService.__cart, JSON.stringify(cart));
  }

  private addCartToAccount(content: CartItem): Observable<Cart> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/sales/cart/add', content, {headers: headers})
      .map((content: CartResponse) => content.cart);
  }

  private deleteCartFromCookies(content: CartItem): Cart {

    let cart: Cart = new Cart();
    let exist: boolean = this._cookieService.check(CartService.__cart);
    if (exist) {

      cart = JSON.parse(this._cookieService.get(CartService.__cart));

      let cartItem: CartItem = cart.cartItems.find(p => p.productId == content.productId);

      if (cartItem) {
        cart.cartItems.splice(cart.cartItems.indexOf(cartItem), 1);
      }

      this._cookieService.set(CartService.__cart, JSON.stringify(cart));

    }
    return cart;

  }

  private editCartFromCookies(item: CartItem): Cart {

    let cart: Cart = new Cart();
    let exist: boolean = this._cookieService.check(CartService.__cart);
    if (exist) {

      cart = JSON.parse(this._cookieService.get(CartService.__cart));

      let cartItem: CartItem = cart.cartItems.find(p => p.productId == item.productId);

      if (cartItem) {
        cart.cartItems.splice(cart.cartItems.indexOf(cartItem), 1);
      }
      cart.cartItems.push(item);

      this._cookieService.set(CartService.__cart, JSON.stringify(cart));

    }
    return cart;

  }
}

