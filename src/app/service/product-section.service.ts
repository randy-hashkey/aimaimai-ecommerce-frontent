import {Injectable} from '@angular/core';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductContent, ProductResponse} from '../models/product.model';
import {ProductAttribute} from '../models/attribute.model';
import {ProductAttributeResponse} from '../models/product-attribute.model';

@Injectable({
  providedIn: 'root'
})
export class ProductSectionService {

  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  constructor(private _httpClient: HttpClient) {
  }

  deleteCategories(productId: string, merchantId: string, categories: string[]): Observable<ProductContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let content = {
      productId: productId,
      merchantId: merchantId,
      categories: categories
    };
    return this._httpClient.post('./api/catalog/product/section/delete-categories', content, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {

      });
  }

  deleteAttributes(productId: string, merchantId: string, attributes: ProductAttribute[]): Observable<ProductContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let content = {
      productId: productId,
      merchantId: merchantId,
      attributes: attributes
    };

    return this._httpClient.post('./api/catalog/product/section/delete-attributes', content, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {

      });
  }

  deleteImages(productId: string, merchantId: string, images: string[]): Observable<ProductContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let content = {
      productId: productId,
      merchantId: merchantId,
      images: images
    };

    return this._httpClient.post('./api/catalog/product/section/delete-images', content, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {

      });
  }


  getAttributes(productId: string): Observable<ProductAttributeResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');


    return this._httpClient.get('./api/catalog/product/section/public/attributes/' + productId, {headers: headers})
      .map((response: ProductAttributeResponse) => response)
      .do(response => {

      });
  }


}
