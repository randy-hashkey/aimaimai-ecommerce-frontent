import {Injectable} from '@angular/core';
import {ContactContent, ContactResponse} from '../models/contact.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  contactIdToMap: Map<string, ContactContent> = new Map<string, ContactContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  registerContact(content: ContactContent): Observable<ContactResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/blog/contact-us/public/add', content, {headers: headers})
      .map((response: ContactResponse) => response)
      .do(response => {
        this.contactIdToMap.set(response.contact.id, response.contact);
      });
  }
}

