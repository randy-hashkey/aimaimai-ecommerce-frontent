import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {ReferralCodeContent, ReferralCodeResponse} from '../models/referral-code.model';

@Injectable({
  providedIn: 'root'
})
export class ReferralService {

  constructor(private _httpClient: HttpClient) {
  }

  findReferralCode(code: string): Observable<ReferralCodeContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('code', code);

    return this._httpClient.get('./api/referral/code/public/find-by-code?' + httpParams.toString(), {headers: headers})
      .map((response: ReferralCodeResponse) => response.referralCode);
  }
}
