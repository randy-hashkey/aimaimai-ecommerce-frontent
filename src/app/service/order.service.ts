import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {RuleContent, RuleResponse} from '../models/rule.model';
import {OrderContent, OrderResponse} from '../models/order.model';
import {AuthService} from '../auth.service';
import {OrderGroupResponse, orderMerchantGroupContent} from '../models/order.group.model.';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  orderIdToModelMap: Map<string, OrderContent> = new Map<string, OrderContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  validCoupon(couponCode: string): Observable<RuleContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('couponCode', couponCode);

    return this._httpClient.get('./api/sales/rule/valid-coupon?' + httpParams.toString(), {headers: headers})
      .map((response: RuleResponse) => response.rule);
  }

  addCouponToCart(content: RuleContent): Observable<RuleContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/sales/rule/add-rule-to-cart', content, {headers: headers})
      .map((response: RuleResponse) => response.rule);
  }

  checkout(): Observable<OrderResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/order/checkout', {headers: headers})
      .map((response: OrderResponse) => response);
  }

  getOrderById(id: string): Observable<OrderResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/order/get-by-id?id=' + id, {headers: headers})
      .map((response: OrderResponse) => response);
  }

  getByGroupId(groupId: string, orderId: string): Observable<OrderContent> {
    if (this.orderIdToModelMap.get(orderId) != null) {
      return Observable.of(this.orderIdToModelMap.get(orderId));
    }
    this.orderIdToModelMap.set(orderId, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/order/get-by-group-id?groupId=' + groupId, {headers: headers})
      .map((response: OrderResponse) => response.order)
      .do(response => {
        this.orderIdToModelMap.set(response.id, response);
      });
  }

  getOrderByIdMerchant(id: string): Observable<OrderResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/order-group/get-by-merchant', {headers: headers})
      .map((response: OrderResponse) => response);
  }

  getMyOrder(id: string): Observable<OrderContent> {
    if (this.orderIdToModelMap.has(id)) {
      return Observable.of(this.orderIdToModelMap.get(id));
    }
    this.orderIdToModelMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/sales/order-group/get', {headers: headers})
      .map((response: OrderResponse) => response.order)
      .do(response => {
        this.orderIdToModelMap.set(response.id, response);
      });
  }

  listMyOrder(page: number, size: number): Observable<OrderResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'updated_at,desc');

    return this._httpClient.get('./api/sales/order?' + httpParams.toString(), {headers: headers})
      .map((response: OrderResponse) => response);
  }

  createOrder(content): Observable<OrderResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/sales/order/create', content, {headers: headers})
      .map((response: OrderResponse) => response);
  }

  deleteCouponCart(cartId: string, couponCode: string): Observable<RuleContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let content = {
      cartId: cartId,
      couponCode: couponCode
    };

    return this._httpClient.post('./api/sales/rule/delete-rule-from-cart', content, {headers: headers})
      .map((response: RuleResponse) => response.rule);
  }

  listMerchantGroup(orderId: string, page: number, size: number): Observable<OrderGroupResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'createdAt,asc');

    return this._httpClient.get('./api/sales/order-group/merchant?merchantId=' + orderId, {headers: headers})
      .map((response: OrderGroupResponse) => response);
  }

  getMerchantGroupId(orderGroupId: string, merchantId: string): Observable<orderMerchantGroupContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('merchantId', merchantId)
      .set('orderGroupId', orderGroupId);

    return this._httpClient.get('./api/sales/order-group/get-by-merchant?' + httpParams.toString(), {headers: headers})
      .map((response: OrderGroupResponse) => response.orderMerchantGroup);
  }
}
