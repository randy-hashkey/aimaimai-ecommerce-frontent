import {TestBed} from '@angular/core/testing';

import {ShipmentMethodService} from './shipment-method.service';

describe('ShipmentMethodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShipmentMethodService = TestBed.get(ShipmentMethodService);
    expect(service).toBeTruthy();
  });
});
