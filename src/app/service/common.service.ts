import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public readonly client_id: string = '238762493698651';

  public readonly redirect_uri: string = 'http://localhost:4300/login';

  constructor() {
  }

  shortOrderId(id: string): string {
    let orderId: string = id.substring(0, id.indexOf('-'));
    let firstText = 'AIMAIMAI-';
    return firstText + orderId;
  }
}
