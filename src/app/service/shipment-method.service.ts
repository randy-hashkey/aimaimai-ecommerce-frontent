import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {ShipmentMethodContent, ShipmentMethodResponse} from '../models/shipment-method.model';

@Injectable({
  providedIn: 'root'
})
export class ShipmentMethodService {

  shipmentMethodIdToMap: Map<string, ShipmentMethodContent> = new Map<string, ShipmentMethodContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  list(page: number, size: number, search: string): Observable<ShipmentMethodResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    let httpParams = new HttpParams()
      .set('search', search)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'name,desc');

    return this._httpClient.get('./api/sales/shipment-method/public/?' + httpParams.toString(), {headers: headers})
      .map((response: ShipmentMethodResponse) => response);
  }

  getById(id: string): Observable<ShipmentMethodContent> {
    if (this.shipmentMethodIdToMap.has(id)) {
      return Observable.of(this.shipmentMethodIdToMap.get(id));
    }
    this.shipmentMethodIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/sales/shipment-method/' + id, {headers: headers})
      .map((response: ShipmentMethodResponse) => response.shipmentMethod)
      .do(response => {
        this.shipmentMethodIdToMap.set(id, response);
      });
  }

  delete(content: ShipmentMethodContent): Observable<ShipmentMethodContent> {
    this.shipmentMethodIdToMap.set(content.name, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/sales/shipment-method/delete', content, {headers: headers})
      .map((response: ShipmentMethodResponse) => response.shipmentMethod)
      .do(response => {
        this.shipmentMethodIdToMap.set(response.name, response);
      });
  }

  create(content: ShipmentMethodContent): Observable<ShipmentMethodContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/sales/shipment-method/create', content, {headers: headers})
      .map((response: ShipmentMethodResponse) => response.shipmentMethod)
      .do(response => {
        this.shipmentMethodIdToMap.set(response.name, response);
      });
  }
}
