import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {ProductContent, ProductDiscountContent, ProductResponse} from '../models/product.model';
import {ProductImageContent, ProductImageResponse} from '../models/product-image.model';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {ProductAttribute} from '../models/attribute.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  getById(id: string): Observable<ProductContent> {
    if (this.productIdToMap.get(id) != null) {
      return Observable.of(this.productIdToMap.get(id));
    }
    this.productIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/catalog/product/get/' + id, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {
        this.productIdToMap.set(id, response);
      });
  }

  getByIdAfterEdit(id: string): Observable<ProductContent> {
    this.productIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/catalog/product/get/' + id, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {
        this.productIdToMap.set(id, response);
      });
  }

  listByMerchant(page: number, size: number, search: string, merchantId: string): Observable<ProductResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('search', search)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'createdAt,desc');

    return this._httpClient.get('./api/catalog/product/merchant?merchantId=' + merchantId, {params: httpParams, headers: headers})
      .map((response: ProductResponse) => response);
  }

  delete(content: ProductContent): Observable<ProductContent> {
    this.productIdToMap.set(content.id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/product/delete', content, {headers: headers})
      .map((response: ProductResponse) => response.product)
      .do(response => {
        this.productIdToMap.set(content.id, response);
      });
  }

  add(content: ProductContent): Observable<ProductContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/product/add', content, {headers: headers})
      .map((content: ProductResponse) => content.product)
      .do(response => {
        // this.productIdToMap.set(response.id, response);
      });
  }

  edit(content: ProductContent): Observable<ProductContent> {
    this.productIdToMap.set(content.id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/catalog/product/edit', content, {headers: headers})
      .map((content: ProductResponse) => content.product)
      .do(response => {
        this.productIdToMap.set(response.id, response);
      });
  }

  setDiscount(content: ProductDiscountContent): Observable<ProductContent> {
    this.productIdToMap.set(content.productId, null);

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._httpClient.post('./api/catalog/product/add-discount', content, {headers: headers})
      .map((content: ProductResponse) => content.product)
      .do(response => {
        this.productIdToMap.set(content.productId, response);
      });
  }

  uploadProductImage(imageId: string, productId: string): Observable<ProductImageContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let obj = {
      imageId: imageId,
      productId: productId
    };

    return this._httpClient.post('./api/catalog/product-image/add-image', obj, {headers: headers})
      .map((content: ProductImageResponse) => content.productImage)
      .do(response => {

      });
  }


  addProductAttribute(content: ProductAttribute): Observable<ProductResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/catalog/product/add-attribute', content, {headers: headers})
      .map((content: ProductResponse) => content);
  }
}
