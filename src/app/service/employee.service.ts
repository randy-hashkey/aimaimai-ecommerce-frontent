import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {EmployeeContent, EmployeeResponse} from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  employeeIdToMap: Map<string, EmployeeContent> = new Map<string, EmployeeContent>();

  constructor(private _httpClient: HttpClient) {
  }

  listByMerchant(page: number, size: number, merchantId: string): Observable<EmployeeResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    let httpParams = new HttpParams()
      .set('merchantId', merchantId)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'createdAt,desc');

    return this._httpClient.get('./api/merchant/employee/list-by-merchant', {params: httpParams, headers: headers})
      .map((response: EmployeeResponse) => response);
  }

  invertEmployeeToMerchant(content: EmployeeContent): Observable<EmployeeResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/merchant/employee/invert', content, {headers: headers})
      .map((response: EmployeeResponse) => response);
  }

  delete(content: EmployeeContent): Observable<EmployeeContent> {
    this.employeeIdToMap.set(content.id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/merchant/employee/delete', content, {headers: headers})
      .map((response: EmployeeResponse) => response.employeeMerchant)
      .do(response => {
        this.employeeIdToMap.set(content.id, response);
      });
  }
}
