import {Injectable} from '@angular/core';
import {PaymentMethodContent, PaymentMethodResponse} from '../models/payment-method.model';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentMethodService {

  paymentMethodIdToMap: Map<string, PaymentMethodContent> = new Map<string, PaymentMethodContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  list(page: number, size: number, search: string): Observable<PaymentMethodResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    let httpParams = new HttpParams()
      .set('search', search)
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', 'name,desc');

    return this._httpClient.get('./api/sales/payment-method/public/?' + httpParams.toString(), {headers: headers})
      .map((response: PaymentMethodResponse) => response);
  }

  getById(id: string): Observable<PaymentMethodContent> {
    if (this.paymentMethodIdToMap.has(id)) {
      return Observable.of(this.paymentMethodIdToMap.get(id));
    }
    this.paymentMethodIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/sales/payment-method/' + id, {headers: headers})
      .map((response: PaymentMethodResponse) => response.paymentMethod)
      .do(response => {
        this.paymentMethodIdToMap.set(id, response);
      });
  }

  delete(content: PaymentMethodContent): Observable<PaymentMethodContent> {
    this.paymentMethodIdToMap.set(content.name, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/sales/payment-method/delete', content, {headers: headers})
      .map((response: PaymentMethodResponse) => response.paymentMethod)
      .do(response => {
        this.paymentMethodIdToMap.set(response.name, response);
      });
  }

  create(content: PaymentMethodContent): Observable<PaymentMethodContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.post('./api/sales/payment-method/create', content, {headers: headers})
      .map((response: PaymentMethodResponse) => response.paymentMethod)
      .do(response => {
        this.paymentMethodIdToMap.set(response.name, response);
      });
  }
}
