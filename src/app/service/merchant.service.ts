import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {MerchantContent, MerchantResponse} from '../models/merchant.model';

@Injectable({
  providedIn: 'root'
})
export class MerchantService {

  merchantIdToMap: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  constructor(private _httpClient: HttpClient, private _authService: AuthService) {
  }

  registerMerchant(content: MerchantContent): Observable<MerchantResponse> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/merchant/create', content, {headers: headers})
      .map((response: MerchantResponse) => response)
      .do(response => {
        this.merchantIdToMap.set(response.merchant.merchantId, response.merchant);
      });
  }

  edit(content: MerchantContent): Observable<MerchantContent> {
    // if (this.merchantIdToMap.has(content.id)) return Observable.of(this.merchantIdToMap.get(content.id));
    this.merchantIdToMap.set(content.id, null);

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.post('./api/merchant/edit', content, {headers: headers})
      .map((response: MerchantResponse) => response.merchant)
      .do(response => {
        this.merchantIdToMap.set(response.id, response);
      });
  }

  listMyMerchant(page: number, size: number): Observable<MerchantResponse> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString());

    return this._httpClient.get('./api/merchant/list-my-merchant', {params: httpParams, headers: headers})
      .map((response: MerchantResponse) => response);
  }

  getMyMerchant(id: string): Observable<MerchantContent> {
    if (this.merchantIdToMap.has(id)) {
      return Observable.of(this.merchantIdToMap.get(id));
    }
    this.merchantIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());

    return this._httpClient.get('./api/merchant/get-my-merchant/' + id, {headers: headers})
      .map((response: MerchantResponse) => response.merchant)
      .do(response => {
        this.merchantIdToMap.set(id, response);
      });
  }

  getMerchantById(id: string): Observable<MerchantContent> {
    if (this.merchantIdToMap.has(id)) {
      return Observable.of(this.merchantIdToMap.get(id));
    }
    this.merchantIdToMap.set(id, null);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/merchant/public/' + id, {headers: headers})
      .map((response: MerchantResponse) => response.merchant)
      .do(response => {
        this.merchantIdToMap.set(id, response);
      });
  }


}
