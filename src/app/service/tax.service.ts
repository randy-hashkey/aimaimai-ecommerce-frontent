import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {TaxContent, TaxResponse} from '../models/tax.model';

@Injectable({
  providedIn: 'root'
})
export class TaxService {

  constructor(private _httpClient: HttpClient) {
  }

  getSingleTax(): Observable<TaxContent> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._httpClient.get('./api/sales/tax/public/get', {headers: headers})
      .map((response: TaxResponse) => response.tax);
  }
}
