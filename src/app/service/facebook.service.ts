import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {UserResponse} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class FacebookService {


  constructor(private _httpClient: HttpClient) {
  }

  login(): Observable<any> {
    return this._httpClient.get('./api/user/public/facebook/login', {})
      .map((response: any) => response);
  }

  getUserInfo(code: string): Observable<UserResponse> {
    return this._httpClient.get('./api/user/public/facebook/me?code=' + code, {})
      .map((response: UserResponse) => {
        return response;
      });
  }


}
