import {inject, TestBed} from '@angular/core/testing';

import {MageFormService} from './mage-form.service';

describe('MageFormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MageFormService]
    });
  });

  it('should be created', inject([MageFormService], (service: MageFormService) => {
    expect(service).toBeTruthy();
  }));
});
