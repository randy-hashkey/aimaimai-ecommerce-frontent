export class ErrorResponse {
  message: string;
  parameters?: Object;
  errors: ErrorResponse[];

  static getErrorMessage(errorResponse: ErrorResponse): string {
    let message: string = errorResponse.message;
    if (!errorResponse.parameters) {
      return message;
    }
    let parameters: Object = errorResponse.parameters;

    for (let key in parameters) {
      if (parameters.constructor === Array) {
        message = message.replace('%' + (parseInt(key) + 1), parameters[key]);
      } else {
        message = message.replace('%' + key, parameters[key]);
      }
    }

    return message;
  }
}
