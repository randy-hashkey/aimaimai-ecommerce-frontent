import {ErrorResponse} from './mage-form.model';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MageFormService {

  constructor(private http: Http) {
  }

  requestPost(model: Object, api: string, jsonResponse: boolean = true, token?: string): Observable<any> {
    let headers = new Headers(this.createObjectHeader(token));
    let options = new RequestOptions({headers: headers});
    return this.http.post(api, JSON.stringify(model), options)
      .map((response) => {
        return (jsonResponse) ? response.json() : response;
      })
      .catch((response) => Observable.throw(response.json()));
  }

  handleError(response: Response): ErrorResponse {
    return response.json();
  }

  requestPut(model: Object, api: string): Observable<Object> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.put(api, JSON.stringify(model), options)
      .map((response) => response.json())
      .catch(this.handleError.bind(this));
  }

  createObjectHeader(token: string): Object {
    let object = {};
    object['Content-Type'] = 'application/json';
    if (token) {
      object['Authorization'] = 'Bearer: ' + token;
    }
    return object;
  }
}

