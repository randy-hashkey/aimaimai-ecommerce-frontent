import {ErrorResponse} from './mage-form.model';
import {MageFormService} from './mage-form.service';
import {AuthService} from './../../auth.service';
import {Component, ContentChildren, EventEmitter, Input, OnInit, Output, QueryList} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Field} from './../field';

@Component({
  selector: 'app-mage-form',
  templateUrl: './mage-form.component.html',
  styleUrls: ['./mage-form.component.css'],
  providers: [MageFormService]
})
export class MageFormComponent implements OnInit {

  public form: FormGroup;
  @ContentChildren('field')
  fields: QueryList<Field<any>>;
  @Input()
  url: string;
  @Input()
  isAuthKeyNeeded: boolean = true;
  @Input()
  method: string = MageFormComponent.POST_METHOD;
  @Input()
  formErrors: Object;
  @Input()
  validationMessages: Object;
  @Input()
  rules: Object;
  @Output()
  successForm: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  jsonResponse: boolean = true;
  serverErrorMessage: string;

  constructor(protected fb: FormBuilder, protected formService: MageFormService,
              protected authService: AuthService) {
  }

  public static get SEPARATOR(): string {
    return '/';
  }

  public static get POST_METHOD(): string {
    return 'post';
  }

  public static get PUT_METHOD(): string {
    return 'put';
  }

  public static get GET_METHOD(): string {
    return 'get';
  }

  public static get DELETE_METHOD(): string {
    return 'delete';
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = new FormGroup({});
    this.form = this.fb.group(this.rules);
  }

  ngAfterViewInit() {
    for (let field of this.fields.toArray()) {
      field.registerOnChange(this.validate.bind(this));
    }
  }


  updateFormValue(): void {
    let object: Object = {};
    for (let field of this.fields.toArray()) {
      this.form.controls[field.getName()].setValue(field.getValue());
    }
  }

  emptyAllFieldErrors() {
    for (let field of this.fields.toArray()) {
      field.setErrorMessage('');
    }
  }

  validate(): boolean {
    if (!this.form) {
      return false;
    }
    this.emptyAllFieldErrors();
    this.serverErrorMessage = null;
    this.updateFormValue();
    for (let field of this.fields.toArray()) {
      const control = this.form.get(field.getName());
      if (control && !control.valid) {
        let messages = this.validationMessages[field.getName()];
        for (let key in control.errors) {
          field.setErrorMessage(messages[key] + ' ');
        }
      }
    }
    return this.form.valid;
  }

  submit() {
    if (!this.validate()) {
      return;
    }

    let token: string = (this.isAuthKeyNeeded) ? this.authService.getToken() : null;
    if (this.method === MageFormComponent.POST_METHOD) {

      this.formService.requestPost(this.createModel(),
        this.url, this.jsonResponse, token).subscribe(
        this.handleNonErrorResponse.bind(this),
        this.handleError.bind(this)
      );
    } else if (this.method === MageFormComponent.PUT_METHOD) {
      this.formService.requestPut(this.createModel(), this.url).subscribe(
        this.handleNonErrorResponse.bind(this),
        this.handleError.bind(this)
      );
    } else if (this.method === MageFormComponent.GET_METHOD) {

    } else if (this.method === MageFormComponent.DELETE_METHOD) {

    }

  }

  createModel(): Object {
    let object: Object = {};
    for (let key in this.form.value) {
      if (!this.form.value.hasOwnProperty(key)) {
        break;
      }
      //array
      if (key.indexOf(MageFormComponent.SEPARATOR) === -1) {
        object[key] = this.form.value[key];
      } else {
        let keyParts: string[] = key.split(MageFormComponent.SEPARATOR);
        object = this.assignObjectByPath(object, keyParts, this.form.value[key]);
      }

    }

    // if(this.isAuthKeyNeeded) {
    //   params.append("authKey", this.authService.getToken());
    // }
    return object;
  }

  assignObjectByPath(obj: Object, keyPath: string[], value: string): Object {
    if (keyPath.length === 1) {
      obj[keyPath[0]] = value;
    } else {
      var key = keyPath.shift();
      obj[key] = this.assignObjectByPath(typeof obj[key] === 'undefined' ? {} : obj[key],
        keyPath, value);
    }

    return obj;
  }

  handleNonErrorResponse(data: Object) {
    this.successForm.emit(data);
  }

  handleError(data: ErrorResponse) {
    this.serverErrorMessage = ErrorResponse.getErrorMessage(data);
  }

  getFieldByName(name: string): Field<any> {
    for (let field of this.fields.toArray()) {
      if (field.getName() == name) {
        return field;
      }
    }
    return null;
  }

  processError(errors: Object) {
    for (let prop in errors) {
      let field: Field<any> = this.getFieldByName(prop);
      if (!field) {
        break;
      }
      if (errors[prop] instanceof Object) {
        field.setErrorMessage(errors[prop][0]);
      } else {
        field.setErrorMessage(errors[prop]);
      }
    }
  }

}
