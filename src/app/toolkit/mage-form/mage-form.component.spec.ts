import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MageFormComponent} from './mage-form.component';

describe('MageFormComponent', () => {
  let component: MageFormComponent;
  let fixture: ComponentFixture<MageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MageFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
