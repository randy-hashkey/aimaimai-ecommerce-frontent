import {AuthService} from './../../auth.service';
import {Component, ContentChild, EventEmitter, HostListener, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {Field} from '../field';
import {AppSettings} from '../../app.setting';

@Component({
  selector: 'app-dropdown2',
  templateUrl: './dropdown2.component.html',
  styleUrls: ['./dropdown2.component.scss']
})
export class Dropdown2Component extends Field<any> implements OnInit {

  @Input()
  placeholder: string;

  @Input()
  text: string = null;

  @Output()
  textChange: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  items: any[] = [];

  dropdownOpened: boolean = false;

  @Input()
  initFirstItem: boolean = false;

  @Input()
  valueIndex: string;

  @ContentChild(TemplateRef)
  public itemTemplate: TemplateRef<any>;

  constructor(private authService: AuthService) {
    super();
  }

  ngOnInit() {
    if (this.initFirstItem) {
      this.triggerDropdownItemClick(this.items[0], this.items[0].text);
    }
  }

  toggleDropdown(event: Event) {
    this.dropdownOpened = !this.dropdownOpened;
  }

  openDropdown(event: Event) {
    this.dropdownOpened = true;
    event.preventDefault();
  }

  closeDropdown(event: Event) {
    this.dropdownOpened = false;
    event.preventDefault();
  }

  triggerDropdownItemClick(item: any, text: string) {
    let value = this.valueIndex ? item[this.valueIndex] : item;
    this.setValue(value);
    this.setText(text);
    this.dropdownOpened = false;
  }

  setText(text: string) {
    this.text = text;
    this.textChange.emit(text);
  }

  @HostListener('document:click', ['$event'])
  handleDocumentClick(event: Event) {
    if (event.target && !(<HTMLElement>event.target).closest('.dropdown-wrapper')) {
      this.dropdownOpened = false;
    }
  }

  @HostListener('document:keydown', ['$event'])
  handleMousedown(event: KeyboardEvent) {
    if (event.keyCode == AppSettings.DOWN_KEY && this.dropdownOpened) {
    }
  }
}
