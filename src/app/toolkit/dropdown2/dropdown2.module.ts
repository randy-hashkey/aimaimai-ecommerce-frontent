import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Dropdown2Component} from './dropdown2.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [Dropdown2Component],
  exports: [Dropdown2Component]
})
export class Dropdown2Module {
}
