import {URLSearchParams} from '@angular/http';

export function convertObjectToURLSearchParams(object: any): URLSearchParams {
  let params: URLSearchParams = new URLSearchParams;
  for (let key in object) {
    params.append(key, object[key]);
  }
  return params;
}

export function convertDateTimeLocalToTimestamp(value: string): number {
  return (new Date(value.replace('T', ' ')).getTime()) / 1000;
}

// export function convertTimestampToDateTimeLocal(timestamp : number) : string {
//     return (new Date(timestamp)).format("Y-m-dTh:i");
// }
