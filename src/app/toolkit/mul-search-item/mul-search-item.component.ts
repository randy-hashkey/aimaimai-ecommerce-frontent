import {SearchItemResponse} from './../search/search.component';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mul-search-item',
  templateUrl: './mul-search-item.component.html',
  styleUrls: ['./mul-search-item.component.css']
})
export class MulSearchItemComponent implements OnInit {

  @Input()
  value: SearchItemResponse;

  @Output()
  remove: EventEmitter<SearchItemResponse> = new EventEmitter<SearchItemResponse>();

  constructor() {
  }

  ngOnInit() {
  }

  triggerRemoveEvent(event: Event) {
    this.remove.emit(this.value);
  }

}
