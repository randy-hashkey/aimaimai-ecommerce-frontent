import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MulSearchItemComponent} from './mul-search-item.component';

describe('MulSearchItemComponent', () => {
  let component: MulSearchItemComponent;
  let fixture: ComponentFixture<MulSearchItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MulSearchItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MulSearchItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
