import {AppSettings} from './../../app.setting';
import {Headers, Http, RequestOptions} from '@angular/http';
import {UploadImageResponse} from './upload-image.model';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class UploadImageService {

  readonly IMAGE_ID: string = '1';

  constructor(private http: Http) {
  }

  uploadImage(file: File, token: string): Observable<UploadImageResponse> {
    let formData: FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('typeId', this.IMAGE_ID);
    let headers = new Headers({
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});
    return this.http.post(AppSettings.UPLOAD_IMAGE, formData, options)
      .map((response) => response.json());
  }
}
