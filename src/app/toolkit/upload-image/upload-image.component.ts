import {FileModel} from './../toolkit.model';
import {UploadImageResponse} from './upload-image.model';
import {Router} from '@angular/router';
import {AuthService} from './../../auth.service';
import {UploadImageService} from './upload-image.service';
import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css'],
  providers: [UploadImageService]
})
export class UploadImageComponent implements OnInit {

  @Input()
  uploadDisabled: boolean = true;
  @ViewChild('input')
  inputElement: ElementRef;
  @Output()
  successUpload: EventEmitter<FileModel[]> = new EventEmitter<FileModel[]>();
  imageSrc: string = UploadImageComponent.NO_IMAGE_SRC;

  constructor(private uploadImageService: UploadImageService,
              private authService: AuthService,
              private elementRef: ElementRef,
              private router: Router) {
  }

  public static get NO_IMAGE_SRC(): string {
    return 'assets/camera.png';
  }

  ngOnInit() {
    if (!this.authService.loggedIn()) {
      this.router.navigate(['login']);
    }
  }

  upload(event: Event) {
    if (!this.uploadDisabled) {
      this.uploadImageService.uploadImage(this.inputElement.nativeElement.files[0],
        this.authService.getToken())
        .subscribe(this.handleUploadImageResponse.bind(this));
    }
  }

  handleUploadImageResponse(response: UploadImageResponse) {
    if (response.status) {
      this.successUpload.emit(response.files);
    }
    this.imageSrc = UploadImageComponent.NO_IMAGE_SRC;
  }

  displaySelectImage(event: Event) {
    this.inputElement.nativeElement.click();
  }

  readUrl(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.imageSrc = event.target.result;
        this.uploadDisabled = false;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

}
