import {FileModel} from './../toolkit.model';

export class UploadImageResponse {
  status: boolean;
  files: FileModel[];
}

