import {Headers, Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class Search2Service {

  constructor(private _http: Http) {
  }

  getData(api: string, query: string, token?: string): Observable<Array<any>> {
    let headers = new Headers({'Content-Type': 'application/json'});
    if (token) {
      headers.append('Authorization', 'Bearer ' + token);
    }
    let options = new RequestOptions({headers: headers});

    api = api.replace(':q', query ? query : '');

    return this._http.get(api, options).map((response) => response.json());
  }
}
