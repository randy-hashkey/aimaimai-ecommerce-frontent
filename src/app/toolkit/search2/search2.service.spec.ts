import {inject, TestBed} from '@angular/core/testing';

import {Search2Service} from './search2.service';

describe('Search2Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Search2Service]
    });
  });

  it('should be created', inject([Search2Service], (service: Search2Service) => {
    expect(service).toBeTruthy();
  }));
});
