import {AuthService} from './../../auth.service';
import {Search2Service} from './search2.service';
import {Component, ContentChild, EventEmitter, HostListener, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {Field} from './../field';

@Component({
  selector: 'app-search2',
  templateUrl: './search2.component.html',
  styleUrls: ['./search2.component.scss'],
  providers: [Search2Service]
})
export class Search2Component extends Field<string> implements OnInit {

  @Input()
  placeholder: string;

  @Input()
  items: Array<any> = [];

  @Input()
  authKeyNeeded: boolean = false;
  @Output()
  itemsChange: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();
  @Input()
  text: string;
  @Output()
  textChange: EventEmitter<string> = new EventEmitter<string>();
  @Input()
  url: string;
  @Input()
  textIndex: string = null;
  @Input()
  valueIndex: string = null;
  @Input()
  total: number = null;
  @Output()
  totalChange: EventEmitter<number> = new EventEmitter<number>();
  @Output()
  dataChange: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  needSearch: EventEmitter<string> = new EventEmitter<string>();
  @Input()
  enableDataChange: boolean = false;
  @ContentChild(TemplateRef)
  public itemTemplate: TemplateRef<any>;
  fetchingData: boolean = false;
  dataHasBeenFetched: boolean = false;

  constructor(private _searchService: Search2Service,
              private _authService: AuthService) {
    super();
  }

  get noResultFound(): boolean {
    if (!this.enableDataChange) {
      return this.total == 0 && this.total != null;
    }
    return this.total == 0 && this.total != null && this.dataHasBeenFetched;
  }

  isAuthKeyNeeded(): boolean {
    return this.authKeyNeeded;
  }

  ngOnInit() {
  }

  handleInputValueChange(text: string) {
    this.setText(text);
    if (this.enableDataChange) {
      this.getData();
    }
    this.needSearch.emit(text);
  }

  getData() {
    let token: string = (this.isAuthKeyNeeded()) ? this._authService.getToken() : null;
    this.fetchingData = true;
    this._searchService.getData(this.url, this.text, token).subscribe(
      this.handleGetData.bind(this)
    );

  }

  getDataKeys(): string[] {
    let values: string[] = [];
    this.items.forEach((itemReponse, index) => {
      values.push(itemReponse.value);
    });

    return values;
  }

  handleGetData(data: Array<any>) {
    this.fetchingData = false;
    this.dataHasBeenFetched = true;
    this.dataChange.emit(data);
  }

  triggerDropdownItemClick(selectedItem: any) {
    this.setValue(selectedItem[this.valueIndex]);
    this.setText(this.findItem(selectedItem[this.valueIndex])[this.textIndex]);
    this.clearData();
  }

  findItem(value): any {
    for (let item of this.items) {
      if (item[this.valueIndex] === value) {
        return item;
      }
    }
    return null;
  }

  clearData() {
    this.setItems([]);
  }

  refreshData(event: Event) {
    this.clearData();
    this.setValue(null);
    this.setText(null);
    if (this.enableDataChange) {
      this.getData();
    }
    this.needSearch.emit(this.text);
  }

  @HostListener('document:click', ['$event'])
  handleDocumentClick(event: Event) {
    if (event.target && !(<HTMLElement>event.target).closest('.search-dropdown-item')) {
      this.clearData();
      this.setTotal(null);
    }
  }

  setText(text: string) {
    this.text = text;
    this.textChange.emit(text);
  }

  setTotal(total: number) {
    this.total = total;
    this.totalChange.emit(total);
  }

  setItems(items: Array<any>) {
    this.items = items;
    this.itemsChange.emit(items);
  }

  // isDataDisabled(key : string) : boolean {
  //   return this.disabledData.indexOf(key) !== -1;
  // }

}
