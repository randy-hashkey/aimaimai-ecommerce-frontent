import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable()
export class FormService {

  constructor(private http: Http) {
  }

  requestPost(model: any, api: string): Observable<Object> {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    let options = new RequestOptions({headers: headers});
    return this.http.post(api, model, options)
      .map((response) => response.json())
      .catch(this.handleError);
  }

  requestPostWithAuthkey(model: any, api: string, token: string): Observable<Object> {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({headers: headers});
    return this.http.post(api, model, options)
      .map((response) => response.json())
      .catch(this.handleError);
  }

  handleError(error: Response) {
    (error);
    return Observable.throw(error.json().error || 'Server Error');
  }
}
