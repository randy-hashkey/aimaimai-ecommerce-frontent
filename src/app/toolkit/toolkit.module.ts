import {Search2Component} from './search2/search2.component';
import {CKEditorModule} from 'ng2-ckeditor';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputComponent} from './input/input.component';
import {FormComponent} from './form/form.component';
import {ButtonComponent} from './button/button.component';
import {TextareaComponent} from './textarea/textarea.component';
import {TableComponent} from './table/table.component';
import {ModalComponent} from './modal/modal.component';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {MatIconModule} from '@angular/material';
import {SearchComponent} from './search/search.component';
import {UploadImageComponent} from './upload-image/upload-image.component';
import {SelectImageComponent} from './select-image/select-image.component';
import {SelectImageFieldComponent} from './select-image-field/select-image-field.component';
import {QuantityFieldComponent} from './quantity-field/quantity-field.component';
import {TabsComponent} from './tabs/tabs.component';
import {TabsItemComponent} from './tabs-item/tabs-item.component';
import {PaginationComponent} from './pagination/pagination.component';
import {MageFormComponent} from './mage-form/mage-form.component';
import {MulSearchComponent} from './mul-search/mul-search.component';
import {MulSearchItemComponent} from './mul-search-item/mul-search-item.component';
import {DropdownComponent} from './dropdown/dropdown.component';
import {CheckboxComponent} from './checkbox/checkbox.component';
import {RadioComponent} from './radio/radio.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {CkeditorComponent} from './ckeditor/ckeditor.component';
import {DatepickerComponent} from './datepicker/datepicker.component';
import {AngularDateTimePickerModule} from 'angular2-datetimepicker';
import {UploadComponent} from './upload/upload.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, NgxPaginationModule,
    ReactiveFormsModule, HttpModule, MatIconModule,
    CKEditorModule,
    AngularDateTimePickerModule
  ],
  declarations: [InputComponent, FormComponent, ButtonComponent,
    TextareaComponent, TableComponent, ModalComponent, Search2Component,
    ConfirmDialogComponent, SearchComponent, UploadImageComponent, SelectImageComponent,
    SelectImageFieldComponent, QuantityFieldComponent, TabsComponent, TabsItemComponent,
    PaginationComponent, MageFormComponent, MulSearchComponent, MulSearchItemComponent,
    DropdownComponent, CheckboxComponent, RadioComponent, CkeditorComponent, DatepickerComponent,
    UploadComponent],
  exports: [InputComponent, FormComponent, ButtonComponent, DropdownComponent, CheckboxComponent,
    TextareaComponent, TableComponent, ModalComponent, SelectImageFieldComponent, QuantityFieldComponent,
    CkeditorComponent, Search2Component,
    TabsComponent, TabsItemComponent, MageFormComponent, MulSearchComponent, CheckboxComponent, RadioComponent,
    ConfirmDialogComponent, SearchComponent, UploadImageComponent, SelectImageComponent, DatepickerComponent,
    UploadComponent]
})
export class ToolkitModule {
}
