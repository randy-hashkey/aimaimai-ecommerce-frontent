import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {GetFilesByUserResponse} from './select-image.model';
import {Observable} from 'rxjs';
import {AppSettings} from './../../app.setting';
import {Injectable} from '@angular/core';

@Injectable()
export class SelectImageService {

  readonly IMAGE_ID: number = 1;

  constructor(private http: Http) {
  }

  getUserImage(token: string): Observable<GetFilesByUserResponse> {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({headers: headers});

    let params = new URLSearchParams();
    params.append('authKey', token);
    params.append('typeId', this.IMAGE_ID + '');
    return this.http.post(AppSettings.GET_USER_IMAGE, params).map((response) => response.json());
  }

  deleteImage(fileId: string, token: string) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + token
    });
    let options = new RequestOptions({headers: headers});

    let params = new URLSearchParams();
    params.append('fileId', fileId);
    return this.http.post(AppSettings.DELETE_FILE, params, options).map((response) => response.json());
  }
}
