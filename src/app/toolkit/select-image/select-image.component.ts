import {FileModel} from './../toolkit.model';
import {AppSettings} from './../../app.setting';
import {GetFilesByUserResponse, ImageResponse} from './select-image.model';
import {AuthService} from './../../auth.service';
import {SelectImageService} from './select-image.service';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-select-image',
  templateUrl: './select-image.component.html',
  styleUrls: ['./select-image.component.css'],
  providers: [SelectImageService]
})
export class SelectImageComponent implements OnInit {

  @Input()
  opened: boolean;

  @Output()
  openedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  models: FileModel[] = [];

  selectedModelIds: string[] = [];

  @Input()
  max: number = 10;

  @Output()
  select: EventEmitter<FileModel[]> = new EventEmitter<FileModel[]>();

  constructor(private selectImage: SelectImageService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.selectImage.getUserImage(this.authService.getToken())
      .subscribe(this.handleGetUserImage.bind(this));
  }

  handleGetUserImage(response: GetFilesByUserResponse) {
    this.models = [];
    this.models = response.files;
  }

  triggerOpenedChange(opened: boolean) {
    this.opened = opened;
    this.openedChange.emit(this.opened);
  }

  generateImagePath(path: string): string {
    return AppSettings.GET_IMAGE_PATH + path;
  }

  isSelected(id: string): boolean {
    return this.selectedModelIds.indexOf(id) !== -1;
  }

  selectOrDeselect(id: string) {
    let index = this.selectedModelIds.indexOf(id);
    if (index === -1) {
      this.addNewImage(id);
    } else {
      this.selectedModelIds.splice(index, 1);
    }
  }

  addNewImage(imageId: string) {
    this.selectedModelIds.push(imageId);

    if (this.selectedModelIds.length == (this.max + 1)) {
      this.selectedModelIds.splice(0, 1);
    }
  }

  removeImage(imageId: string) {
    this.selectImage.deleteImage(imageId, this.authService.getToken())
      .subscribe((response: ImageResponse) => {
        if (!response.status) {
          return;
        }
        this.models = this.models.filter((model: FileModel) => {
          return model.id != imageId;
        });
      });
  }

  handleSuccessUpload(models: FileModel[]) {
    for (let model of models) {
      this.models.push(model);
    }
  }

  handleSelect(event: Event) {
    let models: FileModel[] = [];

    for (let id of this.selectedModelIds) {
      models.push(this.getFileModelById(id));
    }
    this.selectedModelIds = [];
    this.triggerOpenedChange(false);
    this.select.emit(models);
  }

  getFileModelById(id: string): FileModel {
    for (let model of this.models) {
      if (model.id == id) {
        return model;
      }
    }

    return null;
  }

  hasSelected(): boolean {
    return this.selectedModelIds.length !== 0;
  }
}
