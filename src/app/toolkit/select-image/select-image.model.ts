import {FileModel} from './../toolkit.model';

export class GetFilesByUserResponse {
  status: number;

  files: FileModel[];
}

export class ImageResponse {
  imageId: string;
  status: number;
}

export class ImageApi {
  public static get DELETE_IMAGE_API(): string {
    return './api/file/default/delete';
  }
}
