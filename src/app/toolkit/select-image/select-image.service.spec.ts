import {inject, TestBed} from '@angular/core/testing';

import {SelectImageService} from './select-image.service';

describe('SelectImageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectImageService]
    });
  });

  it('should be created', inject([SelectImageService], (service: SelectImageService) => {
    expect(service).toBeTruthy();
  }));
});
