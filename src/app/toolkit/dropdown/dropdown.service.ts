import {DropdownResponse} from './../toolkit.model';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class DropdownService {

  constructor(private http: Http) {
  }

  getData(api: string, data?: object): Observable<DropdownResponse> {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({headers: headers});

    let searchParams = new URLSearchParams();
    for (let key in data) {
      searchParams.set(key, data[key]);
    }

    return this.http.get(api).map((response) => response.json());
  }
}
