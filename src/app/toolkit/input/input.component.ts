import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Field} from './../field';


@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InputComponent extends Field<string> implements OnInit {

  @Input()
  placeholder: string;

  @Input()
  type: string;

  @Input()
  inputClass: string = 'input full';

  @Input()
  disabled: boolean = false;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
