import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SelectImageFieldComponent} from './select-image-field.component';

describe('SelectImageFieldComponent', () => {
  let component: SelectImageFieldComponent;
  let fixture: ComponentFixture<SelectImageFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectImageFieldComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectImageFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
