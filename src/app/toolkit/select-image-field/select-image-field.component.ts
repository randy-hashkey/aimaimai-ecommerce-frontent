import {FileModel} from './../toolkit.model';
import {AppSettings} from './../../app.setting';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Field} from './../field';

@Component({
  selector: 'app-select-image-field',
  templateUrl: './select-image-field.component.html',
  styleUrls: ['./select-image-field.component.css']
})
export class SelectImageFieldComponent extends Field<string[]> implements OnInit {

  @Input()
  placeholder: string;

  @Input()
  type: string;

  selectImageOpened: boolean = false;

  @Input()
  models: FileModel[] = [];

  @Input()
  isSelectedImageShown: boolean = true;

  @Input()
  btnClass: string = 'btn btn-primary';

  @Input()
  btnText: string = 'Select Image';

  @Input()
  icon: string = null;

  @Input()
  max: number = 10;

  @Output()
  onImageSelect: EventEmitter<FileModel[]> = new EventEmitter<FileModel[]>();

  constructor() {
    super();
  }

  ngOnInit() {
  }

  openSelectImage(event: Event) {
    this.selectImageOpened = true;
  }

  handleSelectEvent(models: FileModel[]) {
    this.models = models;
    this.onImageSelect.emit(this.models);
  }

  getValue(): string[] {
    let values: string[] = [];
    for (let model of this.models) {
      values.push(model.id + '');
    }

    return values;
  }

  generateImagePath(path: string): string {
    return AppSettings.GET_IMAGE_PATH + path;
  }
}
