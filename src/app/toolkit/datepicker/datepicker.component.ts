import {Component, Input, OnInit} from '@angular/core';
import {Field} from '../field';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent extends Field<string> implements OnInit {

  value: any;

  @Input()
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd/MMM/yyyy',
    defaultOpen: false
  };

  constructor() {
    super();
    this.value = new Date();
  }

  ngOnInit() {
  }

  handleChange(event: Event) {
    this.onChange(event);
  }

}
