import {inject, TestBed} from '@angular/core/testing';

import {MulSearchService} from './mul-search.service';

describe('MulSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MulSearchService]
    });
  });

  it('should be created', inject([MulSearchService], (service: MulSearchService) => {
    expect(service).toBeTruthy();
  }));
});
