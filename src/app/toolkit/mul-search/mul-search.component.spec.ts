import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MulSearchComponent} from './mul-search.component';

describe('MulSearchComponent', () => {
  let component: MulSearchComponent;
  let fixture: ComponentFixture<MulSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MulSearchComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MulSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
