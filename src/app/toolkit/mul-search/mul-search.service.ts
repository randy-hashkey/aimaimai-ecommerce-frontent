import {SearchResponse} from './../search/search.component';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class MulSearchService {

  constructor(private http: Http) {
  }

  getData(api: string, query: string, data?: object): Observable<SearchResponse> {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({headers: headers});

    let searchParams = new URLSearchParams();
    for (let key in data) {
      searchParams.set(key, data[key]);
    }

    if (!query) {
      query = '';
    }
    api = api.replace(':q', query);

    return this.http.get(api).map((response) => response.json());
  }
}
