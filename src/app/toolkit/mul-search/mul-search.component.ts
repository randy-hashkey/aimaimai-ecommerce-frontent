import {SearchItemResponse, SearchResponse} from './../toolkit.model';
import {AuthService} from './../../auth.service';
import {MulSearchService} from './mul-search.service';
import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Field} from './../field';


@Component({
  selector: 'app-mul-search',
  templateUrl: './mul-search.component.html',
  styleUrls: ['./mul-search.component.css'],
  providers: [MulSearchService]
})
export class MulSearchComponent extends Field<string[]> implements OnInit {

  @Input()
  placeholder: string;

  @Input()
  url: string;

  @Input()
  isAuthKeyNeeded: boolean = true;

  data: SearchItemResponse[] = [];

  @Input()
  text: string = null;

  @Input()
  disabledData: string[] = [];

  selectedItems: SearchItemResponse[] = [];

  noResultFound: boolean = false;

  constructor(private searchService: MulSearchService, private authService: AuthService) {
    super();
  }

  ngOnInit() {
    this.value = [];
  }

  handleInputValueChange(event: Event) {
    this.getData();
  }

  getData() {
    let object: Object = {};

    if (this.isAuthKeyNeeded) {
      object['authKey'] = this.authService.getToken();
    }
    this.searchService.getData(this.url, this.text, object).subscribe(
      this.handleGetData.bind(this)
    );

  }

  getDataKeys(): string[] {
    let values: string[] = [];
    this.data.forEach((itemReponse, index) => {
      values.push(itemReponse.value);
    });

    return values;
  }

  handleGetData(response: SearchResponse) {
    this.data = response.data;
    this.noResultFound = this.data.length === 0;
  }

  triggerDropdownItemClick(value: string) {
    if (this.isDataDisabled(value)) {
      return;
    }

    let responseItem: SearchItemResponse = this.findItem(value);
    this.selectedItems.push(responseItem);
    this.text = null;
    this.clearData();
  }

  findItem(value): SearchItemResponse {
    for (let item of this.data) {
      if (item.value === value) {
        return item;
      }
    }

    return null;
  }

  clearData() {
    this.data = [];
  }

  refreshData(event: Event) {
    this.clearData();
    this.text = null;
    this.getData();
  }

  @HostListener('document:click', ['$event'])
  handleDocumentClick(event: Event) {
    if (event.target && !(<HTMLElement>event.target).closest('.search-dropdown-item')) {
      this.clearData();
      this.noResultFound = false;
    }
  }

  isDataDisabled(key: string): boolean {
    return this.disabledData.indexOf(key) !== -1;
  }

  handleRemoveEvent(item: SearchItemResponse) {
    for (let selectedItem of this.selectedItems) {
      if (selectedItem.value === item.value) {
        this.selectedItems.splice(this.selectedItems.indexOf(selectedItem), 1);
      }
    }
  }

  getValue(): string[] {
    let values: string[] = [];
    for (let selectedItem of this.selectedItems) {
      values.push(selectedItem.value);
    }
    return values;
  }
}

