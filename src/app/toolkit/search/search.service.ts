import {Headers, Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';
import {SearchResponse} from './search.component';
import {Injectable} from '@angular/core';

@Injectable()
export class SearchService {

  constructor(private http: Http) {
  }

  getData(api: string, query: string, token?: string): Observable<SearchResponse> {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    if (token) {
      headers.append('Authorization', 'Bearer ' + token);
    }
    let options = new RequestOptions({headers: headers});

    api = api.replace(':q', query ? query : '');

    return this.http.get(api, options).map((response) => response.json());
  }
}
