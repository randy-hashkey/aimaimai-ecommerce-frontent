export class ReferralLevelResponse {
  search: string;
  status: number;
  error: string;
  referralLevels: ReferralLevelModel;
  referralLevel: ReferralLevelContent;
}

export class ReferralLevelModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ReferralLevelContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ReferralLevelContent {
  id: number;
  level: number;
  currencyId: string;
  returnAmount: number;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class REFERRAL_LEVEL_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
