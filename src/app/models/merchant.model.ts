export class MerchantResponse {
  search: string;
  status: number;
  error: string;
  merchants: MerchantModel;
  merchant: MerchantContent;
}

export class MerchantModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: MerchantContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class MerchantContent {
  id: string;
  merchantId: string;
  name: string;
  description: string;
  chineseDescription: string;
  imageId: string;
  headerImageId: string;
  userId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  nric:string;
}

export class FilterMerchantContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class MERCHANT_PAGE {
  public static LIST = '/my-store';
  public static VIEW = '/my-store/get';
  public static LIST_PRODUCT = '/my-store/my-products';

}

export class MERCHANT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
