export class CurrencyGroupResponse {
  search: string;
  status: number;
  error: string;
  currencyGroups: CurrencyGroupModel;
  currencyGroup: CurrencyGroupContent;
}

export class CreateCurrencyGroupResponse {
  status: boolean;
  currencyGroup: CurrencyGroupContent;
}


export class CurrencyGroupModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: CurrencyGroupContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class CurrencyGroupContent {
  id: string;
  imageId: string;
  currencyCode: string;
  description: string;
  chineseDescription: string;
  webLink: string;
  whitePaperFile: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class FilterCurrencyGroupContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class CURRENCY_GROUP_PAGE {
  public static LIST = '/pages/currencygroup';
  public static VIEW = '/pages/currencygroup' + '/view';
}

export class CURRENCY_GROUP_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
