export class CategoryResponse {
  search: string;
  status: number;
  error: string;
  categories: CategoryModel;
  category: CategoryContent;
}

export class CategoryModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: CategoryContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class CategoryContent {
  name: string;
  chineseName: string;
  categoryName: string;
  description: string;
  chineseDescription: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  iconImageId: string;
}

export class FilterCategoryContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class CATEGORY_PAGE {
  public static LIST = '/pages/product-category';
  public static VIEW = '/pages/product-category/view';
}

export class CATEGORY_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
