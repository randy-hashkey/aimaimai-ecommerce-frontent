export class SliderResponse {
  search: string;
  status: number;
  error: string;
  sliders: SliderModel;
  slider: SliderContent;
}

export class SliderModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: SliderContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class SliderContent {
  title: string;
  chineseTitle: string;
  description: string;
  chineseDescription: string;
  url: string;
  imageId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterSliderContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class SLIDER_PAGE {
  public static LIST = '/pages/slider';
  public static VIEW = '/pages/slider/view';
}

export class SLIDER_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
