export class CreatePaypalResponse {
  redirect_url: string;
  status: string;
}


export class OrderPaymentResponse {
  error: string;
  orderPayment: OrderPaymentContent;
}

export class OrderPaymentContent {
  createdAt: Date;
  expiryDate: Date;
  orderId: string;
  paymentMethod: string;
  status: number;
  totalCost: number;
  updatedAt: Date;
}


export class PAYMENT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'CANCEL';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'SUCCESS';
    }
  }
}

