export class SpecialCategoryResponse {
  search: string;
  status: number;
  error: string;
  specialCategories: SpecialCategoryModel;
  specialCategory: SpecialCategoryContent;
}

export class SpecialCategoryModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: SpecialCategoryContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class SpecialCategoryContent {
  id: string;
  keyword: string;
  category: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterSpecialCategoryContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class SPECIAL_CATEGORY_PAGE {
  public static LIST = '/pages/special-category';
}

export class SPECIAL_CATEGORY_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
