export class ProductAttributeResponse {
  search: string;
  status: number;
  error: string;
  productAttributes: ProductAttributeModel;
  productAttribute: ProductAttributeContent;


  attributes: ProductAttributeContent[] = [];
}

export class ProductAttributeModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ProductAttributeContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ProductAttributeContent {
  productId: string;
  attributeName: string;
  attributeValue: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  additionalPrice:number;
  id:string;

  productAttributeId:string;
}

export class FilterProductAttributeContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class PRODUCT_PAGE {
  public static LIST = '/pages/product-attribute';
  public static VIEW = '/pages/product-attribute/view';
}

export class PRODUCT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}


// export class ProductAttribute {
//   productId: string;
//   attributeName: string;
//   attributeValue: string;
// }


// export class AttributeProductModel {
//   id: string;
//   attributeName: string;
//   productId: string;
//   attributeValue: string;
//   quantity: number;
//   additionalPrice: number;
//   status: number;
//   createdAt: Date;
//   updatedAt: Date;
// }
