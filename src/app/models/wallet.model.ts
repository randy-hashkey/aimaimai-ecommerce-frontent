export class AdjustmentForm {
  currencyId: string;
  userId: string;
  amount: number;
}

export class WalletResponse {
  search: string;
  status: number;
  error: string;
  credit: WalletContent;
}

export class WalletContent {
  id: string;
  userId: string;
  totalCredit: number;
  currencyId: string;
  activityType: string;
  adjustmentValue: string;
  note: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class WalletModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: WalletContent[] = [];
}


export class GET_CURRENCY {
  public static CASH = "cash";
  public static POINT = "point";
  public static CREDIT = "credit";
}
