import {UserContent} from './user.model';

export class CurrencyResponse {
  search: string;
  status: number;
  error: string;
  currencies: CurrencyModel;
  currency: CurrencyContent;
}

export class CreateCurrencyResponse {
  status: boolean;
  currency: CurrencyContent;
}


export class CurrencyModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: CurrencyContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class CurrencyContent {
  id: string;
  name: string;
  userId: string;
  currencyGroupId: string;
  purchasableMode: boolean;
  mainCurrency: boolean;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  user: UserContent;
  chineseName: string;
}


export class FilterCurrencyContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class CURRENCY_PAGE {
  public static LIST = '/pages/currency';
  public static VIEW = '/pages/currency' + '/view';
}

export class CURRENCY_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
