export class ItemResponse {
  search: string;
  status: number;
  error: string;
  ecommerceItems: ItemModel;
  ecommerceItem: ItemContent;
}


export class ItemModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ItemContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ItemContent {
  id: string;
  categoryId: string;
  currencyId: string;
  infoId: string;
  imageId: string;
  name: string;
  price: number;
  stock: number;
  shortDescription: string;
  longDescription: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class FilterInfoContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class ITEM_PAGE {
  public static LIST = '/pages/ecommerce/product';
}

export class ITEM_STATUS {
  public static getStatus(status: number) {
    if (status == 9) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
    if (status == 11) {
      return 'REJECTED';
    }
    if (status == 0) {
      return 'DELETED';
    }
  }
}
