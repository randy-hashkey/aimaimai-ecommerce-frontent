export class InfoResponse {
  search: string;
  status: number;
  error: string;
  ecommerceInfos: InfoModel;
  ecommerceInfo: InfoContent;
}

export class CreateInfoResponse {
  status: boolean;
}

export class InfoModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: InfoContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class InfoContent {
  id: string;
  merchantId: string;
  merchantName: string;
  bannedNote: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class FilterInfoContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class INFO_PAGE {
  public static LIST = '/pages/ecommerce/merchant';
}

export class INFO_STATUS {
  public static getStatus(status: number) {
    if (status == 9) {
      return 'NOT_VERIFIED';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
    if (status == 12) {
      return 'BANNED';
    }
  }
}
