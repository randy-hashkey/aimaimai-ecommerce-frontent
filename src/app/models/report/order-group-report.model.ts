export class OrderGroupResponse {
  search: string;
  status: number;
  error: string;
  orderMerchantGroups: orderGroupModel;
}

export class orderGroupModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: orderMerchantGroupContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class orderMerchantGroupContent {
  id: string;
  orderId: string;
  merchantId: string;
  shipmentMethodId: string;
  shipmentCost: string;
  totalProductCost: string;
  telephone: string;
  shippingStreetName: string;
  shippingUnitNumber: string;
  shippingCity: string;
  shippingCountry: string;
  shippingPostalCode: string;
  billingStreetName: string;
  billingUnitNumber: string;
  billingCity: string;
  billingCountry: string;
  billingPostalCode: string;
  shippingAddressDetail: string;
  billingAddressDetail: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  orderItems: []
}
