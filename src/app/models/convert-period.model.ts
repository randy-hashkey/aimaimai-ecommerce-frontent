export class ConvertPeriodResponse {
  search: string;
  status: number;
  error: string;
  convertPeriods: ConvertPeriodModel;
  convertPeriod: ConvertPeriodContent;
}

export class ConvertPeriodModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ConvertPeriodContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ConvertPeriodContent {
  id: string;
  currencyId: string;
  sourceCurrencyId: string;
  targetCurrencyId: string;
  totalConvertedRate: number;
  periodInSecond: number;
  transactionFee: number;
  relatedCurrencyGroupId: string;
  reward: boolean;
  rate: number;
  transactionFeeCurrencyId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class CONVERT_RATE_PAGE {
  public static LIST = '/pages/convertrate';
}

export class CONVERT_PERIOD_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
