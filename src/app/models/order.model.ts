import {RuleContent} from './rule.model';
import {OrderPaymentContent} from './order.payment.model';

export class OrderResponse {
  search: string;
  status: number;
  error: string;
  orders: OrderModel;
  order: OrderContent;
  list: OrderContent[];
}

export class OrderModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: OrderContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class OrderContent {
  id: string;
  paymentMethodId: string;
  customerId: string;
  customerName: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  orderMerchantGroups: orderMerchantGroupContent[] = [];
  error: string;
  orderPayment: OrderPaymentContent;
  rules: RuleContent[] = [];
  taxCost: number;
}

export class orderMerchantGroupContent {
  id: string;
  orderId: string;
  paymentMethodId: string;
  shipmentMethodId: string;
  shipmentCost: number;
  txCost: number;
  totalProductCost: number;

  telephone: string;
  shippingStreetName: string;
  shippingUnitNumber: string;
  shippingCity: string;
  shippingCountry: string;
  shippingPostalCode: string;
  billingStreetName: string;
  billingUnitNumber: string;
  billingCity: string;
  billingCountry: string;
  billingPostalCode: string;
  merchantId: string;
  merchantName: string;
  shippingAddressDetail: string;
  billingAddressDetail: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  orderItems: OrderItemContent[] = [];
}

export class OrderItemContent {
  id: string;
  merchantGroupId: string;
  chineseProductName: string;
  productId: string;
  productName: string;
  quantity: number;
  price: number;
  imageId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;

  orderItemAttributes = [];
}

export class FilterOrderContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class ORDER_PAGE {
  public static LIST = '/pages/order';
  public static VIEW = '/pages/order/view';
}

export class ORDER_STATUS {
  public static getOrderStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 2) {
      return 'CHECKOUT';
    }
    if (status == 3) {
      return 'PENDING PAYMENT';
    }
    if (status == 4) {
      return 'PAID';
    }
    // if (status == 5) return 'COMPLETE';
    // if (status == 12) return 'FINISH';
  }

  public static getGroupStatus(status: number) {
    if (status == 0) {
      return 'REJECT';
    }
    if (status == 1) {
      return 'CANCEL';
    }
    if (status == 2) {
      return 'BOOKING';
    }
    if (status == 3) {
      return 'SENDING';
    }
    if (status == 4) {
      return 'FINISH';
    }
  }

  public static getGroupItemStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 10) {
      return 'BOOKING';
    }
  }
}
