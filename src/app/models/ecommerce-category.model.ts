export class CategoryResponse {
  search: string;
  status: number;
  error: string;
  ecommerceCategories: CategoryModel;
  ecommerceCategory: CategoryContent;
}

export class CreateCategoryResponse {
  status: boolean;
  ecommerceCategory: CategoryContent;
}

export class CategoryModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: CategoryContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class CategoryContent {
  id: string;
  name: string;
  description: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class FilterCategoryContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class CATEGORY_PAGE {
  public static LIST = '/pages/ecommerce/category/';
}

export class CATEGORY_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
