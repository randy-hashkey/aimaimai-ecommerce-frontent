export class TaxResponse {
  search: string;
  status: number;
  error: string;
  tax: TaxContent;
}

export class TaxContent {
  id: string;
  chargeableMax: number;
  rate: number;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class TAX_PAGE {
  public static LIST = '/pages/tax';
  public static VIEW = '/pages/tax/view';
}
