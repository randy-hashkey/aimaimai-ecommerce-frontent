export class ConvertRateResponse {
  search: string;
  status: number;
  error: string;
  convertRates: ConvertRateModel;
  convertRate: ConvertRateContent;
}

export class ConvertRateModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ConvertRateContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ConvertRateContent {
  id: string;
  currencyId: string;
  sourceCurrencyId: string;
  targetCurrencyId: string;
  rate: string;
  transactionFee: string;
  transactionFeeCurrencyId: string;
  minimumConversionAmount: boolean;
  status: number;
  createdAt: Date;
  type: number;
  updatedAt: Date;
}


export class FilterConvertRateContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}


export class CONVERT_RATE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}


export class CONVERT_RATE_TYPE {
  public static getStatus(status: number) {
    if (status == 1) {
      return 'PURCHASABLE';
    }
    if (status == 2) {
      return 'TRANSACTION COST';
    }
    if (status == 3) {
      return 'ECOMMERCE BASE';
    }
    if (status == 4) {
      return 'STACKING';
    }
    if (status == 5) {
      return 'IEO';
    }
  }

}
