export class FacebookContent {

  id: string;
  first_name: string;
  last_name: string;
  name: string;
  email: string;
  picture: Pictures;
  error: string;
  token: string;
  authKey: string;
}

export class Pictures {
  data: PictureAttribute;
}

export class PictureAttribute {
  height: number;
  width: number;
  url: string;
  _silhouette: boolean;

}
