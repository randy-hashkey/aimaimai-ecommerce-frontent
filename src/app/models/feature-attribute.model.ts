export class FeatureAttributeResponse {
  search: string;
  status: number;
  error: string;
  featureAttributes: FeatureAttributeModel;
  featureAttribute: FeatureAttributeContent;
}

export class FeatureAttributeModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: FeatureAttributeContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class FeatureAttributeContent {
  attributeName: string;
  attributeValue: string;
  chineseAttributeValue: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterFeatureAttributeContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class FEATURE_ATTRIBUTE_PAGE {
  public static LIST = '/pages/feature-attribute';
  public static VIEW = '/pages/feature-attribute/view';
}

export class FEATURE_ATTRIBUTE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
