export class EmployeeResponse {
  search: string;
  status: number;
  error: string;
  employeeMerchants: EmployeeModel;
  employeeMerchant: EmployeeContent;
}

export class EmployeeModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: EmployeeContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class EmployeeContent {
  id: string;
  merchantId: string;
  userId: string;
  userStatus: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  email: string;
}


export class EMPLOYEE_PAGE {
  public static LIST = '/pages/employee';
}

export class EMPLOYEE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
