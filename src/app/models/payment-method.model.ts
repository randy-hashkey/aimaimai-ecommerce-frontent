export class PaymentMethodResponse {
  search: string;
  status: number;
  error: string;
  paymentMethods: PaymentMethodModel;
  paymentMethod: PaymentMethodContent;
}

export class PaymentMethodModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: PaymentMethodContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class PaymentMethodContent {
  name: string;
  description: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterPaymentMethodContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class PAYMENT_METHOD_PAGE {
  public static LIST = '/pages/payment-method';
  public static VIEW = '/pages/payment-method/view';
}

export class PAYMENT_METHOD_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'HIDE';
    }
    if (status == 11) {
      return 'HIDE';
    }
    if (status == 10) {
      return 'SHOW';
    }
  }
}
