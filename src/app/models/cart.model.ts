import {RuleContent} from './rule.model';
import {ProductAttributeContent} from './product-attribute.model';

export class CartResponse {
  cart: Cart;
  error: string;
}

export class Cart {
  id: string;
  customerId: string;
  cartItems: CartItem[] = [];
  rules: RuleContent[] = [];

  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class CartItem {

  cartId: string;
  productId: string;

  id: string;
  merchantId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;

  quantity: number;

  note:string;

  price:number;
  cartItemAttributes: ProductAttributeContent[] = [];

}

// export class cartItemAttribute {
//   quantity: number;
//   id: string;
//   status: number;
//   createdAt: Date;
//   updatedAt: Date;
//
//   cartItemId: string;
//   productAttributeId: string;
//
//   additionalPrice: number;
// }
//
