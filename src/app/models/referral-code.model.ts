export class ReferralCodeResponse {
  search: string;
  status: number;
  error: string;
  referralCodes: ReferralCodeModel;
  referralCode: ReferralCodeContent;
}

export class ReferralCodeModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ReferralCodeContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ReferralCodeContent {
  id: string;
  userId: string;
  code: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  error;
}
