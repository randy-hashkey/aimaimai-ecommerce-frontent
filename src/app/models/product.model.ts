import {Cart} from './cart.model';
import {ProductAttributeContent} from './product-attribute.model';

export class ProductResponse {
  search: string;
  status: number;
  error: string;
  products: ProductModel;
  product: ProductContent;
}

export class ProductModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ProductContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ProductContent {
  id: string;
  name: string;
  chineseName: string;
  price: number;
  merchantId: string;
  description: string;
  chineseDescription: string;
  quantity: number;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  categories = [];
  shipments = [];
  images = [];
  attributes = [];
  discount: ProductDiscountContent;


  cartItemAttributes: ProductAttributeContent[] = [];
}

export class ProductDiscountContent {
  productId: string;
  discount: number;
  period: Date;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  merchantId: string;
}

export class FilterProductContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class PRODUCT_PAGE {
  public static LIST = '/pages/product';
}

export class PRODUCT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'PUBLISH';
    }
  }
}
