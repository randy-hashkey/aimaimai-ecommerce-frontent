export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}


export class Authorities {
  authority: string;
}
