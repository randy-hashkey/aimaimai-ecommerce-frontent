export class ShipmentMethodResponse {
  search: string;
  status: number;
  error: string;
  shipmentMethods: ShipmentMethodModel;
  shipmentMethod: ShipmentMethodContent;
}

export class ShipmentMethodModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ShipmentMethodContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ShipmentMethodContent {
  name: string;
  cost: number;
  description: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterShipmentMethodContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class SHIPMENT_METHOD_PAGE {
  public static LIST = '/pages/shipment-method';
  public static VIEW = '/pages/shipment-method/view';
}

export class SHIPMENT_METHOD_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}

export class MerchantShipmentMethod {
  merchantGroupId: string;
  shipmentMethodId: string;
  cost: number;
  addressId: string;
}
