import {PageableModel, SortModel} from './currency.model';

export class LoginModel {
  email: string;
  password: string;
}

export class RegisterModel {
  email: string;
  verifyEmail: string;
  firstName: string;
  lastName: string;
  password: string;
  verifyPassword: string;
  referralCode: string;
}

export class LoginResponse {
  error: string;
  password: string;
  phone: string;
  phoneCode: string;
  status: boolean;
  user: UserContent;
}

export class UserResponse {
  search: string;
  status: number;
  error: string;
  user: UserContent;
  users: UserModel;
}

export class UserModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: UserContent[] = [];
}

export class UserContent {
  id: string;
  phone: string;
  authKey: string;
  passwordResetToken: string;
  validationToken: string;
  resetToken: string;
  enabled: boolean;
  firstName: string;
  lastName: string;
  password: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  authorities: Authorities[];
  accountNonExpired: boolean;
  accountNonLocked: boolean;
  credentialsNonExpired: boolean;
  username: string;
  photoFileId: string;
  backICFileId: string;
  frontICFileId: string;
}

export class Authorities {
  authority: string;
}

export class FilterUserContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class USER_PAGE {
  public static LIST = '/pages/user';
}

export class UserStatus {
  public static STATUS_UNVERIFIED: number = 10;
  public static STATUS_DELETED: number = 0;
  public static STATUS_APPROVED: number = 2;

}


export class SendValidationTokenForm {
  public phoneCode: string;

  public phone: string;

  public error?: string;
}
