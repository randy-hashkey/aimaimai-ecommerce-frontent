export class AttributeResponse {
  search: string;
  status: number;
  error: string;
  attributes: AttributeModel;
  attribute: AttributeContent;
}

export class AttributeModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: AttributeContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class AttributeContent {
  name: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  error: string;
}

export class FilterAttributeContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class ProductAttribute {
  productId: string;
  attributeName: string;
  attributeValue: string;
  quantity: number;
  additionalPrice: number;
  image: string;
  merchantId: string;
}

export class ATTRIBUTE_PAGE {
  public static LIST = '/pages/product-attribute';
  public static VIEW = '/pages/product-attribute/view';
}

export class ATTRIBUTE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
