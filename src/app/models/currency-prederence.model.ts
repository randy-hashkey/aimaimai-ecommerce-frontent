export class CurrencyPreferenceResponse {
  search: string;
  status: number;
  error: string;
  currencyPreferences: CurrencyPreferenceModel;
  currencyPreference: CurrencyPreferenceContent;
}

export class CurrencyPreferenceModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: CurrencyPreferenceContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class CurrencyPreferenceContent {
  id: string;
  userId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  currencyGroupId: string;
}


export class FilterCurrencyPreferenceContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class CURRENCY_PREFERENCE_PAGE {
  public static LIST = '/pages/currencypreference';
  public static VIEW = '/pages/currencyprepference' + '/view';
}

export class CURRENCY_PREFERENCE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
