export class PromoResponse {
  search: string;
  status: number;
  error: string;
  promos: PromoModel;
  promo: PromoContent;
}

export class PromoModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: PromoContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class PromoContent {
  id: string;
  imageId: string;
  title: string;
  chineseTitle: string;
  description: string;
  chineseDescription: string;
  specialCategoryId: string;
  period: Date;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterPromoContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class PROMO_PAGE {
  public static LIST = '/pages/promo';
}

export class PROMO_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
