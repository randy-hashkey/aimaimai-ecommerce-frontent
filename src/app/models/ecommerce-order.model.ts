// export class OrderResponse {
//   search: string;
//   status: number;
//   error: string;
//   ecommerceOrders: OrderModel;
//   ecommerceOrder: OrderContent;
// }
//
// export class OrderModel {
//   last: boolean;
//   totalPages: number;
//   totalElements: number;
//   numberOfElements: number;
//   first: boolean;
//   size: number;
//   number: number;
//   sort: SortModel;
//   pageable: PageableModel;
//   content: OrderContent[] = [];
// }
//
// export class SortModel {
//   sorted: boolean;
//   unsorted: boolean;
// }
//
// export class PageableModel {
//   sort: SortModel;
//   pageSize: number;
//   pageNumber: number;
//   offset: number;
//   paged: boolean;
//   unpaged: boolean;
// }
//
// export class OrderContent {
//   id: string;
//   buyerId: string;
//   ecommerceItemId: string;
//   currencyId: string;
//   quantity: number;
//   price: number;
//   buyerNote: string;
//   status: number;
//   createdAt: Date;
//   updatedAt: Date;
// }
//
//
// export class FilterOrderContent {
//   status: number;
//   search: string;
//   sorting: string;
//   asc: boolean;
// }
//
// export class ORDER_PAGE {
//   public static LIST = '/pages/ecommerce/order/';
// }
//
// export class ORDER_STATUS {
//   public static getStatus(status: number) {
//     if (status == 9) return 'PENDING';
//     if (status == 10) return 'CONFIRMED';
//     if (status == 11) return 'SUCCESS';
//     if (status == 12) return 'REJECTED';
//   }
// }
