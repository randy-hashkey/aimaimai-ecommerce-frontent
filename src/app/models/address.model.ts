export class AddressResponse {
  search: string;
  status: number;
  error: string;
  addresses: AddressContent[] = [];
  address: AddressContent;
}

export class AddressModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: AddressContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class AddressContent {
  id: string;
  city: string;
  country: number;
  detail: string;
  latitude: number;
  longitude: number;
  name: string;
  status: number;
  streetName: string;
  unitNumber: number;
  postalCode: string;
}

export class ADDRESS_PAGE {
  public static LIST = '/profile';
  public static VIEW = '/profile/get/';
}

export class ADDRESS_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
