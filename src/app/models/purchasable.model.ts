import {PageableModel, SortModel} from './convert-rate.model';

export class AdminPurchaseModel {
  currencyId: string;
  amount: number;
  convertRateId: string;
}

export class PurchaseResponse {
  search: string;
  status: number;
  amount: number;
  error: string;
  purchasables: PurchaseModel;
}

export class PurchaseModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: PurchaseContent[] = [];
}

export class PurchaseContent {
  id: string;
  totalPurchasableCreditLeft: number;
  activityType: string;
  createdAt: number;
  updatedAt: number;
  currencyId: string;
  userId: string;
  adjustmentValue: number;
}
