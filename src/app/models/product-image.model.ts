export class ProductImageResponse {
  search: string;
  status: number;
  error: string;
  images: ProductImageContent[];
  productImage: ProductImageContent;
}

export class ProductImageModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ProductImageContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ProductImageContent {
  id: string;
  imageId: string;
  productId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  merchantId: string;
}

export class FilterProductImageContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}


export class PRODUCT_IMAGE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
