export class SubscribeResponse {
  search: string;
  status: number;
  error: string;
  subscribes: SubscribeContent[] = [];
  subscribe: SubscribeContent;
  exist: boolean;
}

export class SubscribeModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: SubscribeContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class SubscribeContent {
  id: string;
  email: string;
}

export class SUBSCRIBE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
