import {ConvertRateContent, SortModel} from './convert-rate.model';
import {PageableModel} from './currency.model';

export class IeoContent {
  id: string;
  currencyGroupId: string;
  usedCurrencyId: string;
  targetCurrencyId: string;
  name: string;
  description: string;
  numberOfPurchased: number;
  startedAt: Date;
  closedAt: Date;
  status: number;
  roundCapAmount: number;
  createdAt: Date;
  updatedAt: Date;
}

export class ConvertRateIeoModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ConvertRateIeoContent[] = [];
}


export class IeoResponse {
  search: string;
  status: number;
  error: string;
  ieo: IeoContent;
  ieos: IeoModel;
  convertRates: ConvertRateIeoModel;
}

export class IeoModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: IeoContent[] = [];
}

export class ConvertRateIeoContent {
  id: string;
  ieoId: string;
  convertRateId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  convertRate: ConvertRateContent;
}

export class IEO_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
