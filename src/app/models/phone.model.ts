import {PageableModel, SortModel} from './currency.model';


export class PhoneContent {
  id: string;
  phone: string;
  phoneCode: string;
  userId: string;
}

export class PhoneModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: PhoneContent[] = [];
}

export class PhoneResponse {

  phoneAuthenticationPage: PhoneModel;

}
