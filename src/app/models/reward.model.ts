export class CurrencyRewardResponse {
  userOd: string;
  status: number;
  error: string;
  currencyRewards: CurrencyRewardModel;
  currencyReward: CurrencyRewardContent;
}

export class CurrencyRewardModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: CurrencyRewardContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class CurrencyRewardContent {
  id: string;
  userId: string;
  currencyId: string;
  deadline: Date;
  amount: number;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}


export class FilterCurrencyRewardContent {
  userId: string;
  status: number;
}

export class CURRENCY_REWARD_PAGE {
  public static LIST = '/pages/currencyreward';
  public static VIEW = '/pages/currencyreward' + '/view';
}

export class CURRENCY_REWARD_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
