export class RelationResponse {
  search: string;
  status: number;
  error: string;
  referralRelations: RelationContent[];
  referralRelation: RelationContent;
}

export class RelationModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: RelationContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class RelationContent {
  id: string;
  parentUserId: string;
  childUserId: string;
  usedCode: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
  sourceParentUserId: string;
  relationContents = [];
}


export class FilterConvertRateContent {
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}


export class CONVERT_RATE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
