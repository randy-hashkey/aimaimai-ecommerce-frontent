export class OrderGroupResponse {
  orderMerchantGroups: orderMerchantGroups;
  orderMerchantGroup: orderMerchantGroupContent;
  error: string;
}

export class orderMerchantGroups {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: orderMerchantGroupContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}


export class orderMerchantGroupContent {
  id: string;
  orderId: string;
  paymentMethodId: string;
  shipmentMethodId: string;
  shipmentCost: number;
  txCost: number;
  totalProductCost: number;

  telephone: string;
  shippingStreetName: string;
  shippingUnitNumber: string;
  shippingCity: string;
  shippingCountry: string;
  shippingPostalCode: string;
  billingStreetName: string;
  billingUnitNumber: string;
  billingCity: string;
  billingCountry: string;
  billingPostalCode: string;
  merchantId: string;
  merchantName: string;
  shippingAddressDetail: string;
  billingAddressDetail: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  orderItems: OrderItemContent[] = [];
}

export class OrderItemContent {
  id: string;
  merchantGroupId: string;
  chineseProductName: string;
  productId: string;
  productName: string;
  quantity: number;
  price: number;
  imageId: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterOrderContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class ORDER_PAGE {
  public static LIST = '/pages/order';
  public static VIEW = '/pages/order/view';
}

export class PRODUCT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
