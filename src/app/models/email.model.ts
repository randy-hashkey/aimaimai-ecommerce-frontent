export class EmailContent {
  id: string;
  userId: string;
  validationToken: string;
  resetToken: string;
  passwordResetToken: string;
  primaryEmail: boolean;
  email: string;
  valid: boolean;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class EmailResponse {
  error: string;
  userEmailAuthentication: EmailContent;
  userEmailAuthentications: EmailContent[];

}
