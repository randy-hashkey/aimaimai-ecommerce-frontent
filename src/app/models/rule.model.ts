export class RuleResponse {
  search: string;
  status: number;
  error: string;
  rules: RuleModel;
  rule: RuleContent;
}

export class RuleModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: RuleContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class RuleContent {
  couponCode: string;
  name: string;
  discount: number;
  expiryDate: Date;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterRuleContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}


export class RULE_PAGE {
  public static LIST = '/pages/rule';
  public static VIEW = '/pages/rule/view';
}

export class RULE_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
