export class ProductCategoryResponse {
  search: string;
  status: number;
  error: string;
  products: ProductCategoryModel;
  product: ProductCategoryContent;
}

export class ProductCategoryModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ProductCategoryContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ProductCategoryContent {
  productId: string;
  productName: string;
  categoryId: string;
  categoryName: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterProductCategoryContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class PRODUCT_PAGE {
  public static LIST = '/pages/product-category';
  public static VIEW = '/pages/product-category/view';
}

export class PRODUCT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 1) {
      return 'REJECT';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
