export class ContactResponse {
  search: string;
  status: number;
  error: string;
  contacts: ContactContent[] = [];
  contact: ContactContent;
}

export class ContactModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: ContactContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class ContactContent {
  id: string;
  name: string;
  email: string;
  message: string;
}

export class CONTACT_PAGE {
}

export class CONTACT_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 10) {
      return 'ACTIVE';
    }
  }
}
