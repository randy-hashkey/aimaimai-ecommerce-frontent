export class CodeResponse {
  userId: string;
  status: number;
  code: number;
  error: string;
  referralCode: CodeContent;
}

export class CodeContent {
  id: string;
  userId: string;
  code: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}
