export class FileModel {
  id: string;
  userId: string;
  path: string;
  status: number;
  createdAt: number;
  updatedAt: number;
  typeId: number;
  private: boolean;
  listed: boolean;
}

export class FileResponse {
  files: FileModel[];

}
