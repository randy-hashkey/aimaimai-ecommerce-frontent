export class OrderItemResponse {
  search: string;
  status: number;
  error: string;
  orderItems: OrderItemModel;
  orderItem: OrderItemContent;
}

export class OrderItemModel {
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
  sort: SortModel;
  pageable: PageableModel;
  content: OrderItemContent[] = [];
}

export class SortModel {
  sorted: boolean;
  unsorted: boolean;
}

export class PageableModel {
  sort: SortModel;
  pageSize: number;
  pageNumber: number;
  offset: number;
  paged: boolean;
  unpaged: boolean;
}

export class OrderItemContent {
  id: string;
  name: string;
  chineseName: string;
  description: string;
  chineseDescription: string;
  merchantId: string;
  price: number;
  quantity: number;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}

export class FilterOrderItemContent {
  fromDate: Date;
  endDate: Date;
  status: number;
  search: string;
  sorting: string;
  asc: boolean;
}

export class ORDER_ITEM_PAGE {
  public static LIST = '/pages/product';
}

export class ORDER_STATUS {
  public static getStatus(status: number) {
    if (status == 0) {
      return 'DELETE';
    }
    if (status == 11) {
      return 'PENDING';
    }
    if (status == 10) {
      return 'APPROVED';
    }
  }
}
