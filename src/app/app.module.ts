import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpModule} from '@angular/http';
import {routing} from './app.routing';
import {SharedModule} from './shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppGuard} from './app.guard';
import {AuthService} from './auth.service';
import {ToolkitModule} from './toolkit/toolkit.module';
import {NgbModalModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TokenInterceptor} from './token.interceptor';
import {FormsModule} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {ProductInfoModule} from './pages/product-info/product-info.module';
import {TranslateModule} from '@ngx-translate/core';
import {ToastrModule} from 'ngx-toastr';

import {SlickModule} from 'ngx-slick';
import {FileService} from './file.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    HttpModule,
    BrowserAnimationsModule,
    routing,
    HttpClientModule,
    SharedModule,
    ToolkitModule,
    NgbModule,
    NgbModalModule,
    FormsModule,
    ProductInfoModule,
    TranslateModule.forRoot(),
    SlickModule.forRoot(),
  ],
  providers: [AppGuard, AuthService, CookieService, FileService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent],
  // entryComponents:[AddMerchantEmployeeComponent]
})
export class AppModule {
}

