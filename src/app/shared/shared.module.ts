import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ErrorComponent} from './error/error.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MobileNavigasiComponent} from './mobile-navigasi/mobile-navigasi.component';
import {RouterModule} from '@angular/router';
import {SearchHeaderComponent} from './search-header/search-header.component';
import {ImageSliderHeaderComponent} from './image-slider-header/image-slider-header.component';
import {NgbCarouselModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
// import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MerchantHeaderComponent} from './merchant-header/merchant-header.component';
import {MerchantPanelHeaderComponent} from './merchantpanel-header/merchantpanel-header.component';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {CartService} from '../service/cart.service';
import {TranslateModule} from '@ngx-translate/core';
import {SliderService} from '../service/slider.service';
import {ProductCategoryService} from '../service/product-category.service';
import {FormsModule} from '@angular/forms';
import {ToasterService} from 'src/app/toaster.service';
import {SubscribeService} from '../service/subscribe.service';
import {FileService} from '../file.service';
import {MerchantService} from '../service/merchant.service';


@NgModule({
  declarations: [ErrorComponent, HeaderComponent, FooterComponent, MobileNavigasiComponent, SearchHeaderComponent, ImageSliderHeaderComponent, MerchantHeaderComponent, MerchantPanelHeaderComponent],
  exports: [ErrorComponent, HeaderComponent, FooterComponent, MobileNavigasiComponent, SearchHeaderComponent, ImageSliderHeaderComponent, MerchantHeaderComponent, MerchantPanelHeaderComponent],
  imports: [
    CommonModule,
    MatSidenavModule,
    RouterModule,
    NgbDropdownModule,
    NgbCarouselModule,
    FormsModule,
    // FontAwesomeModule,
    ToolkitModule,
    TranslateModule
  ],
  providers: [CartService, SliderService, ProductCategoryService, ToasterService, SubscribeService, MerchantService, FileService]
})
export class SharedModule {
}
