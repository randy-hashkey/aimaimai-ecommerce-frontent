import {Component, OnInit} from '@angular/core';
import {CartService} from '../../service/cart.service';
import {Cart} from '../../models/cart.model';
import {CategoryContent} from '../../models/product-category.model';
import {ProductCategoryService} from '../../service/product-category.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.scss']
})
export class SearchHeaderComponent implements OnInit {

  content: Cart = new Cart();

  total: number = 0;

  categoryContents: CategoryContent[] = [];

  selectedCategoryName: string;

  searchText: string = '';

  constructor(private _cartService: CartService,
              private _router: Router,
              private _categoryService: ProductCategoryService) {
  }

  ngOnInit() {
    this._cartService.getCart().subscribe(response => {
      if (response) {
        this.total = response.cartItems.length;
      }
    });

    this._categoryService.list(0, 30, null).subscribe(response => {
      this.categoryContents = response.categories.content;
    });
  }

  redirectToCategoryPage(name: string) {
    this.selectedCategoryName = name;
    window.location.href = '/products?category=' + name + '&search=' + this.searchText;
    // this._router.navigate(['/products'], { "queryParams": { "category": name, "search": this.searchText } });
  }

  search() {
    window.location.href = '/products?search=' + this.searchText;
  }

}
