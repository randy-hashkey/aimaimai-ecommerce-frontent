import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {AuthService} from '../../auth.service';
import {UserContent} from '../../models/user.model';

// import {faCoffee} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  // faCoffee = faCoffee;

  @ViewChild('sidenav') sidenav: MatSidenav;

  userContent: UserContent;

  reason = '';
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

  constructor(public _authService: AuthService) {
  }

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  ngOnInit() {
  }


}
