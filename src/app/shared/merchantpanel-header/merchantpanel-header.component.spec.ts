import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MerchantPanelHeaderComponent} from './merchantpanel-header.component';

describe('MerchantPanelHeaderComponent', () => {
  let component: MerchantPanelHeaderComponent;
  let fixture: ComponentFixture<MerchantPanelHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MerchantPanelHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantPanelHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
