import {Component, Input, OnInit} from '@angular/core';
import {MerchantContent} from '../../models/merchant.model';
import {MerchantService} from '../../service/merchant.service';
import {FileService} from '../../file.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-merchantpanel-header',
  templateUrl: './merchantpanel-header.component.html',
  styleUrls: ['./merchantpanel-header.component.scss']
})
export class MerchantPanelHeaderComponent implements OnInit {

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  content: MerchantContent = new MerchantContent();

  @Input()
  merchantId: string;

  constructor(private _merchantService: MerchantService,
              public _fileService: FileService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    ('h');
    this._activatedRoute.params.subscribe(params => {
      this.merchantId = params['merchantId'];
      this.get(this.merchantId);
    });
  }

  get(merchantId: string) {
    ('s');
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.content = response;
    });
  }

}
