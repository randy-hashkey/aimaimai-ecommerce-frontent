import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MobileNavigasiComponent} from './mobile-navigasi.component';

describe('MobileNavigasiComponent', () => {
  let component: MobileNavigasiComponent;
  let fixture: ComponentFixture<MobileNavigasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MobileNavigasiComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileNavigasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
