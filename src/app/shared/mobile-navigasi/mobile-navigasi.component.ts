import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';

@Component({
  selector: 'app-mobile-navigasi',
  templateUrl: './mobile-navigasi.component.html',
  styleUrls: ['./mobile-navigasi.component.scss']
})
export class MobileNavigasiComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;

  reason = '';
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

  constructor() {
  }

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  ngOnInit() {
  }

}
