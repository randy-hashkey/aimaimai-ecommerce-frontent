import {Component, OnInit} from '@angular/core';
import {MerchantContent} from '../../models/merchant.model';
import {MerchantService} from '../../service/merchant.service';
import {FileService} from '../../file.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-merchant-header',
  templateUrl: './merchant-header.component.html',
  styleUrls: ['./merchant-header.component.scss']
})
export class MerchantHeaderComponent implements OnInit {

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  content: MerchantContent = new MerchantContent();

  constructor(private _merchantService: MerchantService,
              public _fileService: FileService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    ('h');
    this._activatedRoute.params.subscribe(params => {
      (params);
      if (params['id']) {
        this.get(params['id']);
      }
    });
  }

  get(merchantId: string) {
    ('s');
    this._merchantService.getMyMerchant(merchantId).subscribe(response => {
      this.content = response;
      this.merchantMapToId.set(merchantId, response);
    });
  }

}
