import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SubscribeContent} from '../../models/subscribe.model';
import {Validators} from '@angular/forms';
import {SubscribeService} from '../../service/subscribe.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {FileService} from '../../file.service';
import {ToasterService} from 'src/app/toaster.service';

// import { faCoffee } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  closeResult: string;

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  readonly EMAIL: string = 'email';

  model: SubscribeContent = new SubscribeContent();

  errorMessage: string;

  constructor(private _subscribeService: SubscribeService,
              private _router: Router,
              public fileService: FileService,
              private _toasterService: ToasterService,
  ) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.EMAIL] = [this.model.email, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.EMAIL] = {'required': 'email is required'};
    return json;
  }

  subscribe(val) {
    if (!val) {
      this._toasterService.openToast('warning', 'please input email', 'warning');
      return;
    }
    let model: SubscribeContent = new SubscribeContent;
    model.email = val;

    this._subscribeService.registerSubscribes(model)
      .subscribe(
        (response) => {
          this._toasterService.openToast('email successfully subscribed', null, 'success');
        },
        (errorResponse: HttpErrorResponse) => {
          this._toasterService.openToast('warning', errorResponse.error.error, 'warning');
        }
      );
  }

}
