import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ImageSliderHeaderComponent} from './image-slider-header.component';

describe('ImageSliderHeaderComponent', () => {
  let component: ImageSliderHeaderComponent;
  let fixture: ComponentFixture<ImageSliderHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImageSliderHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageSliderHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
