import {Component, OnInit} from '@angular/core';
import {SliderContent} from '../../models/slider.model';
import {SliderService} from '../../service/slider.service';
import {FileService} from '../../file.service';
import {CategoryContent} from '../../models/product-category.model';
import {ProductCategoryService} from '../../service/product-category.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-image-slider-header',
  templateUrl: './image-slider-header.component.html',
  styleUrls: ['./image-slider-header.component.scss']
})
export class ImageSliderHeaderComponent implements OnInit {

  contents: SliderContent[] = [];

  categoryContents: CategoryContent[] = [];

  constructor(private _sliderService: SliderService,
              public fileService: FileService,
              private _categoryService: ProductCategoryService,
              private _router: Router
  ) {
  }

  ngOnInit() {
    this._sliderService.list(0, 10).subscribe(response => {
      this.contents = response.sliders.content;
    });

    this._categoryService.list(0, 20, null).subscribe(response => {
      this.categoryContents = response.categories.content;
    });
  }

  redirectToCategoryPage(name: string) {
    this._router.navigate(['/products'], {'queryParams': {'category': name}});
  }

}

