import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppSettings} from './app.setting';
import {Observable} from 'rxjs';
import {FileResponse} from './models/file.model';
import {AuthService} from './auth.service';

@Injectable()
export class FileService {

  static readonly IMAGE_ID: number = 1;

  static readonly DOC_ID: number = 2;

  constructor(private _http: Http, private http: HttpClient, private _authService: AuthService) {
  }

  static generateDownloadPath(id: string, token?: string) {
    return AppSettings.DOWNLOAD_FILE + id + '&authKey=' + token;
  }

  generateImagePathById(id: string): string {
    if (id == '0' || !id) {
      return '/assets/img/app/no-image.png';
    }
    return AppSettings.GET_IMAGE_BY_ID+id;
  }

  generatePathByIdToken(id: string, token: string) {
    return AppSettings.GET_IMAGE_BY_TOKEN + id + '&authKey=' + token;
  }

  printFile(id: string) {

    window.open(AppSettings.DOWNLOAD_FILE + id, '_blank');

    // this._http.get(AppSettings.DOWNLOAD_FILE + id, {
    //   responseType: ResponseContentType.Blob
    // }).subscribe(
    //   (response) => {
    //     var blob = new Blob([response.blob()], {type: 'application/pdf'});
    //     const blobUrl = URL.createObjectURL(blob);
    //     const iframe = document.createElement('iframe');
    //     iframe.style.display = 'none';
    //     iframe.src = blobUrl;
    //     document.body.appendChild(iframe);
    //     iframe.contentWindow.print();
    //   });
  }

  uploadFile(file: File, admin: boolean = false, privateFile: boolean = false, type: number = FileService.IMAGE_ID): Observable<FileResponse> {

    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer ' + this._authService.getToken());
    let formData: FormData = new FormData();
    formData.append('typeId', type + '');
    formData.append('file', file, file.name);
    formData.append('admin', (admin ? 1 : 0) + '');
    formData.append('private', (privateFile ? 1 : 0) + '');
    return this.http.post(AppSettings.UPLOAD_IMAGE, formData, {headers: headers})
      .map((response: FileResponse) => response);
  }
}
