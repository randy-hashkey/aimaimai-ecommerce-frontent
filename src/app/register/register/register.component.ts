import {Component, OnInit, ViewChild} from '@angular/core';
import {RegisterModel} from '../../models/user.model';
import {UserService} from '../../service/user.service';
import {AuthService} from '../../auth.service';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {FormComponent} from '../../toolkit/form/form.component';
import {ReferralService} from '../../service/referral.service';
import {ToasterService} from 'src/app/toaster.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  readonly FIRST_NAME: string = 'firstName';
  readonly LAST_NAME: string = 'lastName';
  readonly EMAIL: string = 'email';
  readonly PASSWORD: string = 'password';
  readonly VERIFY_PASSWORD: string = 'verifyPassword';
  readonly REFERRAL_CODE: string = 'referralCode';

  errorMessage: string;

  model: RegisterModel;

  @ViewChild('form')
  formComponent: FormComponent;

  constructor(private _userService: UserService,
              public _authService: AuthService,
              private _toasterService: ToasterService,
              private _routerService: Router,
              private _referralService: ReferralService) {
  }

  ngOnInit() {
    this.model = new RegisterModel();
  }

  getRules(): Object {
    let json: Object = {};
    json[this.FIRST_NAME] = [this.model.firstName, [Validators.required, Validators.minLength(3)]];
    json[this.LAST_NAME] = [this.model.lastName];
    json[this.EMAIL] = [this.model.email, [Validators.required, Validators.minLength(3), Validators.email]];
    json[this.PASSWORD] = [this.model.password, [Validators.required, Validators.minLength(6)]];
    json[this.VERIFY_PASSWORD] = [this.model.verifyPassword, [Validators.required, Validators.minLength(6)]];
    json[this.REFERRAL_CODE] = [this.model.referralCode];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.FIRST_NAME] = {'required': 'first name is required'};
    json[this.LAST_NAME] = {};
    json[this.EMAIL] = {'required': 'email is required'};
    json[this.PASSWORD] = {'required': 'password is required'};
    json[this.VERIFY_PASSWORD] = {'required': 'verify password is required'};
    json[this.REFERRAL_CODE] = {};
    return json;
  }

  register(obj: any) {
    let model: RegisterModel = new RegisterModel();
    model.firstName = obj[this.FIRST_NAME];
    model.lastName = obj[this.LAST_NAME];
    model.email = obj[this.EMAIL];
    model.password = obj[this.PASSWORD];
    model.verifyPassword = obj[this.VERIFY_PASSWORD];
    model.referralCode = obj[this.REFERRAL_CODE];

    if (obj[this.REFERRAL_CODE]) {
      this._referralService.findReferralCode(obj[this.REFERRAL_CODE]).subscribe(response => {
          if (!response.code) {
            this.errorMessage = response.error;
          }
          this._userService.register(model).subscribe(
            (response) => {
              this._authService.login(response.user);
              this._routerService.navigate(['/']);
            },
            (errorResponse: HttpErrorResponse) => {
              this._toasterService.openToast(null, errorResponse.error.error, 'error');
              this.errorMessage = errorResponse.error.error;
            }
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this._toasterService.openToast(null, errorResponse.error.error, 'error');
          this.errorMessage = errorResponse.error.error;
          return;
        });
    } else {
      this._userService.register(model).subscribe(
        (response) => {
          this._authService.login(response.user);
          this._routerService.navigate(['/']);
        },
        (errorResponse: HttpErrorResponse) => {

          this._toasterService.openToast(null, errorResponse.error.error, 'error');
          this.errorMessage = errorResponse.error.error;
        }
      );
    }

  }


}








