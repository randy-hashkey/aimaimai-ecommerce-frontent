import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterComponent} from './register/register.component';
import {RouterModule, Routes} from '@angular/router';
import {UserService} from '../service/user.service';
import {ToolkitModule} from '../toolkit/toolkit.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReferralService} from '../service/referral.service';
import {ToasterService} from '../toaster.service';

export const routes: Routes = [
  {path: '', component: RegisterComponent},
];

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    ToolkitModule
  ],
  providers: [UserService, ReferralService, ToasterService]
})
export class RegisterModule {
}
