import {Injectable} from '@angular/core';
import {AppSettings} from './app.setting';
import {UserContent} from './models/user.model';
import {FacebookService} from './service/facebook.service';

@Injectable()
export class AuthService {

  constructor(private _facebookService: FacebookService) {
  }

  login(content: UserContent) {
    content.id = 'hidden';
    content.passwordResetToken = 'hidden';
    content.validationToken = 'hidden';
    content.resetToken = 'hidden';
    content.password = 'hidden';
    content.accountNonExpired = null;
    content.accountNonLocked = null;
    content.credentialsNonExpired = null;
    localStorage.setItem(AppSettings.USER_INFO, JSON.stringify(content));
  }

  logout() {
    localStorage.removeItem(AppSettings.USER_INFO);
    window.location.reload();
  }

  loggedIn(): boolean {
    let token = localStorage.getItem(AppSettings.USER_INFO);
    return !!token;
  }

  getToken(): string {
    if (localStorage.getItem(AppSettings.USER_INFO)) {
      return this.getUserInfo().authKey;
    }
  }


  getUserInfo(): UserContent {
    return JSON.parse(localStorage.getItem(AppSettings.USER_INFO));
  }

  loginWithFacebook() {
    this._facebookService.login().subscribe(response => {
      window.location.href = response.redirectURL;
    });
  }

}
