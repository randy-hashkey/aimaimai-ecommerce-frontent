import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Validators} from '@angular/forms';
import {LoginModel} from '../../models/user.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import {FormComponent} from '../../toolkit/form/form.component';
import {UserService} from '../../service/user.service';
import {ToasterService} from 'src/app/toaster.service';
import {FacebookService} from '../../service/facebook.service';
import {CommonService} from '../../service/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error: string;

  readonly EMAIL: string = 'email';

  readonly PASSWORD: string = 'password';

  model: LoginModel;

  @ViewChild('form')
  formComponent: FormComponent;

  constructor(private _userService: UserService,
              private _authService: AuthService,
              private _toasterService: ToasterService,
              private _router: Router,
              private _facebookService: FacebookService,
              private _activeRoute: ActivatedRoute) {
  }

  ngOnInit() {

    if (this._authService.loggedIn()) {
      this._router.navigate(['/']);
    }

    this.model = new LoginModel;

    this._activeRoute.queryParams.subscribe(params => {
      if (params['code'] != null) {
        this._facebookService.getUserInfo(params['code']).subscribe(response => {
            if (response.user.authKey) {
              this._authService.login(response.user);
              this._router.navigate(['/']);
            }
          },
          (err: HttpErrorResponse) => {

            this._toasterService.openToast(null, err.error.error, 'error');
            this.error = err.error.error;

          });

      }
    });
  }

  getRules(): Object {
    let json: Object = {};
    json[this.EMAIL] = [this.model.email, [Validators.required]];
    json[this.PASSWORD] = [this.model.password, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.EMAIL] = {'required': 'email is required'};
    json[this.PASSWORD] = {'required': 'Password is required'};
    return json;
  }

  login(object: any) {
    this._userService.login(object).subscribe(response => {
        if (response.user.authKey) {
          this._authService.login(response.user);
          this._router.navigate(['/']);
        }
      },
      (err: HttpErrorResponse) => {

        this._toasterService.openToast(null, err.error.error, 'error');
        this.error = err.error.error;
      });
  }

  loginByFacebook() {
    this._facebookService.login().subscribe(response => {
      window.location.href = response.redirectURL;
    });
  }
}
