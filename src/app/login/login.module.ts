import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {ToolkitModule} from '../toolkit/toolkit.module';

import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserService} from '../service/user.service';
import {ToasterService} from '../toaster.service';
import {FacebookService} from '../service/facebook.service';

export const routes: Routes = [
  {path: '', component: LoginComponent},
];

@NgModule({
  declarations: [LoginComponent],
  exports: [LoginComponent],
  entryComponents: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    ToolkitModule
  ],
  providers: [UserService, ToasterService, FacebookService]
})
export class LoginModule {
}
