import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileService} from '../../../file.service';

@NgModule({
  providers: [FileService],
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: []
})
export class OrderViewingModule {
}
