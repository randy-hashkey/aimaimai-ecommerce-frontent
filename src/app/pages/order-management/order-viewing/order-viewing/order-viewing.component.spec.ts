import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderViewingComponent} from './order-viewing.component';

describe('OrderViewingComponent', () => {
  let component: OrderViewingComponent;
  let fixture: ComponentFixture<OrderViewingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderViewingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderViewingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
