import {Component, Input, OnInit} from '@angular/core';
import {OrderContent} from '../../../../models/order.model';
import {OrderService} from '../../../../service/order.service';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../../file.service';
import {MerchantContent} from 'src/app/models/merchant.model';
import {MerchantService} from 'src/app/service/merchant.service';

@Component({
  selector: 'app-order-viewing',
  templateUrl: './order-viewing.component.html',
  styleUrls: ['./order-viewing.component.scss']
})
export class OrderViewingComponent implements OnInit {

  orderMapToId: Map<string, OrderContent> = new Map<string, OrderContent>();

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  @Input()

  content: OrderContent = new OrderContent();

  detail: boolean = false;

  orderId: string;

  @Input()

  contents: OrderContent[];

  @Input()

  pageSize: number = 10;

  total: number = 0;

  search: string = '';

  status: number;

  asc: boolean;

  page: any;

  constructor(private _orderService: OrderService,
              private _activatedRoute: ActivatedRoute,
              public fileService: FileService,
              private _merchantService: MerchantService,
  ) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['orderId']) {
        this.get(params['orderId']);
        this.orderId = params['orderId'];
      }
    });
    this.list();
  }

  list() {
    this._orderService.listMyOrder(this.page, this.pageSize).subscribe(response => {

      this.contents = response.orders.content;
    });
  }

  get(orderId: string) {
    this._orderService.getOrderById(orderId).subscribe(response => {
      this.content = response.order;

    });
  }

  getMerchant(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.merchantMapToId.set(merchantId, response);
    });
  }
}
