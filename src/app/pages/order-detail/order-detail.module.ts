import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderDetailComponent} from './order-detail/order-detail.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'src/app/shared/shared.module';
import {HeaderComponent} from 'src/app/shared/header/header.component';
import {FooterComponent} from 'src/app/shared/footer/footer.component';
import {ToolkitModule} from 'src/app/toolkit/toolkit.module';
import {MerchantService} from 'src/app/service/merchant.service';
import {OrderService} from 'src/app/service/order.service';
import {FileService} from '../../file.service';
import {OrderPaymentService} from '../../service/order-payment.service';
import {ToasterService} from '../../toaster.service';

export const routes: Routes = [
  {path: ':orderId', component: OrderDetailComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  },
  {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  providers: [MerchantService, OrderService, FileService, OrderPaymentService, ToasterService],
  declarations: [OrderDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ToolkitModule,
  ]
})
export class OrderDetailModule {
}
