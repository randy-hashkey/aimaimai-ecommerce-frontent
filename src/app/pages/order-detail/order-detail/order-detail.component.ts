import {Component, Input, OnInit} from '@angular/core';
import {OrderService} from '../../../service/order.service';
import {ActivatedRoute} from '@angular/router';
import {ORDER_STATUS, OrderContent, orderMerchantGroupContent,} from '../../../models/order.model';
import {MerchantContent} from 'src/app/models/merchant.model';
import {MerchantService} from 'src/app/service/merchant.service';
import {FileService} from '../../../file.service';
import {OrderPaymentService} from '../../../service/order-payment.service';
import {OrderPaymentContent} from '../../../models/order.payment.model';
import {OrderGroupService} from '../../../service/order-group.service';
import {ToasterService} from '../../../toaster.service';
import {CommonService} from '../../../service/common.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  content: OrderContent = new OrderContent();

  contents: orderMerchantGroupContent[] = [];

  orderPaymentContent: OrderPaymentContent;

  loading: boolean = false;

  @Input()
  orderId: string;

  constructor(private _orderService: OrderService,
              private _activatedRoute: ActivatedRoute,
              private _merchantService: MerchantService,
              public fileService: FileService,
              private _orderPaymentService: OrderPaymentService,
              private _orderGroupService: OrderGroupService,
              private _toasterService: ToasterService,
              public commontService: CommonService
  ) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['orderId']) {
        this.orderId = params['orderId'];
        this.get(this.orderId);
      }
    });
  }


  getTotalProductCost(): number {
    let total = 0;
    for (let group of this.content.orderMerchantGroups) {
      for (let item of group.orderItems) {
        let cost = item.price * item.quantity;
        total = total + cost;
      }
    }
    return total;
  }

  getTotalRule(): number {
    if (this.content.rules.length == 0) {
      return 0;
    }

    let totalDiscount = 0;
    for (let rule of this.content.rules) {
      totalDiscount = totalDiscount + rule.discount;
    }
    if (totalDiscount > 100) {
      totalDiscount = 100;
    }
    return (totalDiscount * this.getTotalProductCost()) / 100;
  }

  get(orderId: string) {
    this._orderService.getOrderById(orderId).subscribe(response => {
      this.content = response.order;
      this.orderPaymentContent = response.order.orderPayment;
      for (let merchant of response.order.orderMerchantGroups) {
        this.getMerchant(merchant.merchantId);
      }
    });
  }


  getMerchant(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.merchantMapToId.set(merchantId, response);
    });
  }


  getStatusTextOrder(val: number) {
    return ORDER_STATUS.getGroupStatus(val);
  }

  getStatusText(val: number) {
    return ORDER_STATUS.getOrderStatus(val);
  }

  pay(paymentMethod: string) {
    switch (paymentMethod) {
      case ('PAYPAL'):
        this.createOrderWithPaypal();
        break;
    }
  }

  createOrderWithPaypal() {
    this.loading = true;
    this._orderPaymentService.createOrderByPaypal(this.content.id).subscribe(response => {
      // if (response.redirect_url) this.loading = false;
      window.location.href = response.redirect_url;
      // window.open(response.redirect_url, '_blank');
    });
  }

  finish(groupId: string) {
    this._orderGroupService.finish(groupId).subscribe(response => {
      this._toasterService.openToast('finish', null, 'success');
      this.get(this.orderId);
    });
  }
}
