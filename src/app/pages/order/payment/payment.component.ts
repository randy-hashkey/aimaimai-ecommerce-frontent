import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrderContent} from 'src/app/models/order.model';
import {PaymentMethodContent} from 'src/app/models/payment-method.model';
import {PaymentMethodService} from 'src/app/service/payment-method.service';
import {TaxService} from 'src/app/service/tax.service';
import {TaxContent} from 'src/app/models/tax.model';
import {ProductSectionService} from '../../../service/product-section.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  @Input()
  orderContent: OrderContent = new OrderContent();

  paymentContents: PaymentMethodContent[];

  paymentContent: PaymentMethodContent;

  taxContent: TaxContent = new TaxContent();

  @Output()
  selectPayment: EventEmitter<PaymentMethodContent> = new EventEmitter<PaymentMethodContent>();

  @Input()
  shipmentCost: number = 0;

  totalAttributePrice: number = 0;

  constructor(private _paymentSertvice: PaymentMethodService,
              private _taxService: TaxService,
              private _productSectionService: ProductSectionService) {
    this.paymentContent = new PaymentMethodContent();
    this.paymentContent.name = 'Select Payment Method';
  }

  ngOnInit() {
    this.listPayment();
    this._taxService.getSingleTax().subscribe(response => {
      this.taxContent = response;
    });

  }


  getTotalProductCost(): number {
    let total = 0;
    for (let group of this.orderContent.orderMerchantGroups) {
      for (let item of group.orderItems) {
        let cost = item.price * item.quantity;
        total = total + cost;
      }
    }
    return total + this.totalAttributePrice;
  }

  getTotalRule(): number {
    if (this.orderContent.rules.length == 0) {
      return 0;
    }

    let totalDiscount = 0;
    for (let rule of this.orderContent.rules) {
      totalDiscount = totalDiscount + rule.discount;
    }
    if (totalDiscount > 100) {
      totalDiscount = 100;
    }
    return (totalDiscount * this.getTotalProductCost()) / 100;
  }

  listPayment() {
    this._paymentSertvice.list(0, 10, '').subscribe(response => {
      this.paymentContents = response.paymentMethods.content;
    });
  }

  getPayment(val: PaymentMethodContent) {
    this.paymentContent = val;
    this.selectPayment.emit(this.paymentContent);
  }

  getTotalTax(totalProductCost: number): number {
    if (totalProductCost < this.taxContent.chargeableMax) {
      return 0;
    }
    return (totalProductCost * this.taxContent.rate) / 100;
  }

  getPercentageTax(totalProductCost: number): number {
    if (totalProductCost < this.taxContent.chargeableMax) {
      return 0;
    }
    return this.taxContent.rate;
  }

}
