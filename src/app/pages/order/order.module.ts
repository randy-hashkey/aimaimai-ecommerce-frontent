import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderComponent} from './order/order.component';
import {OrderItemComponent} from './order-item/order-item.component';
import {PaymentComponent} from './payment/payment.component';
import {RouterModule, Routes} from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {SharedModule} from '../../shared/shared.module';
import {SearchHeaderComponent} from '../../shared/search-header/search-header.component';
import {OrderService} from '../../service/order.service';
import {ToolkitModule} from 'src/app/toolkit/toolkit.module';
import {NgbActiveModal, NgbAlertModule, NgbDropdownModule, NgbModal, NgbModalConfig, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {ShipmentMethodService} from 'src/app/service/shipment-method.service';
import {PaymentMethodService} from 'src/app/service/payment-method.service';
import {TaxService} from 'src/app/service/tax.service';
import {AddressService} from 'src/app/service/address.service';
import {ToasterService} from 'src/app/toaster.service';
import {FileService} from 'src/app/file.service';
import {AddAddressFormComponent} from './add-address-form/add-address-form.component';
import {MatExpansionModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MerchantInfoModule} from '../merchant-info/merchant-info.module';

export const routes: Routes = [
  {path: ':orderId', component: OrderComponent},
  {path: 'add', component: AddAddressFormComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  }, {
    path: '',
    component: SearchHeaderComponent,
    outlet: 'first-header'
  }
];

@NgModule({
  declarations: [OrderComponent, OrderItemComponent, PaymentComponent, AddAddressFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatMenuModule,
    SharedModule,
    ToolkitModule,
    NgbDropdownModule,
    MatExpansionModule,
    NgxPaginationModule,
    NgbTabsetModule,
    NgxDatatableModule,
    MerchantInfoModule,
    NgbAlertModule,
  ],
  providers: [OrderService, ShipmentMethodService, PaymentMethodService, TaxService, AddressService,
    ToasterService, FileService, NgbModalConfig, NgbModal, NgbActiveModal,]
})
export class OrderModule {
}
