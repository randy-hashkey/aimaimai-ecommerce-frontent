import {Component, Input, OnInit} from '@angular/core';
import {OrderService} from '../../../service/order.service';
import {OrderContent} from '../../../models/order.model';
import {AddressService} from 'src/app/service/address.service';
import {AddressContent} from 'src/app/models/address.model';
import {MerchantShipmentMethod} from 'src/app/models/shipment-method.model';
import {HttpErrorResponse} from '@angular/common/http';
import {ToasterService} from 'src/app/toaster.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {AddAddressFormComponent} from '../add-address-form/add-address-form.component';
import {FileService} from '../../../file.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  shipmentCost: number = 0;

  content: OrderContent = new OrderContent();

  addressContents: AddressContent[];

  @Input()

  addressContent: AddressContent;

  merchantShipmentMethod: MerchantShipmentMethod[] = [];

  addressId: string;

  paymentId: string;

  errorMessage: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: AddressContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  constructor(
    private _orderService: OrderService,
    private _addressService: AddressService,
    private _toasterService: ToasterService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private ngModalService: NgbModal,
    public fileService: FileService,
    config: NgbModalConfig
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['addressId']) {
        this.addressContent.id = params['addressId'];
      }
    });

    this.get();
    this.getPrimaryAddress();
    this.listMyAddress();
  }

  get() {
    this._orderService.checkout().subscribe(response => {
      this._orderService.getOrderById(response.order.id).subscribe(response => {
          this.content = response.order;
        }
      );
    });
  }

  listMyAddress() {
    this._addressService.listMyAddress(0, 10).subscribe(response => {
      this.addressContents = response.addresses;
    });
  }

  getPrimaryAddress() {
    this._addressService.getPrimary().subscribe(response => {
        this.addressContent = response;
      },
      (errorResponse: HttpErrorResponse) => {
        this._toasterService.openToast('please set primary address', null, 'error');
      });
  }

  getPayment(val) {
    this.paymentId = val.name;
  }

  createOrder() {
    if (!this.addressContent) {
      this._toasterService.openToast('please set primary address', null, 'error');
      return;
    }

    let model = {
      id: this.content.id,
      paymentId: this.paymentId,
      merchantShipmentMethod: this.merchantShipmentMethod
    };

    this._orderService.createOrder(model).subscribe(response => {
        this._toasterService.openToast('order create', null, 'success');
        this._router.navigate(['order-detail', model.id]);
      },
      (errorResponse: HttpErrorResponse) => {
        // this.errorMessage = errorResponse.error.error;
        this._toasterService.openToast(errorResponse.error.error, null, 'error');
      });
  }


  getShipment(val: MerchantShipmentMethod) {
    let content = this.merchantShipmentMethod.find(merchantGroup => merchantGroup.merchantGroupId == val.merchantGroupId);
    if (content) {
      this.merchantShipmentMethod.splice(this.merchantShipmentMethod.indexOf(content), 1);
    }
    this.merchantShipmentMethod.push(val);


    let shipmentCost: number = 0;
    for (let cost of this.merchantShipmentMethod) {
      shipmentCost = shipmentCost + cost.cost;
    }

    this.shipmentCost = shipmentCost;
  }

  addAddress() {
    this.open(AddAddressFormComponent, null);
  }

  open(modalContent, addressId) {
    let openModal = this.ngModalService.open(modalContent, {size: 'lg'});
    if (addressId) {
      openModal.componentInstance.addressId = addressId;
    }
    openModal.result.then((close) => {
      this.list(this.page);
      this.listMyAddress();
      this.getPrimaryAddress();
    }, (dismis) => {
    });
  }

  list(page: number) {
    this.page = page;
    this._addressService.listMyAddress(0, 10).subscribe(response => {
      this.addressContents = response.addresses;
    });
  }


}
