import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {orderMerchantGroupContent} from 'src/app/models/order.model';
import {MerchantService} from 'src/app/service/merchant.service';
import {MerchantContent} from 'src/app/models/merchant.model';
import {ShipmentMethodService} from 'src/app/service/shipment-method.service';
import {MerchantShipmentMethod, ShipmentMethodContent} from 'src/app/models/shipment-method.model';
import {AddressContent} from 'src/app/models/address.model';
import {AddressService} from 'src/app/service/address.service';
import {FileService} from 'src/app/file.service';
import {ProductSectionService} from '../../../service/product-section.service';
import {ProductAttributeContent} from '../../../models/product-attribute.model';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent implements OnInit {

  merchantContent: MerchantContent = new MerchantContent();

  @Input()
  addressContents: AddressContent[] = [];

  addressContent: AddressContent = new AddressContent();

  @Input()
  item: orderMerchantGroupContent = new orderMerchantGroupContent();

  shipmentContents: ShipmentMethodContent[] = [];

  shipmentContent: ShipmentMethodContent = new ShipmentMethodContent();

  @Output()
  selectShipment: EventEmitter<MerchantShipmentMethod> = new EventEmitter<MerchantShipmentMethod>();

  contentSelected: MerchantShipmentMethod = new MerchantShipmentMethod();


  productAttributes: ProductAttributeContent[] = [];


  constructor(
    private _merchantService: MerchantService,
    private _shipmentService: ShipmentMethodService,
    private _addressService: AddressService,
    public fileService: FileService,
    private _productSectionService: ProductSectionService) {
    this.addressContent.name = 'select address';
    this.shipmentContent.name = 'Select Courier';
  }

  ngOnInit() {
    this.getMerchantInfo();
    this.listShipment();
    this.listAddress();
    this.contentSelected.addressId = this.addressContent.id;

  }

  getMerchantInfo() {
    this._merchantService.getMerchantById(this.item.merchantId).subscribe(response => {
      this.merchantContent = response;
    });
  }

  listShipment() {
    this._shipmentService.list(0, 10, '').subscribe(response => {
      this.shipmentContents = response.shipmentMethods.content;
    });
  }

  getShipment(content: ShipmentMethodContent) {
    this.contentSelected.merchantGroupId = this.item.id;
    this.contentSelected.shipmentMethodId = content.name;
    this.contentSelected.cost = content.cost;
    this.selectShipment.emit(this.contentSelected);

    this.shipmentContent = content;
  }

  listAddress() {
    this._addressService.listMyAddress(0, 10).subscribe(response => {
      this.addressContents = response.addresses;
      for (let address of response.addresses) {
        if (address.status == 10) {
          this.addressContent = address;
        }
      }
    });
  }

  getAddress(content: AddressContent) {
    this.contentSelected.merchantGroupId = this.item.id;
    this.contentSelected.addressId = content.id;
    this.selectShipment.emit(this.contentSelected);

    this.addressContent = content;
  }

}
