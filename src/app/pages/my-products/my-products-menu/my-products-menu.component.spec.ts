import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MyProductsMenuComponent} from './my-products-menu.component';

describe('MyProductsMenuComponent', () => {
  let component: MyProductsMenuComponent;
  let fixture: ComponentFixture<MyProductsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyProductsMenuComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyProductsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
