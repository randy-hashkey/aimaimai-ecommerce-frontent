import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyProductsComponent} from './my-products/my-products.component';
import {CreateMyProductFormComponent} from './create-my-product-form/create-my-product-form.component';
import {ViewMyProductsComponent} from './view-my-products/view-my-products.component';
import {EditMyProductFormComponent} from './edit-my-product-form/edit-my-product-form.component';
import {RouterModule, Routes} from '@angular/router';
import {MyProductsMenuComponent} from './my-products-menu/my-products-menu.component';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {SharedModule} from '../../shared/shared.module';

export const routes: Routes = [
  {path: '', component: MyProductsComponent},
  {path: ':id', component: ViewMyProductsComponent},
  {path: ':id/edit', component: EditMyProductFormComponent},
  {path: ':id/create', component: CreateMyProductFormComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  declarations: [MyProductsComponent, CreateMyProductFormComponent, ViewMyProductsComponent, EditMyProductFormComponent, MyProductsMenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class MyProductsModule {
}
