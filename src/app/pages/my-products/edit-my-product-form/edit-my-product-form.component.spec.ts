import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditMyProductFormComponent} from './edit-my-product-form.component';

describe('EditMyProductFormComponent', () => {
  let component: EditMyProductFormComponent;
  let fixture: ComponentFixture<EditMyProductFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditMyProductFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMyProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
