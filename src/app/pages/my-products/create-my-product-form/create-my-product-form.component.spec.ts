import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateMyProductFormComponent} from './create-my-product-form.component';

describe('CreateMyProductFormComponent', () => {
  let component: CreateMyProductFormComponent;
  let fixture: ComponentFixture<CreateMyProductFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateMyProductFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMyProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
