import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MerchantComponent} from './merchant/merchant.component';
// import {MerchantInfoModule} from '../merchant-info/merchant-info.module';
import {RouterModule, Routes} from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from 'src/app/shared/shared.module';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from 'src/app/shared/header/header.component';
import {FooterComponent} from 'src/app/shared/footer/footer.component';
import {MerchantService} from 'src/app/service/merchant.service';
import {ProductService} from 'src/app/service/product.service';
import {FileService} from 'src/app/file.service';
import {ProductsComponent} from './products/products.component';
import {ProductItemModule} from '../product-item/product-item.module';
import {PipesModule} from '../../pipes/pipes.module';

export const routes: Routes = [
  {path: ':merchantId', component: MerchantComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  }
];

@NgModule({
  declarations: [MerchantComponent, ProductsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    // MerchantInfoModule,
    NgxPaginationModule,
    SharedModule,
    NgbModalModule,
    ProductItemModule,
    PipesModule
  ],
  providers: [MerchantService, ProductService, FileService]
})
export class MerchantModule {
}
