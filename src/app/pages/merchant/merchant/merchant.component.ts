import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {MerchantService} from '../../../service/merchant.service';
import {MerchantContent} from '../../../models/merchant.model';

@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss']
})
export class MerchantComponent implements OnInit {

  content: MerchantContent = new MerchantContent();

  @Input()
  merchantId: string;

  constructor(private activatedRoute: ActivatedRoute,
              private _merchantService: MerchantService,
              public _fileService: FileService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.merchantId = params['merchantId'];
      this.get(this.merchantId);
    });
  }

  get(merchantId: string) {
    ('s');
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.content = response;
    });
  }

}
