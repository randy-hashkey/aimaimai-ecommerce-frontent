import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from '../../../models/product.model';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ProductPublicService} from '../../../service/public/product-public.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @Input()
  merchantId: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: ProductContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  constructor(
    private _authService: AuthService,
    private _activatedRoute: ActivatedRoute,
    public fileService: FileService,
    private _productService: ProductService,
    private _productPublicService: ProductPublicService) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._productPublicService.listByMerchantPublic(this.page, this.pageSize, this.search, this.merchantId).subscribe(response => {
      this.contents = response.products.content;
      for (let product of response.products.content) {
        this._productPublicService.getImagesByProductIdPublic(product.id).subscribe(response => {
          product.images = response.images;
        });
      }
      this.total = response.products.totalElements;
    });
  }


}
