import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ProductContent} from '../../../models/product.model';
import {ProductPublicService} from '../../../service/public/product-public.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {

  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  @Input()
  merchantId: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: ProductContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;


  constructor(
    private _authService: AuthService,
    private _activatedRoute: ActivatedRoute,
    public fileService: FileService,
    private _productService: ProductService,
    private _productPublicService: ProductPublicService
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._productPublicService.listPublic(this.page, this.pageSize, this.search, null).subscribe(response => {
      this.contents = response.products.content;
      this.total = response.products.totalElements;
    });
  }

}
