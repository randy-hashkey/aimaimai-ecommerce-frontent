import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from '../../../models/product.model';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ProductPublicService} from '../../../service/public/product-public.service';

@Component({
  selector: 'app-popular-item',
  templateUrl: './popular-item.component.html',
  styleUrls: ['./popular-item.component.scss']
})
export class PopularItemComponent implements OnInit {

  @Input()
  product: ProductContent;

  imageId: string = '0';

  constructor(public fileService: FileService,
              private _productService: ProductService,
              private _productPublicService: ProductPublicService
  ) {
  }

  ngOnInit() {
    this._productPublicService.getImagesByProductIdPublic(this.product.id).subscribe(response => {
      this.product.images = response.images;
      this.imageId = response.images[0].imageId;
    });
  }

}
