import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ProductSpecialCategoryPublicService} from '../../../service/public/product-special-category-public.service';
import {ProductCategoryContent} from '../../../models/product-category';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.scss']
})
export class PopularComponent implements OnInit {

  @Input()
  merchantId: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 16;

  contents: ProductCategoryContent[];

  total: number = 0;

  orderBy: string = 'popular';

  status: number;

  sorting: string;

  asc: boolean;

  constructor(
    private _activatedRoute: ActivatedRoute,
    public fileService: FileService,
    private _productService: ProductService,
    private _productSpecialCategoryPublicService: ProductSpecialCategoryPublicService
  ) {

  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._productSpecialCategoryPublicService.productMostPopularPublic(this.page, this.pageSize).subscribe(response => {
      this.contents = response.products.content;
      this.total = response.products.totalElements;
    });
  }

}
