import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from '../../../models/product.model';
import {FileService} from 'src/app/file.service';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.scss']
})
export class NewItemComponent implements OnInit {

  @Input()
  product: ProductContent;

  constructor(public fileService: FileService) {
  }

  ngOnInit() {
  }

}
