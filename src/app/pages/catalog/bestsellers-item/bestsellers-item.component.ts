import {Component, Input, OnInit} from '@angular/core';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ProductPublicService} from '../../../service/public/product-public.service';
import {ProductCategoryContent} from '../../../models/product-category';
import {ProductContent} from '../../../models/product.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-bestsellers-item',
  templateUrl: './bestsellers-item.component.html',
  styleUrls: ['./bestsellers-item.component.scss']
})
export class BestsellersItemComponent implements OnInit {

  @Input()
  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  @Input()
  productCategory: ProductCategoryContent = new ProductCategoryContent();

  @Input()

  imageId: string = '0';

  constructor(public fileService: FileService,
              private _productService: ProductService,
              private _productPublicService: ProductPublicService,
              private _activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {

    this._productPublicService.getImagesByProductIdPublic(this.productCategory.productId).subscribe(response => {
      this.imageId = response.images[0].imageId;
      this.getProductById(this.productCategory.productId);
    });
  }

  getProductById(productId: string) {
    this._productPublicService.getByIdPublic(productId).subscribe(response => {
      this.productIdToMap.set(productId, response);
    });
  }
}
