import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BestsellersItemComponent} from './bestsellers-item.component';

describe('BestsellersItemComponent', () => {
  let component: BestsellersItemComponent;
  let fixture: ComponentFixture<BestsellersItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BestsellersItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestsellersItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
