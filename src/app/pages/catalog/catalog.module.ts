import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PopularComponent} from './popular/popular.component';
import {PopularItemComponent} from './popular-item/popular-item.component';
import {NewItemComponent} from './new-item/new-item.component';
import {NewComponent} from './new/new.component';
import {BestsellersComponent} from './bestsellers/bestsellers.component';
import {BestsellersItemComponent} from './bestsellers-item/bestsellers-item.component';
import {CategoryComponent} from './category/category.component';
import {FilterComponent} from './filter/filter.component';
import {ProductComponent} from './product/product.component';
import {ProductItemComponent} from './product-item/product-item.component';
import {RouterModule, Routes} from '@angular/router';
import {CatalogComponent} from './catalog/catalog.component';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {SharedModule} from '../../shared/shared.module';
import {ProductService} from '../../service/product.service';
import {FileService} from '../../file.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {ImageSliderHeaderComponent} from '../../shared/image-slider-header/image-slider-header.component';
import {SearchHeaderComponent} from '../../shared/search-header/search-header.component';

export const routes: Routes = [
  {path: '', component: CatalogComponent},
  {
    path: '',
    component: SearchHeaderComponent,
    outlet: 'first-header'
  }, {
    path: '',
    component: ImageSliderHeaderComponent,
    outlet: 'second-header'
  }, {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  declarations: [PopularComponent, PopularItemComponent, NewItemComponent, NewComponent,
    BestsellersComponent, BestsellersItemComponent, CategoryComponent, FilterComponent, ProductComponent, ProductItemComponent, CatalogComponent],
  exports: [PopularComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ToolkitModule,
    SharedModule,
    NgxPaginationModule,

    NgbPaginationModule,
    NgbAlertModule
  ],
  providers: [ProductService, FileService],
})
export class CatalogModule {
}
