import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreditWalletComponent} from './credit-wallet/credit-wallet.component';
import {RouterModule, Routes} from '@angular/router';

export const routes: Routes = [
  {path: '', component: CreditWalletComponent},
];

@NgModule({
  declarations: [CreditWalletComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class CreditWalletModule {
}
