import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductInfoComponent} from './product-info.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [ProductInfoComponent],
  exports: [ProductInfoComponent],
  entryComponents: [ProductInfoComponent],
  imports: [
    CommonModule,
    NgbDropdownModule,
    ToolkitModule,
    FormsModule
  ]
})
export class ProductInfoModule {
}
