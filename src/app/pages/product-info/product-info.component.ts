import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from '../../models/product.model';
import {CartService} from '../../service/cart.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CartItem} from '../../models/cart.model';
import {FileService} from '../../file.service';
import {ProductAttribute} from '../../models/attribute.model';
import {ProductAttributeContent} from '../../models/product-attribute.model';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss']
})
export class ProductInfoComponent implements OnInit {

  readonly PRODUCT_ID: string = 'productId';
  readonly ATTRIBUTEVALUE: string = 'attributeValue';
  model: CartItem = new CartItem();
  attribute: ProductAttribute = new ProductAttribute();

  @Input()
  quantity: number = 0;

  @Input()
  content: ProductContent = new ProductContent();

  contents: ProductContent[];

  contentAttributes = [];
  contentAttributeNames = [];

  selectedAttribute: ProductAttributeContent[] = [];

  attributePrice: number = 0;

  constructor(private _cartService: CartService,
              public ngActiveModal: NgbActiveModal,
              public fileService: FileService
  ) {
  }

  ngOnInit() {

    let groupByAttributeName = [];

    for (let attribute of this.content.attributes) {
      if (this.contentAttributeNames.indexOf(attribute.attributeName) === -1) {
        // if (attribute.attributeName != 'default') {
        this.contentAttributeNames.push(attribute.attributeName);
        // }
      }
    }

    this.content.attributes.forEach(function (a) {
      groupByAttributeName [a.attributeName] = groupByAttributeName [a.attributeName] || [];
      groupByAttributeName [a.attributeName].push({
        id: a.id,
        attributeValue: a.attributeValue,
        quantity: a.quantity,
        additionalPrice: a.additionalPrice
      });
    });

    this.contentAttributes = groupByAttributeName;

    console.log(this.contentAttributes);
  }

  addToCart() {
    let content: CartItem = new CartItem();
    content.productId = this.content.id;
    content.quantity = this.quantity;
    content.merchantId = this.content.merchantId;
    // content.attributes = this.selectedAttribute;

    // let attribute: ProductAttribute = new ProductAttribute();
    // attribute.attributeValue = event[this.ATTRIBUTEVALUE];

    this._cartService.addToCart(content);
    this.ngActiveModal.close();
  }


  getSelected(val, event, additionalPrice: number) {
    // console.log(val);
    // console.log(event.target.checked);

    let selected: boolean = event.target.checked;


    let model: ProductAttributeContent = new ProductAttributeContent();
    model.id = val;

    if (selected) {
      this.selectedAttribute.push(model);

      this.attributePrice = this.attributePrice + additionalPrice;
    } else {
      this.attributePrice = this.attributePrice - additionalPrice;
      this.selectedAttribute.splice(this.selectedAttribute.indexOf(event), 1);
    }


    console.log(this.selectedAttribute);
  }


}
