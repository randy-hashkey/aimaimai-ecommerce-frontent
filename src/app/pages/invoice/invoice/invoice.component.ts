import {Component, Input, OnInit, Output} from '@angular/core';
import {AddressService} from '../../../service/address.service';
import {UserService} from '../../../service/user.service';
import {UserContent} from '../../../models/user.model';
import {AddressContent} from '../../../models/address.model';
import {OrderContent} from '../../../models/order.model';
import {OrderService} from '../../../service/order.service';
import {ActivatedRoute} from '@angular/router';
import {MerchantService} from 'src/app/service/merchant.service';
import {MerchantContent} from '../../../models/merchant.model';
import {ItemContent} from '../../../models/ecommerce-item.model';
import {OrderPaymentContent} from '../../../models/order.payment.model';
import {ShipmentMethodContent} from '../../../models/shipment-method.model';
import {CommonService} from '../../../service/common.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  orderPaymentContent: OrderPaymentContent;

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  @Output()

  itemMapToId: Map<string, ItemContent> = new Map<string, ItemContent>();

  @Input()

  shipmentContents: ShipmentMethodContent[] = [];


  content: UserContent;

  address: AddressContent;

  contents: OrderContent = new OrderContent();


  constructor(private _addressService: AddressService,
              private _userService: UserService,
              private _orderService: OrderService,
              private _activatedRoute: ActivatedRoute,
              private _merchantService: MerchantService,
              public commonService: CommonService
  ) {
  }

  ngOnInit() {
    this.get();
    this.getPrimary();

    this._activatedRoute.params.subscribe(params => {
      if (params['orderId']) {
        this.getMyOrder(params['orderId']);

      }
    });
  }


  get() {
    this._userService.getMyProfile().subscribe(response => {
      this.content = response;

    });
  }

  getMyOrder(orderId: string) {
    this._orderService.getOrderById(orderId).subscribe(response => {
      this.contents = response.order;
      this.orderPaymentContent = response.order.orderPayment;
      for (let merchant of response.order.orderMerchantGroups) {
        this.getMerchant(merchant.merchantId);
        // for(let item of merchant.orderItems) {
        //   this.itemMapToId.set(item.id);
        // }
      }

    });
  }

  getMerchant(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.merchantMapToId.set(merchantId, response);
    });
  }

  getPrimary() {
    this._addressService.getPrimary().subscribe(response => {
      this.address = response;

    });
  }

  getTotalRule(): number {
    if (this.contents.rules.length == 0) {
      return 0;
    }

    let totalDiscount = 0;
    for (let rule of this.contents.rules) {
      totalDiscount = totalDiscount + rule.discount;
    }
    if (totalDiscount > 100) {
      totalDiscount = 100;
    }
    return (totalDiscount * this.getTotalProductCost()) / 100;
  }

  getTotalProductCost(): number {
    let total = 0;
    for (let group of this.contents.orderMerchantGroups) {
      for (let item of group.orderItems) {
        let cost = item.price * item.quantity;
        total = total + cost;
      }
    }
    return total;
  }

  print() {
    let printContents = document.getElementById('invoice').innerHTML;
    let originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
  }
}
