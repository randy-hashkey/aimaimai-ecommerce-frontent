import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SecurityAndResolutionCenterComponent} from './security-and-resolution-center.component';

describe('SecurityAndResolutionCenterComponent', () => {
  let component: SecurityAndResolutionCenterComponent;
  let fixture: ComponentFixture<SecurityAndResolutionCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SecurityAndResolutionCenterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityAndResolutionCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
