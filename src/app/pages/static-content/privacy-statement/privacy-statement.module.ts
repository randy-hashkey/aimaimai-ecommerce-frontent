import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrivacyStatementComponent} from './privacy-statement/privacy-statement.component';
import {HeaderComponent} from '../../../shared/header/header.component';
import {FooterComponent} from '../../../shared/footer/footer.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {FileService} from '../../../file.service';

export const routes: Routes = [
  {path: '', component: PrivacyStatementComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];


@NgModule({
  declarations: [PrivacyStatementComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [FileService]
})
export class PrivacyStatementModule {
}
