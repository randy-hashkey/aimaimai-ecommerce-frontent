import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SafeProgramComponent} from './safe-program.component';

describe('SafeProgramComponent', () => {
  let component: SafeProgramComponent;
  let fixture: ComponentFixture<SafeProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SafeProgramComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafeProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
