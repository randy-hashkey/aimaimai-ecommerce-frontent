import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AboutAimaimaiComponent} from './about-aimaimai.component';

describe('AboutAimaimaiComponent', () => {
  let component: AboutAimaimaiComponent;
  let fixture: ComponentFixture<AboutAimaimaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutAimaimaiComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutAimaimaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
