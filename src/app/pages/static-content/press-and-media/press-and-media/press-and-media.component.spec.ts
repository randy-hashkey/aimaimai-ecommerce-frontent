import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PressAndMediaComponent} from './press-and-media.component';

describe('PressAndMediaComponent', () => {
  let component: PressAndMediaComponent;
  let fixture: ComponentFixture<PressAndMediaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PressAndMediaComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PressAndMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
