import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {HeaderComponent} from '../../../shared/header/header.component';
import {FooterComponent} from '../../../shared/footer/footer.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ToolkitModule} from '../../../toolkit/toolkit.module';
import {FileService} from '../../../file.service';
import {ContactService} from '../../../service/contact.service';
import {ToasterService} from '../../../toaster.service';

export const routes: Routes = [
  {path: '', component: ContactUsComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ToolkitModule,
  ],
  providers: [FileService, ContactService, ToasterService],
})
export class ContactUsModule {
}
