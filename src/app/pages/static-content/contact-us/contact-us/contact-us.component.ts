import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ContactContent} from '../../../../models/contact.model';
import {ContactService} from '../../../../service/contact.service';
import {FileService} from '../../../../file.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {ToasterService} from 'src/app/toaster.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  readonly NAME: string = 'name';
  readonly EMAIL: string = 'email';
  readonly MESSAGE: string = 'message';

  model: ContactContent = new ContactContent();

  errorMessage: string;


  constructor(private _contactService: ContactService,
              private _router: Router,
              public fileService: FileService,
              private _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name];
    json[this.EMAIL] = [this.model.email];
    json[this.MESSAGE] = [this.model.message];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'Name is required'};
    json[this.EMAIL] = {'required': 'email is required'};
    json[this.MESSAGE] = {'required': 'message is required'};
    return json;
  }

  add(obj: any) {
    let model: ContactContent = new ContactContent;
    model.name = obj[this.NAME];
    model.email = obj[this.EMAIL];
    model.message = obj[this.MESSAGE];

    this._contactService.registerContact(model)
      .subscribe(
        (response) => {

          this._router.navigate(['', response.contact.id]);
          this.success.emit(false);
          this._toasterService.openToast('message sent successfully ', null, 'success');
          return;
        },
        (errorResponse: HttpErrorResponse) => {
          this.errorMessage = errorResponse.error.error;
        }
      );
  }

  cancel() {
    this.success.emit(false);
  }
}
