import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HelpTopicsComponent} from './help-topics/help-topics.component';
import {HeaderComponent} from '../../../shared/header/header.component';
import {FooterComponent} from '../../../shared/footer/footer.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {FileService} from '../../../file.service';

export const routes: Routes = [
  {path: '', component: HelpTopicsComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  declarations: [HelpTopicsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [FileService]
})
export class HelpTopicsModule {
}
