import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UnsubscribeMailComponent} from './unsubscribe-mail.component';

describe('UnsubscribeMailComponent', () => {
  let component: UnsubscribeMailComponent;
  let fixture: ComponentFixture<UnsubscribeMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnsubscribeMailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsubscribeMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
