import {Component, OnInit} from '@angular/core';
import {SubscribeContent} from '../../../models/subscribe.model';
import {ToasterService} from '../../../toaster.service';
import {SubscribeService} from '../../../service/subscribe.service';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-unsubscribe-mail',
  templateUrl: './unsubscribe-mail.component.html',
  styleUrls: ['./unsubscribe-mail.component.scss']
})
export class UnsubscribeMailComponent implements OnInit {

  readonly EMAIL: string = 'email';

  model: SubscribeContent = new SubscribeContent();

  errorMessage: string;

  constructor(private _emailSubscribeService: SubscribeService,
              private _toasterService: ToasterService) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.EMAIL] = [this.model.email, [Validators.required, Validators.email]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.EMAIL] = {'required': 'email is required'};
    return json;
  }

  add(obj: any) {
    this._emailSubscribeService.unSubscribeMail(obj[this.EMAIL])
      .subscribe(
        (response) => {
          if (response.exist) {
            this._toasterService.openToast('Success', 'unsubscribe mail', 'warning');
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.errorMessage = errorResponse.error.error;
        }
      );
  }


}
