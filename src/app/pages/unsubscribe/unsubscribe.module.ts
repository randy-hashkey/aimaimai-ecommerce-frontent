import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UnsubscribeMailComponent} from './unsubscribe-mail/unsubscribe-mail.component';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {SharedModule} from '../../shared/shared.module';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {SubscribeService} from '../../service/subscribe.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

export const routes: Routes = [
  {path: '', component: UnsubscribeMailComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  }
];

@NgModule({
  declarations: [UnsubscribeMailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    ToolkitModule,
    SharedModule
  ],
  providers: [SubscribeService],
})
export class UnsubscribeModule {
}
