import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentSuccessComponent} from './payment-success/payment-success.component';
import {PaymentCancelComponent} from './payment-cancel/payment-cancel.component';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {SharedModule} from '../../shared/shared.module';
import {PaymentStatusComponent} from './payment-status.component';
import {OrderPaymentService} from '../../service/order-payment.service';

export const routes: Routes = [
  {path: 'status/:orderId', component: PaymentStatusComponent},
  // {path: 'success/:orderId', component: PaymentSuccessComponent},
  // {path: 'cancel/:orderId', component: PaymentCancelComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  declarations: [PaymentSuccessComponent, PaymentCancelComponent, PaymentStatusComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  providers: [OrderPaymentService],
})
export class PaymentStatusModule {
}
