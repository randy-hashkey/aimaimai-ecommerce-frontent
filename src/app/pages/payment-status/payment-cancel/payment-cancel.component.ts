import {Component, OnInit} from '@angular/core';
import {OrderContent} from '../../../models/order.model';

@Component({
  selector: 'app-payment-cancel',
  templateUrl: './payment-cancel.component.html',
  styleUrls: ['./payment-cancel.component.scss']
})
export class PaymentCancelComponent implements OnInit {


  content: OrderContent = new OrderContent();

  constructor() {
  }

  ngOnInit() {


  }


}
