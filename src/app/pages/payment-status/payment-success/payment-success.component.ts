import {Component, OnInit} from '@angular/core';
import {OrderContent} from '../../../models/order.model';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.scss']
})
export class PaymentSuccessComponent implements OnInit {
  content: OrderContent = new OrderContent();

  constructor() {
  }

  ngOnInit() {


  }

}
