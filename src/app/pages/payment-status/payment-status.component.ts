import {Component, OnInit} from '@angular/core';
import {OrderPaymentService} from '../../service/order-payment.service';
import {ActivatedRoute} from '@angular/router';
import {OrderPaymentContent, PAYMENT_STATUS} from '../../models/order.payment.model';

@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.scss']
})
export class PaymentStatusComponent implements OnInit {

  content: OrderPaymentContent;

  constructor(private _orderPaymentService: OrderPaymentService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['orderId']) {
        this._orderPaymentService.getOrderPayment(params['orderId']).subscribe(response => {
          this.content = response;
        });
      }
    });
  }

  getStatusText(val: number) {
    return PAYMENT_STATUS.getStatus(val);
  }

  pay(orderId: string) {
    this._orderPaymentService.createOrderByPaypal(orderId).subscribe(response => {
      window.open(response.redirect_url, '_blank');
    });
  }
}
