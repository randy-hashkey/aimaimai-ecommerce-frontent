import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../service/user.service';
import {ActivatedRoute} from '@angular/router';
import {UserContent} from '../../../models/user.model';

@Component({
  selector: 'app-get-user-info',
  templateUrl: './get-user-info.component.html',
  styleUrls: ['./get-user-info.component.scss']
})
export class GetUserInfoComponent implements OnInit {

  @Input()
  content: UserContent = new UserContent();

  constructor(private _userService: UserService,
              private _activatedRout: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRout.params.subscribe(params => {
      if (params['userId']) {
        this.get(params['userId']);
      }
    });
  }

  get(userId) {
    this._userService.get(userId).subscribe(response => {
      this.content = response;
    });
  }

}
