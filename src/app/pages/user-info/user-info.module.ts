import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {SharedModule} from '../../shared/shared.module';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {GetUserInfoComponent} from './get-user-info/get-user-info.component';

export const routes: Routes = [
  {path: 'get/:userId', component: GetUserInfoComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ToolkitModule,
    NgxDatatableModule
  ],
  declarations: [GetUserInfoComponent]
})
export class UserInfoModule {
}
