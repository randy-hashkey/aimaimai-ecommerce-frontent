import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductCategoryService} from 'src/app/service/product-category.service';
import {CategoryContent} from 'src/app/models/product-category.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products-header',
  templateUrl: './products-header.component.html',
  styleUrls: ['./products-header.component.scss']
})
export class ProductsHeaderComponent implements OnInit {

  categoryContents: CategoryContent[] = [];

  selectedCategoryName: string;

  @Output()
  update: EventEmitter<string> = new EventEmitter<string>();

  constructor(private _categoryService: ProductCategoryService,
              private _router: Router) {
  }

  ngOnInit() {
    this.listByCategory();
  }


  listByCategory() {
    this._categoryService.list(0, 8, null)
      .subscribe((response) => {
        this.categoryContents = response.categories.content;
      });
  }

  redirectToCategoryPage(name: string) {
    this.update.emit(name);
    this.selectedCategoryName = name;
    this._router.navigate(['/products'], {'queryParams': {'category': name}});
  }

}
