import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductMightLikeComponent} from './product-might-like.component';

describe('ProductMightLikeComponent', () => {
  let component: ProductMightLikeComponent;
  let fixture: ComponentFixture<ProductMightLikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductMightLikeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMightLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
