import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from 'src/app/models/product.model';
import {FileService} from 'src/app/file.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-might-like',
  templateUrl: './product-might-like.component.html',
  styleUrls: ['./product-might-like.component.scss']
})
export class ProductMightLikeComponent implements OnInit {

  @Input()
  recommendedProducts: ProductContent[] = [];

  @Input()
  productId: string;

  constructor(public fileService: FileService,
              private _router: Router) {
  }

  ngOnInit() {
  }

  redirectToProductPage(productId: string) {
    this._router.navigate(['/product/' + productId]);
  }

}
