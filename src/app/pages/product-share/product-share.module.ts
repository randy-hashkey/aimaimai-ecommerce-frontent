import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductsHeaderComponent} from './products-header/products-header.component';
import {ProductMightLikeComponent} from './product-might-like/product-might-like.component';

@NgModule({
  declarations: [ProductsHeaderComponent, ProductMightLikeComponent],
  exports: [ProductsHeaderComponent, ProductMightLikeComponent],
  imports: [
    CommonModule,
    // SharedModule
  ]
})
export class ProductShareModule {
}
