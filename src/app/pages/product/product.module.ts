import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductComponent} from './product.component';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {MatExpansionModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '../../shared/shared.module';
import {ProductService} from '../../service/product.service';
import {ProductShareModule} from '../product-share/product-share.module';
import {SearchHeaderComponent} from '../../shared/search-header/search-header.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {FileService} from 'src/app/file.service';
import {ToasterService} from 'src/app/toaster.service';
import {MerchantService} from 'src/app/service/merchant.service';
import {PipesModule} from '../../pipes/pipes.module';
import {ImageProductComponent} from '../image-product/image-product.component';
import {SlickModule} from 'ngx-slick';

export const routes: Routes = [
  {path: '', component: ProductComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
  {
    path: '',
    component: SearchHeaderComponent,
    outlet: 'first-header'
  }
];

@NgModule({
  declarations: [ProductComponent, ImageProductComponent],
  exports: [ImageProductComponent],

  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatExpansionModule,
    NgxPaginationModule,
    SharedModule,
    ProductShareModule,
    NgbModalModule,
    PipesModule,
    SlickModule.forRoot(),
  ],
  providers: [ProductService, FileService, ToasterService, MerchantService],
  entryComponents: [ImageProductComponent]
})
export class ProductModule {
}
