import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from '../../service/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductContent} from '../../models/product.model';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductInfoComponent} from '../product-info/product-info.component';
import {FileService} from 'src/app/file.service';
import {ProductImageContent} from 'src/app/models/product-image.model';
import {ToasterService} from 'src/app/toaster.service';
import {CategoryContent} from 'src/app/models/product-category.model';
import {MerchantContent} from 'src/app/models/merchant.model';
import {MerchantService} from 'src/app/service/merchant.service';
import {ProductPublicService} from '../../service/public/product-public.service';
import {ImageProductComponent} from '../image-product/image-product.component';
import {ProductAttributeContent, ProductAttributeModel} from '../../models/product-attribute.model';
import {CartItem} from '../../models/cart.model';
import {CartService} from '../../service/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input()
  merchantContent: MerchantContent;

  closeResult: string;

  merchantIdToMap: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  productId: string;

  content: ProductContent = new ProductContent();

  quantityToCart: number = 1;

  categories: CategoryContent[] = [];

  mainCategory: CategoryContent;

  recommendedProducts: ProductContent[] = [];

  defaultImageId;

  slides = [];

  slideConfig = {'slidesToShow': 4, 'slidesToScroll': 4, 'vertical': true};

  contentAttributes = [];

  contentAttributeNames = [];

  cartItem: CartItem = new CartItem();

  constructor(private _productService: ProductService,
              private _activatedRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _router: Router,
              public fileService: FileService,
              private ngModalService: NgbModal,
              private _merchantService: MerchantService,
              private _productPublicService: ProductPublicService,
              private _cartService: CartService
  ) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.productId = params['id'];
        this.get();
      }
    });
  }

  afterChange(e) {
    this.defaultImageId = this.slides[e.currentSlide];
  }

  getProductImages(images: ProductImageContent[]) {
    this.slides = [];
    for (let i of images) {
      this.slides.push({img: this.fileService.generateImagePathById(i.imageId)});
    }

    for (let attribute of this.content.attributes) {

      this.slides.push({img: this.fileService.generateImagePathById(attribute.image)});
    }

    this.defaultImageId = this.slides[0];
  }


  get() {
    this._productPublicService.getByIdPublic(this.productId).subscribe(response => {

      let groupByAttributeName = [];

      for (let attribute of response.attributes) {
        if (this.contentAttributeNames.indexOf(attribute.attributeName) === -1) {
          this.contentAttributeNames.push(attribute.attributeName);

        }
      }

      response.attributes.forEach(function (a) {



        groupByAttributeName [a.attributeName] = groupByAttributeName [a.attributeName] || [];
        groupByAttributeName [a.attributeName].push({
          attributeValue: a.attributeValue,
          quantity: a.quantity,
          additionalPrice: a.additionalPrice,
          id:a.id,
          attributeName:a.attributeName,
          image :a.image
        });
      });

      this.contentAttributes = groupByAttributeName;

      this.content = response;
      this.defaultImageId = response.images[0].imageId;
      this.categories = response.categories;
      if (response.categories.length > 0) {
        this.mainCategory = response.categories[0];
        this.getRecommendedProducts(this.mainCategory.categoryName);
      }
      this.getMerchant(response.merchantId);

      this.getProductImages(response.images);


    });
  }

  getRecommendedProducts(name: string) {
    this._productPublicService.listByCategoryPublic(name, 0, 6, '', '')
      .subscribe((response) => {
        this.recommendedProducts = response.products.content;
      });
  }

  open(content) {
    this.ngModalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // addToCart() {
  //   let openModal = this.ngModalService.open(ProductInfoComponent, {centered: true, size: 'lg'});
  //   openModal.componentInstance.content = this.content;
  //   openModal.componentInstance.quantity = this.quantityToCart;
  //   openModal.result.then((success) => {
  //     this._toasterService.openToast('add to cart', null, 'success');
  //   }, (dismis) => {
  //   });
  // }
  //
  // viewImage(path: string) {
  //   this.openImage(ImageProductComponent, path);
  // }

  openImage(modalContent, path: string) {
    let openModal = this.ngModalService.open(modalContent, {size: 'sm'});
    openModal.componentInstance.path = path;
    openModal.result.then((close) => {
    }, (dismis) => {
    });
  }

  incrementQuantityToCart() {
    if (this.quantityToCart >= this.content.quantity) {
      return;
    }
    this.quantityToCart++;
  }

  decrementQuantityToCart() {
    if (this.quantityToCart <= 1) {
      return;
    }
    this.quantityToCart--;
  }

  redirectToStorePage() {
    this._router.navigate(['/products']);
  }

  redirectToCategoryPage(name: string) {
    this._router.navigate(['/products'], {'queryParams': {'category': name}});
  }

  getMerchant(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.merchantIdToMap.set(merchantId, response);
    });
  }

  routMerchantURL(merchantId: string) {
    this._router.navigate(['merchant', merchantId]);
  }

  getSelect(e) {
    this.defaultImageId = e;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getSelectedAttribute(item) {
    this.defaultImageId = {img: this.fileService.generateImagePathById(item.image)};

    let temp = this.cartItem.cartItemAttributes.find(x => x.attributeName === item.attributeName);
    if (temp) this.cartItem.cartItemAttributes.splice(this.cartItem.cartItemAttributes.indexOf(item), 1);

    this.cartItem.cartItemAttributes.push(item);
  }

  selectedAttribute(item):boolean{
    let temp = this.cartItem.cartItemAttributes.find(x => x.attributeName === item.attributeName);
    return !!temp;

  }

  addToCart(note) {
    this.cartItem.productId = this.content.id;
    this.cartItem.quantity = this.quantityToCart;
    this.cartItem.merchantId = this.content.merchantId;
    this.cartItem.note = note;
    this.cartItem.price = this.content.price + this.getAdditionalPrice();
    this._cartService.addToCart(this.cartItem);
    this.cartItem=new CartItem();

    this._toasterService.openToast("add to cart", null, "success");
  }



  getAdditionalPrice():number{
    let total : number = 0;
    for (let item of this.cartItem.cartItemAttributes)
    {
      total = total+item.additionalPrice;
    }

  return total;
  }



  viewImage(){}

  cleanAttribute(){
    this.cartItem.cartItemAttributes=[];
  }
}
