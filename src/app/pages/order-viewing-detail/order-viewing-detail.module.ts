import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {OrderViewingDetailComponent} from './order-viewing-detail.component';

export const routes: Routes = [];

@NgModule({
  declarations: [OrderViewingDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class OrderViewingDetailModule {
}
