import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderViewingDetailComponent} from './order-viewing-detail.component';

describe('OrderViewingDetailComponent', () => {
  let component: OrderViewingDetailComponent;
  let fixture: ComponentFixture<OrderViewingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderViewingDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderViewingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
