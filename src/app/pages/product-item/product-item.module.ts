import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductItemComponent} from './product-item.component';
import {FileService} from '../../file.service';

@NgModule({
  declarations: [ProductItemComponent],
  exports: [ProductItemComponent],
  providers: [FileService],
  imports: [
    CommonModule,
  ]
})
export class ProductItemModule {
}
