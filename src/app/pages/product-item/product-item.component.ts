import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from '../../models/product.model';
import {FileService} from '../../file.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input()
  product: ProductContent;

  constructor(public fileService: FileService) {
  }

  ngOnInit() {
  }

}
