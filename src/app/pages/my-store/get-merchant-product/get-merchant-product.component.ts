import {Component, Input, OnInit} from '@angular/core';
import {ProductContent} from '../../../models/product.model';
import {NgbActiveModal, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../service/product.service';
import {MERCHANT_PAGE} from '../../../models/merchant.model';
import {ProductImageContent} from '../../../models/product-image.model';
import {FileService} from '../../../file.service';
import {SetMerchantProductDiscountFormComponent} from '../set-merchant-product-discount-form/set-merchant-product-discount-form.component';
import {ProductSectionService} from '../../../service/product-section.service';
import {ProductAttribute} from '../../../models/attribute.model';

@Component({
  selector: 'app-get-merchant-product',
  templateUrl: './get-merchant-product.component.html',
  styleUrls: ['./get-merchant-product.component.scss']
})
export class GetMerchantProductComponent implements OnInit {

  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  @Input()
  content: ProductContent = new ProductContent();

  productImages: ProductImageContent[] = [];

  edit: boolean = false;

  @Input()
  productId: string;

  constructor(private ngActiveModal: NgbActiveModal,
              private _activatedRoute: ActivatedRoute,
              private _productService: ProductService,
              private _productSectionService: ProductSectionService,
              private _router: Router,
              private _ngModalService: NgbModal,
              private fileService: FileService,
              config: NgbModalConfig) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['productId']) {
        this.productId = params['productId'];
      }
      this.get();
    });
  }

  get() {
    this._productService.getById(this.productId).subscribe(response => {
      this.productIdToMap.set(response.id, response);
      this.content = response;
      this.productImages = response.images;
    });
  }

  getProductAfterEdit() {
    this._productService.getByIdAfterEdit(this.productId).subscribe(response => {
      this.productIdToMap.set(response.id, response);
      this.content = response;
      this.productImages = response.images;
    });
  }

  viewMerchant() {
    this._router.navigate([MERCHANT_PAGE.VIEW, this.content.merchantId]);
  }

  uploadImage(id: string) {
    // this._productService.uploadProductImage(id, this.content.id).subscribe(response => {
    //   this.getProductAfterEdit();
    // });
  }

  setDiscount() {
    this.open(SetMerchantProductDiscountFormComponent);
  }

  open(modalContent) {
    let openModal = this._ngModalService.open(modalContent, {size: 'lg'});
    openModal.componentInstance.content = this.content;
    openModal.result.then((close) => {
      this.getProductAfterEdit();
    }, (dismis) => {
    });
  }

  getProductPrice(): number {
    let totalDiscount = 0;
    if (this.content.discount) {
      if (this.content.discount.discount == 0) {
        return this.content.price;
      }
      totalDiscount = (this.content.price * this.content.discount.discount) / 100;
    }

    totalDiscount = this.content.price - totalDiscount;
    return totalDiscount;
  }

  setEdit(val: boolean) {
    this.edit = val;
    this.getProductAfterEdit();
  }

  deleteProductCategories(category: string) {
    let categories: string[] = [];
    categories.push(category);
    this._productSectionService.deleteCategories(this.content.id, this.content.merchantId, categories).subscribe(response => {
      this.getProductAfterEdit();
    });
  }

  deleteProductAttributes(attributeName: string, attributeValue: string) {
    let model = new ProductAttribute();
    model.productId = this.content.id;
    model.attributeName = attributeName;
    model.attributeValue = attributeValue;
    let attributes: ProductAttribute[] = [];
    attributes.push(model);
    this._productSectionService.deleteAttributes(this.content.id, this.content.merchantId, attributes).subscribe(response => {
      this.getProductAfterEdit();
    });
  }

  deleteProductImages(image: string) {
    let images: string[] = [];
    images.push(image);
    this._productSectionService.deleteImages(this.content.id, this.content.merchantId, images).subscribe(response => {
      this.getProductAfterEdit();
    });
  }


  addProductMerchantDetail(contentModal) {
    let openModal = this._ngModalService.open(contentModal, {size: 'lg'});
    openModal.result.then((close) => {
      this.getProductAfterEdit();
    }, (dismis) => {
      this.getProductAfterEdit();
    });
  }


}
