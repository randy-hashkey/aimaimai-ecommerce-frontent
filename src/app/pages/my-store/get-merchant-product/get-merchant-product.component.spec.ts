import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GetMerchantProductComponent} from './get-merchant-product.component';

describe('GetMerchantProductComponent', () => {
  let component: GetMerchantProductComponent;
  let fixture: ComponentFixture<GetMerchantProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GetMerchantProductComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetMerchantProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
