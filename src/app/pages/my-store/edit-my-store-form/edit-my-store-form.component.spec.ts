import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditMyStoreFormComponent} from './edit-my-store-form.component';

describe('EditMyStoreFormComponent', () => {
  let component: EditMyStoreFormComponent;
  let fixture: ComponentFixture<EditMyStoreFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditMyStoreFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMyStoreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
