import {Component, Input, OnInit} from '@angular/core';
import {ORDER_STATUS, OrderContent} from 'src/app/models/order.model';
import {OrderService} from 'src/app/service/order.service';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {CommonService} from '../../../service/common.service';
import {MerchantContent} from '../../../models/merchant.model';
import {MerchantGroupReportService} from '../../../service/report/merchant-group-report.service';
import {orderMerchantGroupContent} from '../../../models/report/order-group-report.model';

@Component({
  selector: 'app-list-purchase',
  templateUrl: './list-purchase.component.html',
  styleUrls: ['./list-purchase.component.scss']
})
export class ListPurchaseComponent implements OnInit {

  content: MerchantContent = new MerchantContent();

  orderIdToMap: Map<string, OrderContent> = new Map<string, OrderContent>();

  contents: orderMerchantGroupContent[] = [];

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  total: number = 0;

  @Input()
  merchantId: string;

  orderId: string;

  errorMessage: string;

  constructor(private _orderService: OrderService,
              private _activatedRoute: ActivatedRoute,
              public fileService: FileService,
              public commontService: CommonService,
              private _merchantGroupReportService:MerchantGroupReportService
  ) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      this.merchantId = params['merchantId'];
      if (this.merchantId) {
        this.list(this.page);
      }
    });
  }


  list(page: number) {
    // this.page = page;
    // this._orderService.listMerchantGroup(this.merchantId, this.page, this.pageSize).subscribe(response => {
    //   this.contents = response.orderMerchantGroups.content;
    //   for (let group of this.contents) {
    //     this._orderService.getByGroupId(group.id, group.orderId).subscribe(response => {
    //       this.orderIdToMap.set(group.orderId, response);
    //     });
    //   }
    //
    //   this.total = response.orderMerchantGroups.totalElements;
    // });


    this._merchantGroupReportService.list(this.merchantId, this.page, this.pageSize).subscribe(response=>{
      this.contents = response.orderMerchantGroups.content;
      this.total = response.orderMerchantGroups.totalElements;
    })
  }

  getStatusText(val: number) {
    return ORDER_STATUS.getGroupStatus(val);
  }
}
