import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GetMerchantOrderDetailComponent} from './get-merchant-order-detail.component';

describe('GetMerchantOrderDetailComponent', () => {
  let component: GetMerchantOrderDetailComponent;
  let fixture: ComponentFixture<GetMerchantOrderDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GetMerchantOrderDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetMerchantOrderDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
