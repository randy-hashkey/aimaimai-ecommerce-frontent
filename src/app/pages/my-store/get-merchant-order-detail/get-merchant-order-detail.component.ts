import {Component, OnInit,} from '@angular/core';
import {ORDER_STATUS, OrderContent} from '../../../models/order.model';
import {OrderService} from '../../../service/order.service';
import {ActivatedRoute} from '@angular/router';
import {orderMerchantGroupContent} from '../../../models/order.group.model.';
import {FileService} from '../../../file.service';
import {OrderGroupService} from '../../../service/order-group.service';
import {ToasterService} from '../../../toaster.service';

@Component({
  selector: 'app-get-merchant-order-detail',
  templateUrl: './get-merchant-order-detail.component.html',
  styleUrls: ['./get-merchant-order-detail.component.scss']
})
export class GetMerchantOrderDetailComponent implements OnInit {

  orderGroupId: string;

  merchantId: string;

  orderId: string;

  orderContent: OrderContent;

  content: orderMerchantGroupContent = new orderMerchantGroupContent();

  // contents: OrderContent = new OrderContent();

  constructor(private _orderService: OrderService,
              private _activatedRoute: ActivatedRoute,
              public fileService: FileService,
              private _orderGroupService: OrderGroupService,
              private _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      this.orderGroupId = params['orderGroupId'];
      this.merchantId = params['merchantId'];
      this.get();
    });
  }

  get() {
    this._orderService.getMerchantGroupId(this.orderGroupId, this.merchantId).subscribe(response => {
      this.content = response;
      this._orderService.getByGroupId(response.id, response.orderId).subscribe(response => {
        this.orderContent = response;
      });
    });

  }

  getTotalCost(): number {
    let total: number = 0;
    for (let item of this.content.orderItems) {
      total = total + (item.quantity * item.price);
    }
    return total;
  }

  approve(groupId: string) {
    this._orderGroupService.approve(groupId).subscribe(response => {
      this._toasterService.openToast('approve', null, 'success');
      this.get();
    });
  }

  reject(groupId: string) {
    this._orderGroupService.reject(groupId).subscribe(response => {
      this._toasterService.openToast('reject', null, 'warning');
      this.get();
    });
  }

  getStatusText(val: number) {
    return ORDER_STATUS.getGroupStatus(val);
  }
}
