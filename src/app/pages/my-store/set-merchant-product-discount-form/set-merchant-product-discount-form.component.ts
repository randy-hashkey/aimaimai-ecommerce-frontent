import {Component, Input, OnInit} from '@angular/core';
import {ProductContent, ProductDiscountContent} from '../../../models/product.model';
import {ProductService} from '../../../service/product.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {ToasterService} from '../../../toaster.service';

@Component({
  selector: 'app-set-merchant-product-discount-form',
  templateUrl: './set-merchant-product-discount-form.component.html',
  styleUrls: ['./set-merchant-product-discount-form.component.scss']
})
export class SetMerchantProductDiscountFormComponent implements OnInit {

  @Input()
  content: ProductContent = new ProductContent();

  readonly DISCOUNT: string = 'discount';

  errorMessage: string;

  constructor(private _productService: ProductService,
              public ngActiveModal: NgbActiveModal,
              protected _toasterService: ToasterService) {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.DISCOUNT] = [this.content.discount.discount, [Validators.required, Validators.min(0), Validators.max(100)]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.DISCOUNT] = {'required': 'Discount is required'};
    return json;
  }

  add(obj: any) {
    let model: ProductDiscountContent = new ProductDiscountContent();
    model.discount = obj[this.DISCOUNT];
    model.merchantId = this.content.merchantId;
    model.productId = this.content.id;

    this._productService.setDiscount(model).subscribe(
      (response) => {
        this._toasterService.openToast('success', 'product discount set ' + obj[this.DISCOUNT] + '%', 'success');
        this.ngActiveModal.close('success');
      },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
      }
    );

  }
}
