import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SetMerchantProductDiscountFormComponent} from './set-merchant-product-discount-form.component';

describe('SetMerchantProductDiscountFormComponent', () => {
  let component: SetMerchantProductDiscountFormComponent;
  let fixture: ComponentFixture<SetMerchantProductDiscountFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SetMerchantProductDiscountFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetMerchantProductDiscountFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
