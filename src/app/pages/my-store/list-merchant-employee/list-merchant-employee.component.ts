import {Component, Input, OnInit} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {PRODUCT_STATUS} from '../../../models/product.model';
import {UserService} from '../../../service/user.service';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeContent} from '../../../models/employee.model';
import {EmployeeService} from '../../../service/employee.service';
import {InvertMerchantEmployeeComponent} from '../invert-merchant-employee/invert-merchant-employee.component';
import {UserContent} from '../../../models/user.model';
import {MerchantContent} from '../../../models/merchant.model';

@Component({
  selector: 'app-list-merchant-employee',
  templateUrl: './list-merchant-employee.component.html',
  styleUrls: ['./list-merchant-employee.component.scss']
})
export class ListMerchantEmployeeComponent implements OnInit {

  content: MerchantContent = new MerchantContent();

  userIdToMap: Map<string, UserContent> = new Map<string, UserContent>();

  @Input()
  merchantId: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: EmployeeContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  deleteEmployeeContent: EmployeeContent = new EmployeeContent();

  constructor(private _userService: UserService,
              private _authService: AuthService,
              private _activatedRoute: ActivatedRoute,
              private _employeeService: EmployeeService,
              private ngModalService: NgbModal,
              private _router: Router,
              config: NgbModalConfig) {
    config.backdrop = 'static';
    config.keyboard = false;

  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
    });
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._employeeService.listByMerchant(this.page, this.pageSize, this.merchantId).subscribe(response => {
      this.contents = response.employeeMerchants.content;
      this.total = response.employeeMerchants.totalElements;

      for (let i of response.employeeMerchants.content) {
        this._userService.get(i.userId).subscribe(userResponse => {
          this.userIdToMap.set(i.userId, userResponse);
        });
      }
    });
  }

  getStatusText(val: number) {
    return PRODUCT_STATUS.getStatus(val);
  }

  invertEmployee() {
    this.open(InvertMerchantEmployeeComponent, this.merchantId);
  }

  open(modalContent, id) {
    let openModal = this.ngModalService.open(modalContent, {size: 'lg'});
    if (id) {
      openModal.componentInstance.merchantId = id;
    }
    openModal.result.then((close) => {
      if (this.deleteEmployeeContent) {
        this.deleteEmployee();
      }
      this.list(this.page);
    }, (dismis) => {
    });
  }

  viewUser(id: string) {
    this._router.navigate(['user/get', id]);
  }

  deleteConfirm(content: EmployeeContent, contentModal) {
    this.deleteEmployeeContent = content;
    this.open(contentModal, null);
  }

  deleteEmployee() {
    this._employeeService.delete(this.deleteEmployeeContent).subscribe(response => {
      this.deleteEmployeeContent = null;
    });
  }
}
