import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListMerchantEmployeeComponent} from './list-merchant-employee.component';

describe('ListMerchantEmployeeComponent', () => {
  let component: ListMerchantEmployeeComponent;
  let fixture: ComponentFixture<ListMerchantEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListMerchantEmployeeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMerchantEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
