import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddMyStoreFormComponent} from './add-my-store-form.component';

describe('AddMyStoreFormComponent', () => {
  let component: AddMyStoreFormComponent;
  let fixture: ComponentFixture<AddMyStoreFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddMyStoreFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMyStoreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
