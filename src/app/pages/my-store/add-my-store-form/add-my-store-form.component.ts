import {Component, OnInit} from '@angular/core';
import {MerchantService} from '../../../service/merchant.service';
import {Router} from '@angular/router';
import {FileService} from '../../../file.service';
import {UserService} from '../../../service/user.service';
import {Validators} from '@angular/forms';
import {MERCHANT_PAGE, MerchantContent} from '../../../models/merchant.model';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-my-store-form',
  templateUrl: './add-my-store-form.component.html',
  styleUrls: ['./add-my-store-form.component.scss']
})
export class AddMyStoreFormComponent implements OnInit {

  ckEditorConfig = {
    removePlugins: 'save, newpage, print',
    extraPlugins: 'divarea'
  };

  readonly NAME: string = 'name';
  readonly DESCRIPTION: string = 'description';
  readonly CHINESE_DESCRIPTION: string = 'chineseDescription';
  readonly NRIC : string = 'nric';

  imageId: string;

  headerImageId: string;

  model: MerchantContent = new MerchantContent();

  errorMessage: string;

  constructor(private _merchantService: MerchantService,
              private _userService: UserService,
              public fileService: FileService,
              private _router: Router) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name, [Validators.required]];
    json[this.DESCRIPTION] = [this.model.description];
    json[this.CHINESE_DESCRIPTION] = [this.model.chineseDescription];
    json[this.NRIC] = [this.model.nric, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'Name is required'};
    json[this.DESCRIPTION] = {};
    json[this.CHINESE_DESCRIPTION] = {};
    json[this.NRIC] = {'required':'NRIC is required'};
    return json;
  }

  add(obj: any) {
    let model: MerchantContent = new MerchantContent;
    model.name = obj[this.NAME];
    model.description = obj[this.DESCRIPTION];
    model.chineseDescription = obj[this.CHINESE_DESCRIPTION];
    model.imageId = this.imageId;
    model.headerImageId = this.headerImageId;
    model.nric = obj[this.NRIC];

    this._merchantService.registerMerchant(model)
      .subscribe(
        (response) => {

          this._router.navigate([MERCHANT_PAGE.VIEW, response.merchant.id]);
        },
        (errorResponse: HttpErrorResponse) => {
          this.errorMessage = errorResponse.error.error;
        }
      );
  }

  handleUploadImageId(event) {
    this.imageId = event;
  }

  handleUploadHeaderImageId(event) {
    this.headerImageId = event;
  }
}
