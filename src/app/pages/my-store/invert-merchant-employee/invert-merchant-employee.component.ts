import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../service/user.service';
import {Router} from '@angular/router';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {EmployeeService} from '../../../service/employee.service';
import {EmployeeContent} from '../../../models/employee.model';

@Component({
  selector: 'app-invert-merchant-employee',
  templateUrl: './invert-merchant-employee.component.html',
  styleUrls: ['./invert-merchant-employee.component.scss']
})
export class InvertMerchantEmployeeComponent implements OnInit {

  @Input()
  merchantId: string;

  readonly EMAIL: string = 'email';

  content: EmployeeContent = new EmployeeContent();

  errorMessage: string;

  constructor(private _employeeService: EmployeeService,
              private _userService: UserService,
              private _router: Router,
              public ngActiveModal: NgbActiveModal) {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.EMAIL] = [this.content.userId, [Validators.required, Validators.email]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.EMAIL] = {'required': 'Email is required'};
    return json;
  }

  add(obj: any) {
    let model: EmployeeContent = new EmployeeContent();
    model.email = obj[this.EMAIL];
    model.merchantId = this.merchantId;

    this._employeeService.invertEmployeeToMerchant(model).subscribe(
      (response) => {
        this.ngActiveModal.close();
      },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
      }
    );

  }


}
