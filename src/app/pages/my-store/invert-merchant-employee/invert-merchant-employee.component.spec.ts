import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InvertMerchantEmployeeComponent} from './invert-merchant-employee.component';

describe('InvertMerchantEmployeeComponent', () => {
  let component: InvertMerchantEmployeeComponent;
  let fixture: ComponentFixture<InvertMerchantEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvertMerchantEmployeeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvertMerchantEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
