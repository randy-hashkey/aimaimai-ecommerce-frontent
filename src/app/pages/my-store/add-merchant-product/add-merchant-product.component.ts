import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MerchantService} from '../../../service/merchant.service';
import {UserService} from '../../../service/user.service';
import {FileService} from '../../../file.service';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {ProductContent} from '../../../models/product.model';
import {ProductService} from '../../../service/product.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ToasterService} from 'src/app/toaster.service';
import {ProductAttribute} from 'src/app/models/attribute.model';

@Component({
  selector: 'app-add-merchant-product',
  templateUrl: './add-merchant-product.component.html',
  styleUrls: ['./add-merchant-product.component.scss']
})
export class AddMerchantProductComponent implements OnInit {

  @Input()
  merchantId: string;

  @Output()
  close: EventEmitter<boolean> = new EventEmitter<boolean>();

  ckEditorConfig = {
    removePlugins: 'save, newpage, print',
    extraPlugins: 'divarea'
  };

  readonly NAME: string = 'name';
  readonly PRICE: string = 'price';
  readonly DESCRIPTION: string = 'description';
  readonly QUANTITY: string = 'quantity';

  sectionCategories: string[] = [];
  sectionAttributes: ProductAttribute[] = [];
  sectionImage: string[] = [];
  sectionShipmentMethod: string[] = [];

  content: ProductContent = new ProductContent();

  errorMessage: string;

  productImageIds: string[] = [];

  constructor(private _merchantService: MerchantService,
              private _userService: UserService,
              private _toasterService: ToasterService,
              public fileService: FileService,
              private _router: Router,
              private _productService: ProductService,
              public ngActiveModal: NgbActiveModal) {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.content.name, [Validators.required]];
    json[this.PRICE] = [this.content.price, [Validators.required]];
    json[this.DESCRIPTION] = [this.content.description];
    json[this.QUANTITY] = [this.content.quantity];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'User is required'};
    json[this.PRICE] = {'required': 'Name is required'};
    json[this.DESCRIPTION] = {};
    json[this.QUANTITY] = {};
    return json;
  }

  add(obj: any) {
    if (this.sectionCategories.length == 0) {
      this._toasterService.openToast('', 'Add at least 1 category', 'error');
      return;
    }

    if (this.productImageIds.length == 0) {
      this._toasterService.openToast('', 'Add at least 1 Image', 'error');
      return;
    }
    let model: ProductContent = new ProductContent();
    model.name = obj[this.NAME];
    model.price = obj[this.PRICE];
    model.description = obj[this.DESCRIPTION];
    model.quantity = obj[this.QUANTITY];
    model.images = this.sectionImage;
    model.categories = this.sectionCategories;
    model.attributes = this.sectionAttributes;
    model.merchantId = this.merchantId;
    model.shipments = this.sectionShipmentMethod;


    this._productService.add(model).subscribe(
      (response) => {
        this.addAllImages(response.id);
        this.ngActiveModal.close('success');
      },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
      }
    );

  }

  addAllImages(productId: string) {
    for (let imageId of this.productImageIds) {
      this._productService.uploadProductImage(imageId, productId)
        .subscribe((response) => {

        });
    }
  }

  addCategoryProduct(event) {
    this.sectionCategories.push(event);
  }

  removeCategory(event) {
    this.sectionCategories.splice(this.sectionCategories.indexOf(event), 1);
  }

  addProductImageId(id: string) {
    this.productImageIds.push(id);
  }

  removeProductImagId(id:string){
    this.productImageIds.splice(this.productImageIds.indexOf(id), 1);
  }

}
