import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddMerchantProductComponent} from './add-merchant-product.component';

describe('AddMerchantProductComponent', () => {
  let component: AddMerchantProductComponent;
  let fixture: ComponentFixture<AddMerchantProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddMerchantProductComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMerchantProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
