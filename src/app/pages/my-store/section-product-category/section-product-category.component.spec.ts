import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SectionProductCategoryComponent} from './section-product-category.component';

describe('SectionProductCategoryComponent', () => {
  let component: SectionProductCategoryComponent;
  let fixture: ComponentFixture<SectionProductCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SectionProductCategoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionProductCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
