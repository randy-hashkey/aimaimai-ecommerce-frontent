import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoryContent} from '../../../models/product-category.model';
import {ProductCategoryService} from '../../../service/product-category.service';

@Component({
  selector: 'app-section-product-category',
  templateUrl: './section-product-category.component.html',
  styleUrls: ['./section-product-category.component.scss']
})
export class SectionProductCategoryComponent implements OnInit {


  @Input()
  content: CategoryContent = new CategoryContent();

  @Output()
  addCategoryContent: EventEmitter<String> = new EventEmitter<String>();

  contents: CategoryContent[] = [];

  total: number = 0;

  constructor(private _categoryService: ProductCategoryService) {
  }

  ngOnInit() {
  }

  handleDataChange(text: string) {
    if (!text) {
      text = '';
    }

    this._categoryService.list(0, 4, text)
      .subscribe((response) => {
        this.contents = response.categories.content;
        this.total = response.categories.totalElements;
      });
  }

  add(val) {
    if (val) {
      this.addCategoryContent.emit(val);
    }
  }
}
