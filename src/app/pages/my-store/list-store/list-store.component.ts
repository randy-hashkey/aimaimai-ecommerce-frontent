import {Component, Input, OnInit} from '@angular/core';
import {MerchantService} from '../../../service/merchant.service';
import {MERCHANT_PAGE, MerchantContent, MERCHANT_STATUS} from '../../../models/merchant.model';
import {Router} from '@angular/router';
import {FileService} from '../../../file.service';
import {UserService} from '../../../service/user.service';

@Component({
  selector: 'app-list-store',
  templateUrl: './list-store.component.html',
  styleUrls: ['./list-store.component.scss']
})
export class ListStoreComponent implements OnInit {
  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: MerchantContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  constructor(private _merchantService: MerchantService,
              private _router: Router,
              public fileService: FileService,
              private _userService: UserService,
  ) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._merchantService.listMyMerchant(this.page, this.pageSize).subscribe(response => {
        this.contents = response.merchants.content;
        this.total = response.merchants.totalElements;
    });
  }

  viewMerchant(merchantId: string) {
    this._router.navigate([MERCHANT_PAGE.LIST, merchantId]);
  }


  registerAsMerchant() {
    this._userService.registerAsMerchant().subscribe(response => {

    });
  }

  getStatusText(val: number) {
    return MERCHANT_STATUS.getStatus(val);
  }
}
