import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyStoresComponent} from './my-stores/my-stores.component';
import {GetMyStoreComponent} from './get-my-store/get-my-store.component';
import {EditMyStoreFormComponent} from './edit-my-store-form/edit-my-store-form.component';
import {AddMyStoreFormComponent} from './add-my-store-form/add-my-store-form.component';
import {RouterModule, Routes} from '@angular/router';
import {MatExpansionModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '../../shared/shared.module';
import {NgbActiveModal, NgbAlertModule, NgbModal, NgbModalConfig, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {NavbarLeftComponent} from './navbar-left/navbar-left.component';
import {NavbarContentComponent} from './navbar-content/navbar-content.component';
import {MerchantService} from '../../service/merchant.service';
import {FileService} from '../../file.service';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {ListMerchantProductComponent} from './list-merchant-product/list-merchant-product.component';
import {GetMerchantProductComponent} from './get-merchant-product/get-merchant-product.component';
import {EditMerchantProductComponent} from './edit-merchant-product/edit-merchant-product.component';
import {ListMerchantEmployeeComponent} from './list-merchant-employee/list-merchant-employee.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AddMerchantProductComponent} from './add-merchant-product/add-merchant-product.component';
import {SectionProductCategoryComponent} from './section-product-category/section-product-category.component';
import {SectionProductAttributeComponent} from './section-product-attribute/section-product-attribute.component';
import {MerchantInfoModule} from '../merchant-info/merchant-info.module';
import {EmployeeService} from '../../service/employee.service';
import {InvertMerchantEmployeeComponent} from './invert-merchant-employee/invert-merchant-employee.component';
import {SetMerchantProductDiscountFormComponent} from './set-merchant-product-discount-form/set-merchant-product-discount-form.component';
import {UserService} from '../../service/user.service';
import {ToasterService} from 'src/app/toaster.service';
import {ListPurchaseComponent} from './list-purchase/list-purchase.component';
import {GetMerchantOrderDetailComponent} from './get-merchant-order-detail/get-merchant-order-detail.component';
import {OrderGroupService} from 'src/app/service/order-group.service';
import {ProductSectionService} from '../../service/product-section.service';
import {SalesInfoComponent} from './sales-info/sales-info.component';
import {PipesModule} from '../../pipes/pipes.module';
import {CommonService} from '../../service/common.service';
import {ListStoreComponent} from './list-store/list-store.component';
import {AddProductAttributeComponent} from './add-product-attribute/add-product-attribute.component';

export const routes: Routes = [
  {path: '', component: MyStoresComponent},
  {path: 'create', component: AddMyStoreFormComponent},
  {path: 'order-detail/:orderGroupId/:merchantId', component: GetMerchantOrderDetailComponent},
  {path: 'get/:merchantId', component: GetMyStoreComponent},
  {path: 'get/:merchantId/edit', component: EditMerchantProductComponent},
  {path: 'get/:merchantId/:productId', component: GetMerchantProductComponent},
  {path: 'invoice/:merchantId', component: ListPurchaseComponent},
  {path: 'list-employee/:merchantId', component: ListMerchantEmployeeComponent},
  {path: 'my-products/:merchantId', component: ListMerchantProductComponent},
  {path: 'sales/:merchantId', component: SalesInfoComponent},
  {path: 'list-store', component: ListStoreComponent},
  {path: 'list-store/:merchantId', component: ListStoreComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatExpansionModule,
    NgxPaginationModule,
    SharedModule,
    NgbTabsetModule,
    ToolkitModule,
    NgxDatatableModule,
    MerchantInfoModule,
    NgbAlertModule,
    PipesModule

  ],
  declarations: [MyStoresComponent, GetMyStoreComponent, EditMyStoreFormComponent,
    AddMyStoreFormComponent, NavbarLeftComponent, NavbarContentComponent,
    ListMerchantProductComponent, GetMerchantProductComponent, EditMerchantProductComponent,
    ListMerchantEmployeeComponent, AddMerchantProductComponent, SectionProductCategoryComponent,
    SectionProductAttributeComponent, InvertMerchantEmployeeComponent, SetMerchantProductDiscountFormComponent,
    ListPurchaseComponent, GetMerchantOrderDetailComponent, SalesInfoComponent, ListStoreComponent, AddProductAttributeComponent,

  ],
  exports: [AddMerchantProductComponent, InvertMerchantEmployeeComponent, SetMerchantProductDiscountFormComponent, ListMerchantEmployeeComponent],
  providers: [MerchantService, FileService, NgbModalConfig, NgbModal, NgbActiveModal,
    EmployeeService, UserService, ToasterService, OrderGroupService, ProductSectionService,
    CommonService],
  entryComponents: [AddMerchantProductComponent, InvertMerchantEmployeeComponent, SetMerchantProductDiscountFormComponent, AddProductAttributeComponent]
})
export class MyStoreModule {
}
