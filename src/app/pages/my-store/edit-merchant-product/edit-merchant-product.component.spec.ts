import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditMerchantProductComponent} from './edit-merchant-product.component';

describe('EditMerchantProductComponent', () => {
  let component: EditMerchantProductComponent;
  let fixture: ComponentFixture<EditMerchantProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditMerchantProductComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMerchantProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
