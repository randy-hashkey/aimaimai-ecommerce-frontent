import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductContent} from '../../../models/product.model';
import {MerchantService} from '../../../service/merchant.service';
import {UserService} from '../../../service/user.service';
import {FileService} from '../../../file.service';
import {Router} from '@angular/router';
import {ProductService} from '../../../service/product.service';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {ProductAttribute} from '../../../models/attribute.model';
import {ToasterService} from '../../../toaster.service';

@Component({
  selector: 'app-edit-merchant-product',
  templateUrl: './edit-merchant-product.component.html',
  styleUrls: ['./edit-merchant-product.component.scss']
})
export class EditMerchantProductComponent implements OnInit {

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  ckEditorConfig = {
    removePlugins: 'save, newpage, print',
    extraPlugins: 'divarea'
  };

  readonly ID: string = 'id';
  readonly NAME: string = 'name';
  // readonly CHINESE_NAME: string = 'chineseName';
  readonly PRICE: string = 'price';
  readonly DESCRIPTION: string = 'description';
  // readonly CHINESE_DESCRIPTION: string = 'chineseDescription';
  readonly QUANTITY: string = 'quantity';

  sectionCategories: string[] = [];
  sectionAttributes: ProductAttribute[] = [];
  sectionImage: string[] = [];

  @Input()
  content: ProductContent = new ProductContent();

  errorMessage: string;

  constructor(private _merchantService: MerchantService,
              private _userService: UserService,
              public fileService: FileService,
              private _router: Router,
              private _productService: ProductService,
              private _toasterService: ToasterService
  ) {
  }

  ngOnInit() {

    for (let category of this.content.categories) {
      this.addCategoryProduct(category.categoryName);
    }

    for (let attribute of  this.content.attributes) {
      this.sectionAttributes.push(attribute);
    }
  }

  getRules(): Object {
    let json: Object = {};
    json[this.ID] = [this.content.id, [Validators.required]];
    json[this.NAME] = [this.content.name, [Validators.required]];
    // json[this.CHINESE_NAME] = [this.content.chineseName, [Validators.required]];
    json[this.PRICE] = [this.content.price, [Validators.required]];
    json[this.DESCRIPTION] = [this.content.description];
    // json[this.CHINESE_DESCRIPTION] = [this.content.chineseDescription];
    json[this.QUANTITY] = [this.content.quantity];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = {'required': 'id is required'};
    json[this.NAME] = {'required': 'name is required'};
    // json[this.CHINESE_NAME] = {};
    json[this.PRICE] = {'required': 'price is required'};
    json[this.DESCRIPTION] = {};
    // json[this.CHINESE_DESCRIPTION] = {};
    json[this.QUANTITY] = {};
    return json;
  }

  add(obj: any) {
    let model: ProductContent = new ProductContent();
    model.id = obj[this.ID];
    model.name = obj[this.NAME];
    // model.chineseName = obj[this.CHINESE_NAME];
    model.price = obj[this.PRICE];
    model.description = obj[this.DESCRIPTION];
    // model.chineseDescription = obj[this.CHINESE_DESCRIPTION];
    model.quantity = obj[this.QUANTITY];
    model.images = this.sectionImage;
    model.categories = this.sectionCategories;
    // model.attributes = this.sectionAttributes;
    model.merchantId = this.content.merchantId;

    this._productService.edit(model).subscribe(
      (response) => {
        this._toasterService.openToast('product edit', null, 'success');
        this.success.emit(false);
      },
      (errorResponse: HttpErrorResponse) => {
        this._toasterService.openToast('failed', errorResponse.error.error, 'error');
        this.errorMessage = errorResponse.error.error;
      }
    );

  }

  addCategoryProduct(event) {
    this.sectionCategories.push(event);
  }

  removeCategory(event) {
    this.sectionCategories.splice(this.sectionCategories.indexOf(event), 1);
  }

  addAttributeProduct(event) {
    this.sectionAttributes.push(event);
  }

  removeAttribute(event) {
    this.sectionAttributes.splice(this.sectionAttributes.indexOf(event), 1);
  }


  cancel() {
    this.success.emit(false);
  }
}
