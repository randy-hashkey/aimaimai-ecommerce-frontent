import {Component, Input, OnInit, Output} from '@angular/core';
import {PRODUCT_STATUS, ProductContent} from '../../../models/product.model';
import {UserService} from '../../../service/user.service';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../service/product.service';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {AddMerchantProductComponent} from '../add-merchant-product/add-merchant-product.component';
import {MERCHANT_PAGE, MerchantContent} from '../../../models/merchant.model';
import {FileService} from '../../../file.service';
import {ProductPublicService} from '../../../service/public/product-public.service';
import {OrderContent} from '../../../models/order.model';
import {MerchantService} from '../../../service/merchant.service';

@Component({
  selector: 'app-list-merchant-product',
  templateUrl: './list-merchant-product.component.html',
  styleUrls: ['./list-merchant-product.component.scss']
})
export class ListMerchantProductComponent implements OnInit {


  order: OrderContent = new OrderContent();

  @Output()
  merchantIdToMap: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  @Input()
  content: ProductContent = new ProductContent();

  @Input()
  merchantContent: MerchantContent = new MerchantContent();

  @Input()
  merchantId: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: ProductContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  deleteContent: ProductContent = new ProductContent();

  constructor(private _userService: UserService,
              private _authService: AuthService,
              private _activatedRoute: ActivatedRoute,
              private _productService: ProductService,
              private ngModalService: NgbModal,
              private _router: Router,
              public fileService: FileService,
              config: NgbModalConfig,
              private _productPublicService: ProductPublicService,
              private _merchantService: MerchantService,
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['merchantId']) {
        this.getMerchant(params['merchantId']);
        this.merchantId = params['merchantId'];
      }
    });

    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._productService.listByMerchant(this.page, this.pageSize, this.search, this.merchantId).subscribe(response => {
      this.contents = response.products.content;
      for (let product of this.contents) {
        this._productPublicService.getImagesByProductIdPublic(product.id).subscribe(response => {
          product.images = response.images;
          this.getMerchant(product.merchantId);
        });
      }

      this.total = response.products.totalElements;
    });
  }

  getMerchant(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.merchantIdToMap.set(merchantId, response);
      this.merchantContent = response;
    });
  }

  getStatusText(val: number) {
    return PRODUCT_STATUS.getStatus(val);
  }

  addProduct() {
    this.deleteContent = null;
    this.open(AddMerchantProductComponent, this.merchantId);
  }

  open(modalContent, merchantId) {
    let openModal = this.ngModalService.open(modalContent, {size: 'lg'});
    if (merchantId) {
      openModal.componentInstance.merchantId = merchantId;
    }
    openModal.result.then((close) => {
      if (this.deleteContent) {
        this.deleteProduct();
      }
      this.list(this.page);
    }, (dismis) => {
    });
  }

  viewProduct(id: string) {
    this._router.navigate([MERCHANT_PAGE.VIEW + '/' + this.merchantId + '/' + id]);
  }

  deleteProductConfirm(content, productContent: ProductContent) {
    this.deleteContent = productContent;
    this.open(content, null);
  }

  deleteProduct() {
    this._productService.delete(this.deleteContent).subscribe(response => {
      this.deleteContent = null;
    });
  }
}
