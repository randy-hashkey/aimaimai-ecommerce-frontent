import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListMerchantProductComponent} from './list-merchant-product.component';

describe('ListMerchantProductComponent', () => {
  let component: ListMerchantProductComponent;
  let fixture: ComponentFixture<ListMerchantProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListMerchantProductComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMerchantProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
