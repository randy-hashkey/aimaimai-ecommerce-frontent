import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SectionProductAttributeComponent} from './section-product-attribute.component';

describe('SectionProductAttributeComponent', () => {
  let component: SectionProductAttributeComponent;
  let fixture: ComponentFixture<SectionProductAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SectionProductAttributeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionProductAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
