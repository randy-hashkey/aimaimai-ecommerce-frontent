import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AttributeContent, ProductAttribute} from '../../../models/attribute.model';
import {AttributeService} from '../../../service/attribute.service';

@Component({
  selector: 'app-section-product-attribute',
  templateUrl: './section-product-attribute.component.html',
  styleUrls: ['./section-product-attribute.component.scss']
})
export class SectionProductAttributeComponent implements OnInit {

  @Input()
  content: AttributeContent = new AttributeContent();

  @Output()
  addAttributeContent: EventEmitter<ProductAttribute> = new EventEmitter<ProductAttribute>();

  contents: AttributeContent[] = [];

  total: number = 0;

  constructor(private _attributeService: AttributeService) {
  }

  ngOnInit() {
  }

  handleDataChange(text: string) {
    if (!text) {
      text = '';
    }

    this._attributeService.list(0, 4, text)
      .subscribe((response) => {
        this.contents = response.attributes.content;
        this.total = response.attributes.totalElements;
      });
  }

  add(name, value, quantity, additionalPrice) {
    if (name && value) {
      let content: ProductAttribute = new ProductAttribute();
      content.attributeName = name;
      content.attributeValue = value;
      content.quantity = quantity;
      content.additionalPrice = additionalPrice;
      this.addAttributeContent.emit(content);
    }
  }
}
