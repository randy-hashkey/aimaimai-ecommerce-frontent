import {Component, Input, OnInit} from '@angular/core';
import {MerchantService} from '../../../service/merchant.service';
import {MERCHANT_PAGE, MERCHANT_STATUS, MerchantContent} from '../../../models/merchant.model';
import {ActivatedRoute, Router} from '@angular/router';
import {FileService} from '../../../file.service';
import {UserService} from '../../../service/user.service';
import {AuthService} from '../../../auth.service';
import {Authorities} from '../../../models/user.model';

@Component({
  selector: 'app-my-stores',
  templateUrl: './my-stores.component.html',
  styleUrls: ['./my-stores.component.scss']
})
export class MyStoresComponent implements OnInit {


  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: MerchantContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  constructor(private _userService: UserService,
              private _authService: AuthService,
              private _activatedRoute: ActivatedRoute,
              public fileService: FileService,
              private _router: Router,
              private _merchantService: MerchantService) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._merchantService.listMyMerchant(this.page, this.pageSize).subscribe(response => {
      if (!response.error) {
        this.contents = response.merchants.content;
        this.total = response.merchants.totalElements;
      }
    });
  }

  viewMerchant(merchantId: string) {
    this._router.navigate([MERCHANT_PAGE.LIST, merchantId]);
  }

  getStatusText(val: number) {
    return MERCHANT_STATUS.getStatus(val);
  }

  merchantIsRegistered(): boolean {
    let authorities: Authorities[] = this._authService.getUserInfo().authorities;
    for (let auth of authorities) {
      if (auth.authority == 'MERCHANT') {
        return true;
      }
    }
    return false;
  }

  registerAsMerchant() {
    this._userService.registerAsMerchant().subscribe(response => {

    });
  }
}
