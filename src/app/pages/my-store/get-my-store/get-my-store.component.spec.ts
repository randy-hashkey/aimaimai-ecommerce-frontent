import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GetMyStoreComponent} from './get-my-store.component';

describe('GetMyStoreComponent', () => {
  let component: GetMyStoreComponent;
  let fixture: ComponentFixture<GetMyStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GetMyStoreComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetMyStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
