import {Component, OnInit} from '@angular/core';
import {MerchantContent} from '../../../models/merchant.model';
import {MerchantService} from '../../../service/merchant.service';
import {FileService} from '../../../file.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-get-my-store',
  templateUrl: './get-my-store.component.html',
  styleUrls: ['./get-my-store.component.scss']
})
export class GetMyStoreComponent implements OnInit {

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  content: MerchantContent = new MerchantContent();

  errorMessage: string;

  constructor(private _merchantService: MerchantService,
              public _fileService: FileService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['merchantId']) {
        this.get(params['merchantId']);
      }
    });
  }

  get(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
        this.content = response;
        this.merchantMapToId.set(merchantId, response);
      },
      (error) => {
        this.errorMessage = error.error.error;
      });
  }

}
