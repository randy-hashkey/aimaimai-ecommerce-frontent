import {Component, Input, OnInit} from '@angular/core';
import {OrderContent, orderMerchantGroupContent} from 'src/app/models/order.model';
import {OrderService} from 'src/app/service/order.service';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {MerchantContent} from '../../../models/merchant.model';

@Component({
  selector: 'app-sales-info',
  templateUrl: './sales-info.component.html',
  styleUrls: ['./sales-info.component.scss']
})
export class SalesInfoComponent implements OnInit {

  content: MerchantContent = new MerchantContent();

  orderIdToMap: Map<string, OrderContent> = new Map<string, OrderContent>();

  contents: orderMerchantGroupContent[] = [];

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  total: number = 0;

  @Input()
  merchantId: string;

  orderId: string;

  errorMessage: string;

  constructor(private _orderService: OrderService,
              private _activatedRoute: ActivatedRoute,
              public fileService: FileService
  ) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      this.merchantId = params['merchantId'];
      if (this.merchantId) {
        this.list(this.page);
      }
    });
  }

  list(page: number) {
    this.page = page;
    this._orderService.listMerchantGroup(this.merchantId, this.page, this.pageSize).subscribe(response => {
      this.total = response.orderMerchantGroups.totalElements;
    });
  }
}
