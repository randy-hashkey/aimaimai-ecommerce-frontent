import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ProductContent} from '../../../models/product.model';
import {AttributeContent, ProductAttribute} from '../../../models/attribute.model';
import {AttributeService} from '../../../service/attribute.service';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ToasterService} from '../../../toaster.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-product-attribute',
  templateUrl: './add-product-attribute.component.html',
  styleUrls: ['./add-product-attribute.component.scss']
})
export class AddProductAttributeComponent implements OnInit {

  @ViewChild('file')
  fileElementRef: ElementRef;

  @Input()
  productContent: ProductContent;

  @Input()
  content: AttributeContent = new AttributeContent();

  contents: AttributeContent[] = [];

  total: number = 0;

  attributeImage;

  errorMessage:string;

  constructor(private _attributeService: AttributeService,
              public _fileService: FileService,
              private _productService: ProductService,
              private _toasterService: ToasterService,
              private _activeModal : NgbModal) {
  }

  ngOnInit() {
  }

  handleDataChange(text: string) {
    if (!text) {
      text = '';
    }

    this._attributeService.list(0, 4, text)
      .subscribe((response) => {
        this.contents = response.attributes.content;
        this.total = response.attributes.totalElements;
      });
  }

  add(name, value, quantity, additionalPrice) {

    if (typeof name === 'object') {
      this._toasterService.openToast(null, "please select valid attribute name", "error");
      return;
    }

    let content: ProductAttribute = new ProductAttribute();
    content.attributeName = name;
    content.productId = this.productContent.id;
    content.attributeValue = value;
    content.quantity = quantity;
    content.additionalPrice = additionalPrice;
    content.image = this.attributeImage;
    content.merchantId = this.productContent.merchantId;

    this._productService.addProductAttribute(content).subscribe(response => {
      if (!response.error) {
        this._toasterService.openToast('success', '', 'success');
        this._activeModal.dismissAll();
      }
    },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
        this._toasterService.openToast(null, this.errorMessage, "error");
      });
  }

  addAttributeImage() {
    this._fileService.uploadFile(this.fileElementRef.nativeElement.files[0]).subscribe(response => {
      let fileResponse: string = response.files[0].id;
      if (fileResponse) {
        this.attributeImage = fileResponse;
      }
    });
  }
}
