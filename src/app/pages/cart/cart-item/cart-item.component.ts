import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CartItem} from '../../../models/cart.model';
import {ProductContent} from '../../../models/product.model';
import {AuthService} from '../../../auth.service';
import {MerchantContent} from 'src/app/models/merchant.model';
import {ToasterService} from 'src/app/toaster.service';
import {FileService} from 'src/app/file.service';
import {ProductService} from 'src/app/service/product.service';
import {ProductImageContent} from 'src/app/models/product-image.model';
import {ProductPublicService} from '../../../service/public/product-public.service';
import {ProductSectionService} from '../../../service/product-section.service';
import {ProductAttributeContent} from '../../../models/product-attribute.model';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {

  @Input()
  merchantIdToMap: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  @Input()
  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  @Input()
  content: CartItem = new CartItem();

  @Output()
  contentItem: EventEmitter<CartItem> = new EventEmitter<CartItem>();

  @Output()
  itemDelete: EventEmitter<CartItem> = new EventEmitter<CartItem>();

  productImageContents: ProductImageContent [] = [];


  productAttributes: ProductAttributeContent[] = [];

  attributePrice: number = 0;

  constructor(public _authService: AuthService,
              public fileService: FileService,
              private _productService: ProductService,
              private _toasterService: ToasterService,
              private _productPublicService: ProductPublicService,
              private _productSectionService: ProductSectionService
  ) {
  }

  ngOnInit() {
    this.getProductImages();
    this.getProductAttribute();
  }

  getProductImages() {
    if (this.content.productId) {
      this._productPublicService.getImagesByProductIdPublic(this.content.productId).subscribe(response => {
        this.productImageContents = response.images;
      });
    }
  }

  deleteCart() {
    this.itemDelete.emit(this.content);
  }

  incrementItem() {

    if (!this._authService.loggedIn()) {
      this._toasterService.openToast('please login', null, 'error');
      return;
    }
    if (this.content.quantity >= this.productIdToMap.get(this.content.productId).quantity) {
      return;
    }
    this.content.quantity++;
    this.contentItem.emit(this.content);

    this.getProductById(this.content.productId);
  }

  decrementItem() {

    if (!this._authService.loggedIn()) {
      this._toasterService.openToast('please login', null, 'error');
      return;
    }

    if (this.content.quantity <= 1) {
      return;
    }
    this.content.quantity--;
    this.contentItem.emit(this.content);

    this.getProductById(this.content.productId);
  }

  getProductById(productId: string) {
    this._productPublicService.getByIdPublic(productId).subscribe(response => {
      this.productIdToMap.set(productId, response);
    });
  }


  getProductAttribute() {
    if (this.content.productId) {
      this._productSectionService.getAttributes(this.content.productId).subscribe(response => {
        for (let item of response.attributes) {
          for (let i of this.content.cartItemAttributes) {

            if (item.id == i.productAttributeId) {

              this.productAttributes.push(item);
              this.attributePrice = this.attributePrice + item.additionalPrice;
            }
          }
        }
      })
    }
  }
}
