import {Component, OnInit, Output} from '@angular/core';
import {CartService} from '../../../service/cart.service';
import {Cart, CartItem} from '../../../models/cart.model';
import {Router} from '@angular/router';
import {RuleContent} from '../../../models/rule.model';
import {ProductService} from '../../../service/product.service';
import {ProductContent} from '../../../models/product.model';
import {MerchantContent} from 'src/app/models/merchant.model';
import {MerchantService} from 'src/app/service/merchant.service';
import {ToasterService} from 'src/app/toaster.service';
import {OrderService} from 'src/app/service/order.service';
import {ProductPublicService} from '../../../service/public/product-public.service';
import {OrderContent} from '../../../models/order.model';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  contents: OrderContent = new OrderContent();

  @Output()
  merchantIdToMap: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  @Output()
  productIdToMap: Map<string, ProductContent> = new Map<string, ProductContent>();

  content: Cart = new Cart();

  ruleContents: RuleContent [] = [];

  condition: boolean = false;

  warningMessage:string;

  constructor(private _cartService: CartService,
              private router: Router,
              private _productService: ProductService,
              private _merchantService: MerchantService,
              private _toasterService: ToasterService,
              private _orderService: OrderService,
              private _productPublicService: ProductPublicService,
              public _authService: AuthService
  ) {
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this._cartService.getCart().subscribe(response => {
      this.content = response;
      this.ruleContents = response.rules;

      for (let item of response.cartItems) {
        this.getProductById(item.productId);
        this.getMerchant(item.merchantId);
      }
    });
    this._orderService.checkout().subscribe(response => {
      this.contents = response.order;
    });
  }

  getProductById(productId: string) {
    this._productPublicService.getByIdPublic(productId).subscribe(response => {
      this.productIdToMap.set(productId, response);
    });
  }

  getMerchant(merchantId: string) {
    this._merchantService.getMerchantById(merchantId).subscribe(response => {
      this.merchantIdToMap.set(merchantId, response);
    });
  }

  checkout() {
    if (!this.condition) {
      this.warningMessage= "please i agree with Terms and conditions";
      this._toasterService.openToast("", "please i agree with Terms and conditions", "warning");
      return;
    }

    if (!this.content.id) {
      this.router.navigate(['/checkout/']);
    }
    this.router.navigate(['/checkout/' + this.contents.id]);
  }

  editCartItem(content: CartItem) {
    this._cartService.editCart(content).subscribe(response => {
      this.get();
    });
  }

  addCoupon(val) {
    this.get();
  }

  deleteCartItem(content: CartItem) {
    this._cartService.deleteCart(content).subscribe(response => {
      this.get();
    });
  }

  getTotalProductCost(): number {
    let totalProductCost: number = 0;
    for (let item of this.content.cartItems) {
      totalProductCost = totalProductCost + this.getSingleProductQuantity(item.price, item.quantity);
    }
    return totalProductCost;
  }

  getDiscount(): number {
    if (this.content.rules.length == 0) {
      return 0;
    }

    let totalDiscount = 0;
    for (let rule of this.content.rules) {
      totalDiscount = totalDiscount + rule.discount;
    }
    if (totalDiscount > 100) {
      totalDiscount = 100;
    }
    return (totalDiscount * this.getTotalProductCost()) / 100;
  }

  getSingleProductQuantity(itemPrice: number, quantity: number): number {
    // if (this.productIdToMap.get(productId)) {
    return itemPrice * quantity;
    // }
  }

  deleteCartRule(couponCode: string) {
    this._orderService.deleteCouponCart(this.content.id, couponCode).subscribe(response => {
      this.get();
    });
  }

  getSelectedCondition(val) {
    console.log(val);
  }
}
