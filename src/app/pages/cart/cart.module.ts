import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CartComponent} from './cart/cart.component';
import {CartItemComponent} from './cart-item/cart-item.component';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {SharedModule} from '../../shared/shared.module';
import {SearchHeaderComponent} from '../../shared/search-header/search-header.component';
import {CartService} from '../../service/cart.service';
import {GetCouponFormModule} from '../get-coupon-form/get-coupon-form.module';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {MerchantService} from 'src/app/service/merchant.service';
import {ToasterService} from 'src/app/toaster.service';
import {FileService} from 'src/app/file.service';
import {OrderService} from 'src/app/service/order.service';
import {ProductSectionService} from '../../service/product-section.service';
import {FormsModule} from '@angular/forms';

export const routes: Routes = [
  {path: '', component: CartComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  }, {
    path: '',
    component: SearchHeaderComponent,
    outlet: 'first-header'
  }
];

@NgModule({
  declarations: [CartComponent, CartItemComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    GetCouponFormModule,
    ToolkitModule,
    FormsModule
  ],
  providers: [CartService, MerchantService, ToasterService, FileService, OrderService, ProductSectionService]
})
export class CartModule {
}
