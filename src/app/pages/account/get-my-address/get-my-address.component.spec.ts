import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GetMyAddressComponent} from './get-my-address.component';

describe('GetMyAddressComponent', () => {
  let component: GetMyAddressComponent;
  let fixture: ComponentFixture<GetMyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GetMyAddressComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetMyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
