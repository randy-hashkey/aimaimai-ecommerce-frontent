import {Component, Input, OnInit} from '@angular/core';
import {AddressService} from '../../../service/address.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {FileService} from '../../../file.service';
import {AddressContent} from '../../../models/address.model';

@Component({
  selector: 'app-get-my-address',
  templateUrl: './get-my-address.component.html',
  styleUrls: ['./get-my-address.component.scss']
})
export class GetMyAddressComponent implements OnInit {

  addressMapToId: Map<string, AddressContent> = new Map<string, AddressContent>();

  @Input()
  page: number = 0;


  @Input()
  pageSize: number = 10;

  content: AddressContent = new AddressContent();

  contents: AddressContent[];

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  addressId: string;

  constructor(private _addressService: AddressService,
              private _ngModalService: NgbModal,
              private _activatedRoute: ActivatedRoute,
              public _fileService: FileService,) {
  }

  ngOnInit() {
    this.list(this.page);

  }

  get(addressId: string) {
    this._addressService.getMyAddress(addressId).subscribe(response => {
      this.content = response;
      this.addressMapToId.set(addressId, response);
    });
  }

  list(page: number) {
    this.page = page;
    this._addressService.listMyAddress(this.page, this.pageSize).subscribe(response => {

      this.contents = response.addresses;


    });
  }

}
