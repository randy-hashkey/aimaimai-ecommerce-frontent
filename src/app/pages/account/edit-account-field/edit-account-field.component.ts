import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserContent} from '../../../models/user.model';
import {MerchantService} from '../../../service/merchant.service';
import {Validators} from '@angular/forms';
import {UserService} from '../../../service/user.service';
import {HttpErrorResponse} from '@angular/common/http';
import {FileService} from '../../../file.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-account-field',
  templateUrl: './edit-account-field.component.html',
  styleUrls: ['./edit-account-field.component.scss']
})
export class EditAccountFieldComponent implements OnInit {

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  content: UserContent = new UserContent();

  readonly ID: string = 'id';
  readonly FIRSTNAME: string = 'firstName';
  readonly LASTNAME: string = 'lastName';
  readonly PHONE: string = 'phone';

  sectionImage: string[] = [];

  errorMessage: string;

  constructor(private _userService: UserService,
              private _router: Router,
              public fileService: FileService,
              private _merchantService: MerchantService,
  ) {
  }

  ngOnInit() {

    this.get();
  }


  get() {
    this._userService.getMyProfile().subscribe(response => {
      this.content = response;

    });
  }

  getRules(): Object {
    let json: Object = {};
    json[this.ID] = [this.content.id, [Validators.required]];
    json[this.FIRSTNAME] = [this.content.firstName, [Validators.required]];
    json[this.LASTNAME] = [this.content.lastName];
    json[this.PHONE] = [this.content.phone];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = {'required': 'id is required'};
    json[this.FIRSTNAME] = {'required': ' first name is required'};
    json[this.LASTNAME] = {};
    json[this.PHONE] = {};
    return json;
  }

  add(obj: any) {
    let model: UserContent = new UserContent();
    model.id = obj[this.ID];
    model.firstName = obj[this.FIRSTNAME];
    model.lastName = obj[this.LASTNAME];
    model.phone = obj[this.PHONE];

    this._userService.edit(model).subscribe(
      (response) => {
        this.success.emit(false);
      },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
      }
    );

  }

  cancel() {
    this.success.emit(false);
  }

}
