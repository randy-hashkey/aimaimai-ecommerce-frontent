import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditAccountFieldComponent} from './edit-account-field.component';

describe('EditAccountFieldComponent', () => {
  let component: EditAccountFieldComponent;
  let fixture: ComponentFixture<EditAccountFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditAccountFieldComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAccountFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
