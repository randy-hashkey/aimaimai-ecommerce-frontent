import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../../service/user.service';
import {FileService} from '../../../file.service';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {AddressContent} from '../../../models/address.model';
import {AddressService} from '../../../service/address.service';

@Component({
  selector: 'app-edit-address-info-form',
  templateUrl: './edit-address-info-form.component.html',
  styleUrls: ['./edit-address-info-form.component.scss']
})
export class EditAddressInfoFormComponent implements OnInit {

  @Input()
  content: AddressContent = new AddressContent();

  readonly ID: string = 'id';
  readonly CITY: string = 'city';
  readonly COUNTRY: string = 'country';
  readonly STATUS: string = 'status';
  readonly STREET_NAME: string = 'streetName';
  readonly UNIT_NUMBER: string = 'unitNumber';
  readonly POSTAL_CODE: string = 'postalCode';
  readonly DETAIL: string = 'detail';

  errorMessage: string;

  constructor(private _addressService: AddressService,
              private _userService: UserService,
              public fileService: FileService,
              private _router: Router,
              public ngActiveModal: NgbActiveModal) {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.ID] = [this.content.id, [Validators.required]];
    json[this.CITY] = [this.content.city, [Validators.required]];
    json[this.COUNTRY] = [this.content.country];
    json[this.STATUS] = [this.content.status];
    json[this.STREET_NAME] = [this.content.streetName];
    json[this.UNIT_NUMBER] = [this.content.unitNumber];
    json[this.POSTAL_CODE] = [this.content.postalCode];
    json[this.DETAIL] = [this.content.detail];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = {};
    json[this.CITY] = {};
    json[this.COUNTRY] = {};
    json[this.STATUS] = {};
    json[this.STREET_NAME] = {};
    json[this.UNIT_NUMBER] = {};
    json[this.POSTAL_CODE] = {};
    json[this.DETAIL] = {};
    return json;
  }

  edit(obj: any) {
    let model: AddressContent = new AddressContent();
    model.city = obj[this.CITY];
    model.country = obj[this.COUNTRY];
    model.status = obj[this.STATUS];
    model.streetName = obj[this.STREET_NAME];
    model.unitNumber = obj[this.UNIT_NUMBER];
    model.postalCode = obj[this.POSTAL_CODE];
    model.detail = obj[this.DETAIL];
    model.id = obj[this.ID];


    this._addressService.edit(model).subscribe(
      (response) => {
        this.ngActiveModal.close('success');
      },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
      }
    );

  }

}
