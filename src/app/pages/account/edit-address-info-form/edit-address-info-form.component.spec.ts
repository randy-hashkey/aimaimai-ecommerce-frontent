import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditAddressInfoFormComponent} from './edit-address-info-form.component';

describe('EditAddressInfoFormComponent', () => {
  let component: EditAddressInfoFormComponent;
  let fixture: ComponentFixture<EditAddressInfoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditAddressInfoFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAddressInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
