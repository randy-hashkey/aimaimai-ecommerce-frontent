import {Component, Input, OnInit} from '@angular/core';
import {LoginModel, UserContent} from '../../../models/user.model';
import {UserService} from '../../../service/user.service';
import {FileService} from '../../../file.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  @Input()
  register: LoginModel;
  content: UserContent;

  @Input()
  UserId: string;

  constructor(private _userService: UserService,
              public fileService: FileService,) {
  }

  ngOnInit() {

    this.get();
  }

  get() {
    this._userService.getMyProfile().subscribe(response => {
      this.content = response;

    });
  }
}
