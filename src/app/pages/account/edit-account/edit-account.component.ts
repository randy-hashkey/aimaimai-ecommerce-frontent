import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../service/user.service';
import {LoginModel, UserContent} from '../../../models/user.model';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.scss']
})
export class EditAccountComponent implements OnInit {

  @Input()

  register: LoginModel;

  content: UserContent = new UserContent();

  edit: boolean = false;

  @Input()
  userId: string;

  constructor(private _userService: UserService,) {
  }

  ngOnInit() {

    this.get();
    this.getMyEmail();
  }

  get() {
    this._userService.getMyProfile().subscribe(response => {
      this.content = response;

    });
  }

  getMyEmail() {
    this._userService.getMyEmail().subscribe(response => {
      this.register = response;

    });
  }

  setEdit(val: boolean) {
    this.edit = val;
    this.get();
  }

}
