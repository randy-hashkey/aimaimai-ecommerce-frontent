import {Component, Input, OnInit} from '@angular/core';
import {ORDER_STATUS, OrderContent} from '../../../models/order.model';
import {OrderService} from '../../../service/order.service';
import {CommonService} from '../../../service/common.service';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.scss']
})
export class ListOrderComponent implements OnInit {

  contents: OrderContent[] = [];

  total: number = 0;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  constructor(private _orderService: OrderService, public commontService: CommonService) {
  }

  ngOnInit() {
    this.list(this.page);
  }

  list(page: number) {
    this.page = page;
    this._orderService.listMyOrder(this.page, this.pageSize).subscribe(response => {
      this.contents = response.orders.content;
      this.total = response.orders.totalElements;
    });
  }

  getStatusText(val: number) {
    return ORDER_STATUS.getOrderStatus(val);
  }

}
