import {Component, Input, OnInit} from '@angular/core';
import {LoginModel, UserContent} from '../../../models/user.model';
import {UserService} from '../../../service/user.service';
import {FileService} from '../../../file.service';
import {ActivatedRoute} from '@angular/router';
import {AddressService} from '../../../service/address.service';
import {AddressContent} from '../../../models/address.model';
import {AuthService} from '../../../auth.service';
import {WalletService} from '../../../service/wallet.service';
import {GET_CURRENCY, WalletModel} from '../../../models/wallet.model';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  @Input()
  register: LoginModel;
  content: UserContent;
  address: AddressContent;

  @Input()

  contents: AddressContent[];

  addressId: string;

  page: any;

  pageSize: number = 10;

  totalCash :number=0;
  totalCredit :number=0;
  totalPoint :number=0;

  constructor(private _userService: UserService,
              public fileService: FileService,
              private _activatedRoute: ActivatedRoute,
              private _addressService: AddressService,
              public _authService: AuthService,
              private _walletService : WalletService
  ) {
  }

  ngOnInit() {
    this.list();

    this.get();

    this.getMyEmail();

    this.getPrimary();

    this.getMyWallet();
  }

  list() {
    this._addressService.listMyAddress(this.page, this.pageSize).subscribe(response => {

      this.contents = response.addresses;

    });
  }

  get() {
    this._userService.getMyProfile().subscribe(response => {
      this.content = response;

    });
  }

  getMyEmail() {
    this._userService.getMyEmail().subscribe(response => {
      this.register = response;

    });
  }

  getPrimary() {
    this._addressService.getPrimary().subscribe(response => {
      this.address = response;

    });
  }

  getMyWallet(){
    this.getWallet(GET_CURRENCY.CASH);
    this.getWallet(GET_CURRENCY.CREDIT);
    this.getWallet(GET_CURRENCY.POINT);
  }


  getWallet(currency :string){
    this._walletService.getMyWallet(currency).subscribe(response=>{

      if (currency == GET_CURRENCY.CASH) this.totalCash = response.credit.totalCredit;
      if (currency == GET_CURRENCY.CREDIT) this.totalCredit = response.credit.totalCredit;
      if (currency == GET_CURRENCY.POINT) this.totalPoint = response.credit.totalCredit;

    })
  }
}
