import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AddressContent} from '../../../models/address.model';
import {Validators} from '@angular/forms';
import {AddressService} from '../../../service/address.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {FileService} from '../../../file.service';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss']
})
export class AddAddressComponent implements OnInit {

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  readonly NAME: string = 'name';
  readonly CITY: string = 'city';
  readonly COUNTRY: string = 'country';
  readonly LATITUDE: string = 'latitude';
  readonly LONGITUDE: string = 'longitude';
  readonly STATUS: string = 'status';
  readonly STREETNAME: string = 'streetName';
  readonly UNITNUMBER: string = 'unitNumber';
  readonly POSTALCODE: string = 'postalCode';
  readonly DETAIL: string = 'detail';

  model: AddressContent = new AddressContent();

  errorMessage: string;

  constructor(private _addressService: AddressService,
              private _router: Router,
              public fileService: FileService,
  ) {
  }

  ngOnInit() {
  }

  getRules(): Object {
    let json: Object = {};
    json[this.NAME] = [this.model.name];
    json[this.CITY] = [this.model.city, [Validators.required]];
    json[this.COUNTRY] = [this.model.country];
    json[this.LATITUDE] = [this.model.latitude];
    json[this.LONGITUDE] = [this.model.longitude];
    json[this.STATUS] = [this.model.status];
    json[this.STREETNAME] = [this.model.streetName];
    json[this.UNITNUMBER] = [this.model.unitNumber];
    json[this.POSTALCODE] = [this.model.postalCode];
    json[this.DETAIL] = [this.model.detail];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.NAME] = {'required': 'Name is required'};
    json[this.CITY] = {'required': 'City is required'};
    json[this.COUNTRY] = {};
    json[this.LATITUDE] = {};
    json[this.LONGITUDE] = {};
    json[this.STATUS] = {};
    json[this.STREETNAME] = {};
    json[this.UNITNUMBER] = {};
    json[this.POSTALCODE] = {};
    json[this.DETAIL] = {};
    return json;
  }

  add(obj: any) {
    let model: AddressContent = new AddressContent;
    model.name = obj[this.NAME];
    model.city = obj[this.CITY];
    model.country = obj[this.COUNTRY];
    model.latitude = obj[this.LATITUDE];
    model.longitude = obj[this.LONGITUDE];
    model.status = obj[this.STATUS];
    model.streetName = obj[this.STREETNAME];
    model.unitNumber = obj[this.UNITNUMBER];
    model.postalCode = obj[this.POSTALCODE];
    model.detail = obj[this.DETAIL];

    this._addressService.registerAddress(model)
      .subscribe(
        (response) => {

          this._router.navigate(['profile/address', response.address.id]);
          this.success.emit(false);
        },
        (errorResponse: HttpErrorResponse) => {
          this.errorMessage = errorResponse.error.error;
        }
      );
  }

  cancel() {
    this.success.emit(false);
  }
}
