import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditAccountComponent} from './edit-account/edit-account.component';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './account/account.component';
import {SharedModule} from '../../shared/shared.module';
import {HeaderComponent} from '../../shared/header/header.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {EditAccountFieldComponent} from './edit-account-field/edit-account-field.component';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {FileService} from '../../file.service';
import {UserService} from '../../service/user.service';
import {GetMyAddressComponent} from './get-my-address/get-my-address.component';
import {NgbActiveModal, NgbModal, NgbModalConfig, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {AddressComponent} from './address/address.component';
import {AddAddressComponent} from './add-address/add-address.component';
import {EditAddressInfoFormComponent} from './edit-address-info-form/edit-address-info-form.component';
import {MyProfileComponent} from './my-profile/my-profile.component';
import {AddressDetailComponent} from './address-detail/address-detail.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ListAddressComponent} from './list-address/list-address.component';
import {OrderViewingComponent} from '../order-management/order-viewing/order-viewing/order-viewing.component';
import {OrderViewingModule} from '../order-management/order-viewing/order-viewing.module';
import {AddressService} from '../../service/address.service';
import {MatExpansionModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {ListOrderComponent} from './list-order/list-order.component';
import {OrderService} from '../../service/order.service';
import {SearchHeaderComponent} from '../../shared/search-header/search-header.component';

export const routes: Routes = [
  {path: '', component: AccountComponent},
  {path: 'create-address', component: AddAddressComponent},
  {path: 'edit', component: EditAccountComponent},
  {path: 'address/:addressId', component: AddressDetailComponent},
  {path: 'get/:userId/edit', component: EditAccountFieldComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  }, {
    path: '',
    component: SearchHeaderComponent,
    outlet: 'first-header'
  }
];

@NgModule({
  declarations: [EditAccountComponent, AccountComponent, EditAccountFieldComponent, GetMyAddressComponent, AddAddressComponent, EditAddressInfoFormComponent, MyProfileComponent, AddressComponent, AddressDetailComponent, ListAddressComponent, OrderViewingComponent, ListOrderComponent],
  exports: [EditAddressInfoFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    OrderViewingModule,
    SharedModule,
    ToolkitModule,
    NgbTabsetModule,
    NgxDatatableModule,
    MatExpansionModule,
    NgxPaginationModule,
  ],
  providers: [FileService, UserService, NgbActiveModal, NgbModalConfig, NgbModal, AddressService, OrderService],
  entryComponents: [EditAddressInfoFormComponent]
})
export class AccountModule {
}
