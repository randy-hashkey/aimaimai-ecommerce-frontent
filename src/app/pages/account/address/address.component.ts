import {Component, Input, OnInit} from '@angular/core';
import {AddressContent} from '../../../models/address.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AddressService} from '../../../service/address.service';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {EditAddressInfoFormComponent} from '../edit-address-info-form/edit-address-info-form.component';
import {FileService} from '../../../file.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

  addressMapToId: Map<string, AddressContent> = new Map<string, AddressContent>();

  @Input()

  content: AddressContent = new AddressContent();

  addressId: string;

  @Input()
  pageSize: number = 10;

  contents: AddressContent[];

  total: number = 0;

  search: string = '';

  status: number;

  asc: boolean;

  page: any;

  deleteContent: AddressContent = new AddressContent();

  constructor(private _addressService: AddressService,
              public _fileService: FileService,
              private _ngModalService: NgbModal,
              private _activatedRoute: ActivatedRoute,
              private _router: Router,
              config: NgbModalConfig
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['addressId']) {
        this.get(params['addressId']);
      }
    });
    this.list();
  }


  list() {
    this._addressService.listMyAddress(this.page, this.pageSize).subscribe(response => {

      this.contents = response.addresses;

    });
  }

  get(addressId: string) {
    this._addressService.getAddressById(addressId).subscribe(response => {
      this.content = response;
      this.addressMapToId.set(addressId, response);
    });
  }

  editAddress(content: AddressContent) {
    let openModal = this._ngModalService.open(EditAddressInfoFormComponent, {size: 'lg'});
    openModal.componentInstance.content = content;
    openModal.result.then((close) => {
      this.get(this.addressId);
    }, (dismis) => {

    });
  }


  open(modalContent, id) {
    let openModal = this._ngModalService.open(modalContent, {size: 'lg'});
    if (id) {
      openModal.componentInstance.addressId = id;
    }
    openModal.result.then((close) => {
      if (this.deleteContent) {
        this.deleteAddress();
      }
      this.list();
    }, (dismis) => {
    });
  }


  deleteAddress() {
    this._addressService.delete(this.deleteContent).subscribe(response => {
      this.deleteContent = null;
    });
  }

  setPrimary(content) {
    this._addressService.setPrimary(content).subscribe(response => {
      this.content = response;
      this.list();
    });
  }
}
