import {Component, Input, OnInit} from '@angular/core';
import {AddressContent} from '../../../models/address.model';
import {ActivatedRoute} from '@angular/router';
import {AddressService} from '../../../service/address.service';
import {FileService} from '../../../file.service';

@Component({
  selector: 'app-address-detail',
  templateUrl: './address-detail.component.html',
  styleUrls: ['./address-detail.component.scss']
})
export class AddressDetailComponent implements OnInit {

  addressMapToId: Map<string, AddressContent> = new Map<string, AddressContent>();

  addressId: string;

  @Input()

  content: AddressContent = new AddressContent();

  @Input()
  pageSize: number = 10;

  contents: AddressContent[] = [];

  total: number = 0;

  search: string = '';

  status: number;

  asc: boolean;

  page: any;

  constructor(private _addressService: AddressService,
              public _fileService: FileService,
              private _activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {

    this._activatedRoute.params.subscribe(params => {
      if (params['addressId']) {
        this.get(params['addressId']);
      }
    });

  }

  get(addressId: string) {
    this.addressId = addressId;
    this._addressService.getMyAddress(addressId).subscribe(response => {
      this.addressMapToId.set(addressId, response);
    });
  }

}
