import {Component, Input, OnInit} from '@angular/core';
import {MerchantService} from '../../service/merchant.service';
import {MerchantContent} from '../../models/merchant.model';
import {FileService} from '../../file.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditMerchantInfoFormComponent} from './edit-merchant-info-form/edit-merchant-info-form.component';
import {orderMerchantGroupContent} from 'src/app/models/order.model';
import {ProductContent} from '../../models/product.model';
import {AuthService} from '../../auth.service';
import {Authorities, UserContent} from '../../models/user.model';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-merchant-info',
  templateUrl: './merchant-info.component.html',
  styleUrls: ['./merchant-info.component.scss']
})
export class MerchantInfoComponent implements OnInit {

  order: orderMerchantGroupContent = new orderMerchantGroupContent();

  merchantMapToId: Map<string, MerchantContent> = new Map<string, MerchantContent>();

  @Input()

  content: MerchantContent = new MerchantContent();

  product: ProductContent = new ProductContent();

  merchantId: string;

  productId: string;

  @Input()
  editable: boolean = false;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: UserContent;

  total: number = 0;

  search: string = '';

  status: number;

  sorting: string;

  asc: boolean;

  constructor(private _merchantService: MerchantService,
              public _fileService: FileService,
              private _activatedRoute: ActivatedRoute,
              private _ngModalService: NgbModal,
              public _router: Router,
              public _authService: AuthService,
              private _userService: UserService) {
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params['merchantId']) {
        this.get(params['merchantId']);
        this.merchantId = params['merchantId'];
      }
    });
    this.getUser();
  }

  get(merchantId: string) {
    if (this.editable) {
      this._merchantService.getMerchantById(merchantId).subscribe(response => {
        this.content = response;
        this.merchantMapToId.set(merchantId, response);
      });
    }
  }

  getUser() {
    this._userService.getMyProfile().subscribe(response => {
      this.contents = response;

    });
  }

  editMerchant() {
    let openModal = this._ngModalService.open(EditMerchantInfoFormComponent, {size: 'lg'});
    openModal.componentInstance.content = this.content;
    openModal.result.then((close) => {
      this.get(this.merchantId);
    }, (dismis) => {

    });
  }

  openMerchant() {
    let merchant: boolean = false;

    let authorities: Authorities[] = this._authService.getUserInfo().authorities;
    for (let auth of authorities) {
      if (auth.authority == 'MERCHANT') {
        merchant = true;
      }
    }

    if (!merchant) {
      this._userService.registerAsMerchant().subscribe(response => {

      });
    }

    this._router.navigate(['/my-store/create']);
  }

}
