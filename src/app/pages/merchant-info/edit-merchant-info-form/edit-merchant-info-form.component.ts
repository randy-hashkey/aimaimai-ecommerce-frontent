import {Component, Input, OnInit} from '@angular/core';
import {MerchantContent} from '../../../models/merchant.model';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MerchantService} from '../../../service/merchant.service';
import {UserService} from '../../../service/user.service';
import {FileService} from '../../../file.service';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-edit-merchant-info-form',
  templateUrl: './edit-merchant-info-form.component.html',
  styleUrls: ['./edit-merchant-info-form.component.scss']
})
export class EditMerchantInfoFormComponent implements OnInit {

  @Input()
  content: MerchantContent = new MerchantContent();

  ckEditorConfig = {
    removePlugins: 'save, newpage, print'
  };

  imageId: string;

  headerImageId: string;

  readonly ID: string = 'id';
  readonly NAME: string = 'name';
  readonly NRIC: string = 'nric';
  readonly DESCRIPTION: string = 'description';
  readonly CHINESE_DESCRIPTION: string = 'chineseDescription';

  errorMessage: string;

  constructor(private _merchantService: MerchantService,
              public fileService: FileService,
              public ngActiveModal: NgbActiveModal) {
  }

  ngOnInit() {

    if (this.content.id) {
      this.imageId = this.content.imageId;
      this.headerImageId = this.content.headerImageId;
    }

  }

  getRules(): Object {
    let json: Object = {};
    json[this.ID] = [this.content.id, [Validators.required]];
    json[this.NAME] = [this.content.name, [Validators.required]];
    json[this.DESCRIPTION] = [this.content.description];
    json[this.CHINESE_DESCRIPTION] = [this.content.chineseDescription];
    json[this.NRIC] = [this.content.nric];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.ID] = {'required': 'id is required'};
    json[this.NAME] = {'required': 'User is required'};
    json[this.NRIC] = {'required': 'NRIC is required'};
    json[this.DESCRIPTION] = {};
    json[this.CHINESE_DESCRIPTION] = {};
    return json;
  }

  edit(obj: any) {
    let model: MerchantContent = new MerchantContent();
    model.name = obj[this.NAME];
    model.description = obj[this.DESCRIPTION];
    model.chineseDescription = obj[this.CHINESE_DESCRIPTION];
    model.imageId = this.imageId;
    model.headerImageId = this.headerImageId;
    model.id = obj[this.ID];
    model.merchantId = obj[this.ID];
    model.nric = obj[this.NRIC];

    this._merchantService.edit(model).subscribe(
      (response) => {
        this.ngActiveModal.close('success');
      },
      (errorResponse: HttpErrorResponse) => {
        this.errorMessage = errorResponse.error.error;
      }
    );

  }

  handleUploadImageId(event) {
    this.imageId = event;
  }

  handleUploadHeaderImageId(event) {
    this.headerImageId = event;
  }
}
