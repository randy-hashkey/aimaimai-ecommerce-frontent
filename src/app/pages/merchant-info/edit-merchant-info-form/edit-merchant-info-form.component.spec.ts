import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditMerchantInfoFormComponent} from './edit-merchant-info-form.component';

describe('EditMerchantInfoFormComponent', () => {
  let component: EditMerchantInfoFormComponent;
  let fixture: ComponentFixture<EditMerchantInfoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditMerchantInfoFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMerchantInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
