import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MerchantInfoComponent} from './merchant-info.component';
import {MerchantService} from '../../service/merchant.service';
import {EditMerchantInfoFormComponent} from './edit-merchant-info-form/edit-merchant-info-form.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {RouterModule, Routes} from '@angular/router';
import {UserService} from '../../service/user.service';
import {PipesModule} from '../../pipes/pipes.module';

export const routes: Routes = [
  {path: '', component: MerchantInfoComponent},
];

@NgModule({
  declarations: [MerchantInfoComponent, EditMerchantInfoFormComponent],
  exports: [MerchantInfoComponent, EditMerchantInfoFormComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    ToolkitModule,
    RouterModule.forChild(routes),
    PipesModule
  ],
  providers: [MerchantService, UserService],
  entryComponents: [EditMerchantInfoFormComponent]
})
export class MerchantInfoModule {
}
