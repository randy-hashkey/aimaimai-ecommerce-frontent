import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GetCouponFormComponent} from './get-coupon-form.component';
import {ToolkitModule} from '../../toolkit/toolkit.module';
import {OrderService} from '../../service/order.service';
import {ToasterService} from 'src/app/toaster.service';

@NgModule({
  declarations: [GetCouponFormComponent],
  exports: [GetCouponFormComponent],
  providers: [OrderService, ToasterService],
  imports: [
    CommonModule,
    ToolkitModule
  ]
})
export class GetCouponFormModule {
}
