import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {OrderService} from '../../service/order.service';
import {AuthService} from '../../auth.service';
import {RuleContent} from '../../models/rule.model';
import {ToasterService} from 'src/app/toaster.service';

@Component({
  selector: 'app-get-coupon-form',
  templateUrl: './get-coupon-form.component.html',
  styleUrls: ['./get-coupon-form.component.scss']
})
export class GetCouponFormComponent implements OnInit {

  readonly COUPON_CODE: string = 'couponCode';

  couponCode: string;

  @Output()
  success: EventEmitter<boolean> = new EventEmitter<boolean>();

  model: RuleContent = new RuleContent();

  constructor(private _orderService: OrderService,
              private _authService: AuthService,
              private _toasterService: ToasterService) {
  }

  ngOnInit() {

  }

  getRules(): Object {
    let json: Object = {};
    json[this.COUPON_CODE] = [this.couponCode, [Validators.required]];
    return json;
  }

  getValidationMessages(): Object {
    let json: Object = {};
    json[this.COUPON_CODE] = {'required': 'Code is required'};
    return json;
  }

  getCoupon(obj: any) {
    if (!this._authService.loggedIn()) {
      this._toasterService.openToast('Coupon validate Login', null, 'error');
      return;
    }
    this._orderService.validCoupon(obj[this.COUPON_CODE]).subscribe(
      (response) => {
        if (response.couponCode) {
          this._orderService.addCouponToCart(response).subscribe(res => {
            if (res.discount) {
              this.success.emit(true);
            }
          });
        }
      },
      (errorResponse: HttpErrorResponse) => {
        this._toasterService.openToast(errorResponse.error.error, null, 'error');
      }
    );
  }


}
