import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GetCouponFormComponent} from './get-coupon-form.component';

describe('GetCouponFormComponent', () => {
  let component: GetCouponFormComponent;
  let fixture: ComponentFixture<GetCouponFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GetCouponFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCouponFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
