import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductListComponent} from './product-list/product-list.component';
import {RouterModule, Routes} from '@angular/router';
import {ProductsComponent} from './products/products.component';
import {ProductsCategoriesComponent} from './products-categories/products-categories.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {ProductMerchantComponent} from './product-merchant/product-merchant.component';
import {ProductService} from '../../service/product.service';
import {FileService} from '../../file.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {HeaderComponent} from '../../shared/header/header.component';
import {SharedModule} from '../../shared/shared.module';
import {FooterComponent} from '../../shared/footer/footer.component';
import {ProductShareModule} from '../product-share/product-share.module';

export const routes: Routes = [
  {path: '', component: ProductsComponent},
  {path: ':products', component: ProductsComponent},
  {
    path: '',
    component: HeaderComponent,
    outlet: 'header'
  }, {
    path: '',
    component: FooterComponent,
    outlet: 'footer'
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatExpansionModule,
    NgxPaginationModule,
    SharedModule,
    ProductShareModule
  ],
  declarations: [ProductListComponent, ProductsComponent, ProductsCategoriesComponent, ProductMerchantComponent],
  providers: [ProductService, FileService]
})
export class ProductsModule {
}
