import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductListComponent} from '../product-list/product-list.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @ViewChild('productList')
  productList: ProductListComponent;

  constructor() {
  }

  ngOnInit() {
  }

  setSelectedCategory(category: string) {
    this.productList.setSelectedCategory(category);
  }

}
