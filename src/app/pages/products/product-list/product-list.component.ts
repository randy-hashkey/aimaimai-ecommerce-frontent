import {Component, Input, OnInit} from '@angular/core';
import {FilterProductContent, ProductContent} from '../../../models/product.model';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FileService} from '../../../file.service';
import {ProductService} from '../../../service/product.service';
import {ProductCategoryService} from 'src/app/service/product-category.service';
import {ProductPublicService} from '../../../service/public/product-public.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @Input()
  merchantId: string;

  @Input()
  page: number = 0;

  @Input()
  pageSize: number = 10;

  contents: ProductContent[];

  total: number = 0;

  status: number;

  sorting: string;

  asc: boolean;

  fromDate: Date = null;

  endDate: Date = null;

  orderBy: string = null;

  selectedCategory: string = null;

  searchText: string = '';

  dataFetched: boolean = false;

  constructor(
    private _authService: AuthService,
    private _categoryService: ProductCategoryService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    public fileService: FileService,
    private _productService: ProductService,
    private _productPublicService: ProductPublicService
  ) {
  }

  setSelectedCategory(category: string) {
    this.selectedCategory = category;
    this.list(this.page);
  }

  ngOnInit() {
    this.selectedCategory = this._activatedRoute.snapshot.queryParams['category'];

    let searchText = this._activatedRoute.snapshot.queryParams['search'];
    if (searchText) {
      this.searchText = searchText;
    }
    this._activatedRoute.params.subscribe(params => {
      if (params['products']) {
        this.orderBy = params['products'];
      }

    });

    this.list(this.page);
    // this.listByCategory();
  }

  list(page: number) {
    this.page = page;
    if (this.selectedCategory) {
      this._productPublicService.listByCategoryPublic(this.selectedCategory, this.page, this.pageSize, this.searchText, this.orderBy)
        .subscribe((response) => {
          this.dataFetched = true;
          this.contents = response.products.content;
          this.total = response.products.totalElements;
        });
    } else {
      this._productPublicService.listPublic(this.page, this.pageSize, this.searchText, this.orderBy).subscribe(response => {
        this.contents = response.products.content;
        this.dataFetched = true;
        this.total = response.products.totalElements;
      });
    }
  }

  handleOnFilter(model: FilterProductContent) {
    this.fromDate = model.fromDate;
    this.endDate = model.endDate;
    this.searchText = model.search;
    this.asc = model.asc;
    this.sorting = model.sorting;
    this.list(this.page);
  }

  redirectToProductPage(product: ProductContent) {
    this._router.navigate(['/product/' + product.id]);
  }

}
