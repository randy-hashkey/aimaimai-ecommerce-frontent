import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductCategoryService} from 'src/app/service/product-category.service';
import {CategoryContent} from 'src/app/models/product-category.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-products-categories',
  templateUrl: './products-categories.component.html',
  styleUrls: ['./products-categories.component.scss']
})
export class ProductsCategoriesComponent implements OnInit {

  panelOpenState = false;

  selectedCategoryName: string;

  categoryContents: CategoryContent[] = [];

  @Output()
  update: EventEmitter<string> = new EventEmitter<string>();


  constructor(private _categoryService: ProductCategoryService,
              private _activatedRoute: ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit() {
    this.selectedCategoryName = this._activatedRoute.snapshot.queryParams['category'];
    this.listByCategory();
  }


  listByCategory() {
    this._categoryService.list(0, 8, null)
      .subscribe((response) => {
        this.categoryContents = response.categories.content;
      });
  }

  redirectToCategoryPage(name: string) {
    this.update.emit(name);
    this.selectedCategoryName = name;
    this._router.navigate(['/products'], {'queryParams': {'category': name}});
  }
}
