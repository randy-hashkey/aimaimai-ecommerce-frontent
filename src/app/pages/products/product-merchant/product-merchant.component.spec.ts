import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductMerchantComponent} from './product-merchant.component';

describe('ProductMerchantComponent', () => {
  let component: ProductMerchantComponent;
  let fixture: ComponentFixture<ProductMerchantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductMerchantComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMerchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
