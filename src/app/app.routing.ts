import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {ErrorComponent} from './shared/error/error.component';
import {AppGuard} from './app.guard';

export const routes: Routes = [
  {path: '', loadChildren: './pages/catalog/catalog.module#CatalogModule'},
  {path: 'products', loadChildren: './pages/products/products.module#ProductsModule'},
  {path: 'product/:id', loadChildren: './pages/product/product.module#ProductModule'},
  {path: 'my-products', loadChildren: './pages/my-products/my-products.module#MyProductsModule', canActivate: [AppGuard]},
  {path: 'register', loadChildren: './register/register.module#RegisterModule'},
  {path: 'login', loadChildren: './login/login.module#LoginModule'},
  {path: 'checkout', loadChildren: './pages/order/order.module#OrderModule', canActivate: [AppGuard]},
  {path: 'cart', loadChildren: './pages/cart/cart.module#CartModule'},
  {path: 'orders', loadChildren: './pages/order-management/order-viewing/order-viewing.module#OrderViewingModule'},
  {path: 'credit-wallet', loadChildren: './pages/credit-wallet/credit-wallet.module#CreditWalletModule'},
  {path: 'profile', loadChildren: './pages/account/account.module#AccountModule', canActivate: [AppGuard]},
  {path: 'user', loadChildren: './pages/user-info/user-info.module#UserInfoModule'},
  {path: 'my-store', loadChildren: './pages/my-store/my-store.module#MyStoreModule', canActivate: [AppGuard]},
  {path: 'invoice', loadChildren: './pages/invoice/invoice.module#InvoiceModule'},
  {path: 'order-detail', loadChildren: './pages/order-detail/order-detail.module#OrderDetailModule'},
  {path: 'merchant', loadChildren: './pages/merchant/merchant.module#MerchantModule'},
  {path: 'payment', loadChildren: './pages/payment-status/payment-status.module#PaymentStatusModule'},
  {path: 'my-inquiries', loadChildren: './pages/static-content/my-inquiries/my-inquiries.module#MyInquiriesModule'},
  {path: 'faq', loadChildren: './pages/static-content/faq/faq.module#FaqModule'},
  {path: 'help-topics', loadChildren: './pages/static-content/help-topics/help-topics.module#HelpTopicsModule'},
  {path: 'user-agreement', loadChildren: './pages/static-content/user-agreement/user-agreement.module#UserAgreementModule'},
  {path: 'privacy-policy', loadChildren: './pages/static-content/privacy-policy/privacy-policy.module#PrivacyPolicyModule'},
  {path: 'contact-us', loadChildren: './pages/static-content/contact-us/contact-us.module#ContactUsModule'},
  {
    path: 'security-and-resolution-center',
    loadChildren: './pages/static-content/security-and-resolution-center/security-and-resolution-center.module#SecurityAndResolutionCenterModule'
  },
  {path: 'safe-program', loadChildren: './pages/static-content/safe-program/safe-program.module#SafeProgramModule'},
  {path: 'about-aimaimai', loadChildren: './pages/static-content/about-aimaimai/about-aimaimai.module#AboutAimaimaiModule'},
  {
    path: 'terms-and-conditions',
    loadChildren: './pages/static-content/terms-and-conditions/terms-and-conditions.module#TermsAndConditionsModule'
  },
  {path: 'privacy-statement', loadChildren: './pages/static-content/privacy-statement/privacy-statement.module#PrivacyStatementModule'},
  {path: 'press-and-media', loadChildren: './pages/static-content/press-and-media/press-and-media.module#PressAndMediaModule'},
  {path: 'all-brands', loadChildren: './pages/static-content/all-brands/all-brands.module#AllBrandsModule'},
  {path: 'sitemap', loadChildren: './pages/static-content/sitemap/sitemap.module#SitemapModule'},

  {path: 'unsubscribe', loadChildren: './pages/unsubscribe/unsubscribe.module#UnsubscribeModule'},
  {path: '**', component: ErrorComponent},
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
