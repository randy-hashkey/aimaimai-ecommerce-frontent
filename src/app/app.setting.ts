export class AppSettings {

  public static USER_LOGIN = './api/user/email/public/login';
  public static GET_FILES_BY_USER = '';
  public static GET_FILES_BY_USER_API = '';
  public static GET_FILE_BY_ID_API = '';
  public static TOKEN_STORAGE = 'authKey';
  public static USER_INFO = 'userInfo';
  public static FIRST_NAME_STORAGE = 'firstName';
  public static LAST_NAME_STORAGE = 'lastName';
  public static CART_ID_STORAGE = 'cartId';
  public static GET_FILE_BY_PATH_API = '';
  public static NO_IMAGE_API = '/assets/img/app/no-image.png';
  public static LEFT_KEY = 37;
  public static RIGHT_KEY = 39;
  public static ENTER_KEY = 13;
  public static DOWN_KEY = 40;
  // public static GET_IMAGE_BY_ID = './api/file/default/id';
  public static GET_IMAGE_BY_ID = './api/file/download/public/';
  public static GET_IMAGE_BY_TOKEN = './api/file/download/public';
  public static DOWNLOAD_FILE = './api/file/download/public/';
  public static DELETE_FILE = './api/file/delete';
  public static GET_USER_IMAGE = './apu/file/download';
  public static GET_IMAGE_PATH = './api/file/download';
  public static UPLOAD_IMAGE = './api/file/upload';
  public static USER_LIST = './api/user/user';
  public static CURRENCY_LIST = './api/currency/search-own';
  public static CURRENCY_REWARD = './api/currency/reward/';
  public static CURRENCY_PREFERENCE = './api/currency/preferred/';
  public static CURRENCY_GROUP_LIST = './api/currency/group/list-by-user';
  private static host: string = './apifile/';


}
